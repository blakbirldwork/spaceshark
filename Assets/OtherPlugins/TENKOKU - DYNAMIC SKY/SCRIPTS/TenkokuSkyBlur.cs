﻿using System;
using UnityEngine;

namespace Tenkoku.Effects
{
  [RequireComponent (typeof(Camera))]
  public class TenkokuSkyBlur : MonoBehaviour
  {

    public int iterations = 3;
    public float blurSpread = 0.6f;
    public Shader blurShader = null;
    public Material material = null;


    void Start(){

        if (material == null){
          material = new Material(blurShader);
          material.hideFlags = HideFlags.DontSave;
        }

        // Disable if we don't support image effects
        if (!SystemInfo.supportsImageEffects){
          enabled = false;
          return;
        }
        // Disable if the shader can't run on the users graphics card
        if (!blurShader || !material.shader.isSupported){
          enabled = false;
          return;
        }
    }



    // Performs one blur iteration.
    void FourTapCone (RenderTexture source, RenderTexture dest, int iteration)
    {
        float off = 0.5f + iteration*blurSpread;
        Graphics.BlitMultiTap (source, dest, material,
                               new Vector2(-off, -off),
                               new Vector2(-off,  off),
                               new Vector2( off,  off),
                               new Vector2( off, -off)
            );
    }

    // Downsamples the texture to a quarter resolution.
    void DownSample4x (RenderTexture source, RenderTexture dest)
    {
        float off = 1.0f;
        Graphics.BlitMultiTap (source, dest, material,
                               new Vector2(-off, -off),
                               new Vector2(-off,  off),
                               new Vector2( off,  off),
                               new Vector2( off, -off)
            );
    }

    // Called by the camera to apply the image effect
    void OnRenderImage (RenderTexture source, RenderTexture destination)
    {
        int rtW = source.width/4;
        int rtH = source.height/4;
        RenderTexture buffer = RenderTexture.GetTemporary(rtW, rtH, 0);

        // Copy source to the 4x4 smaller texture.
        DownSample4x (source, buffer);

        // Blur the small texture
        for(int i = 0; i < iterations; i++)
        {
            RenderTexture buffer2 = RenderTexture.GetTemporary(rtW, rtH, 0);
            FourTapCone (buffer, buffer2, i);
            RenderTexture.ReleaseTemporary(buffer);
            buffer = buffer2;
        }
        Graphics.Blit(buffer, destination);

        RenderTexture.ReleaseTemporary(buffer);
    }



  }
}




