using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

#if UNITY_EDITOR
	using UnityEditor;
#endif


namespace Tenkoku.Core
{


	[ExecuteInEditMode]
	[System.Serializable]
    public class TenkokuModule : MonoBehaviour
	{



	//PUBLIC VARIABLES
	public string tenkokuVersionNumber = "";

	public float tenSystemTime;
	public Transform mainCamera;
	public Transform manualCamera;
	public int cameraTypeIndex = 0;
	//var cameraTypeOptions = new Array("Auto Select Camera","Manual Select Camera");
	//List<string> cameraTypeOptions = new List<string>{"Auto Select Camera","Manual Select Camera"};
	//static string[] cameraTypeOptions = new string[]{"Auto Select Camera","Manual Select Camera"};

	public int lightLayer = 2;
	public LayerMask lightLayerMask;
	public List<string> tenLayerMasks;

	public Color colorOverlay = new Color(1f,1f,1f,1f);
	public Color colorSky = new Color(1f,1f,1f,0f);
	public Color colorAmbient = new Color(0f,0f,0f,0.6f);
	private Color useColorAmbient = new Color(0f,0f,0f,0.6f);

	public bool useSunRays = true;
	public float sunRayIntensity = 1.0f;
	public float sunRayLength = 0.2f;
	public float moonRayIntensity = 1.0f;
	public bool useAmbient  = true;
	//bool _isForward = false;

	public bool enableIBL = false;
	public bool enableFog = false;

	public bool showConfig = false;
	public bool showCelSet = false;
	public bool showTimer = true;
	public bool showSounds = false;

	public bool showConfigTime = false;
	public bool showConfigAtmos = false;
	public bool showConfigWeather = false;

	public bool showIBL = false;
	public bool allowMultiLights = true;

	float cloudRotSpeed = 0.2f;
	float cloudOverRot = 0.0f;
	//float cloudSz = 1.0f;
	float cloudSc = 1.0f;
	public float skyBrightness = 1.0f;
	float useSkyBright = 1.0f;
	public float nightBrightness = 0.4f;
	public float fogDistance = 0.0015f;
	public float fogAtmosphere = 0.2f;

	public float setRotation = 180.0f;
				
	float useLatitude = 0.0f;
	public float setLatitude = 0.0f;
	public float setLongitude = 0.0f;

	//bool sunIsVisible = true;
	public float sunSize = 0.02f;
	public float sunBright = 1.0f;
	public float sunSat = 1.0f;
	float sC;

	bool moonIsVisible = true;
	public float moonSize = 0.02f;
	public float moonPos = 0.0f;
	public float moonBright = 1.0f;
	public float moonSat = 1.0f;
	float mC;

	bool auroraIsVisible = true;
	public float auroraSize = 1.4f;
	public float auroraLatitude = 1.0f;
	public float auroraIntensity = 1.0f;
	public float auroraSpeed = 0.5f;

	public float planetIntensity = 1.0f;
	//bool starIsVisible = true;

	public float ambientShift = 0.0f;
	public float moonLightIntensity = 0.25f;
	public float moonDayIntensity = 0.2f;
	public float starIntensity = 1.0f;
	public float galaxyIntensity = 1.0f;

	public float horizonHeight = 0.5f;
	public float horizonScale = 45.0f;

	public float ambHorizonHeight = 1.0f;
	public float ambHorizonScale = 30.0f;

	public float lowHorizonHeight = 0.5f;
	public float lowHorizonScale = 1.0f;

	public Texture2D colorRamp;


	AudioClip[] globalSoundFile;
	Vector4[] globalSoundVolDay;
	Vector4[] globalSoundVolYear;

	GameObject cloudPlaneObject;

	public int weatherTypeIndex = 0;
	public int sunTypeIndex = 0;
	public int moonTypeIndex = 0;
	public int starTypeIndex = 0;
	public int galaxyTypeIndex = 0;
	public int auroraTypeIndex = 0;
	public int galaxyTexIndex = 0;

	public Texture galaxyTex ;
	public Texture galaxyCubeTex;

	public Transform useCamera;
	private Camera useCameraCam;
	private Transform saveCamera;
	//private float setSkySize = 20.0f;
	private float setSkyUseSize = 1.0f;
	private GameObject worldlightObject;
	private WindZone tenkokuWindObject;

	public GameObject sunlightObject;
	public GameObject sunSphereObject;
	private GameObject sunObject;
	private GameObject eclipseObject;
	private GameObject moonSphereObject;
	private GameObject moonObject;
	public Transform moonlightObject;
	private GameObject skyObject;
	private GameObject ambientObject;

	public float moonLightAmt;
	public float moonPhase;
	private Quaternion mres;




	private ParticleStarfieldHandler starRenderSystem;
	private Renderer starParticleSystem;
	private GameObject starfieldObject;
	private GameObject starGalaxyObject;

	private Mesh mesh;
	private Vector3[] vertices;
	private Color[] colors;
	private Mesh meshAmb;
	private Vector3[] verticesAmb;
	private Color[] colorsAmb;

	private Vector3[] verticesLow;
	private Color[] colorsLow;

	private Tenkoku.Core.TenkokuGlobalSound ambientSoundObject;

	private Color rayCol;

	//private var sunFlare : LensFlare;

	public Color ambientCol = new Color(0.5f,0.5f,0.5f,1.0f);
	public Color colorSun = new Color(1f,1f,1f,1f);
	public Color colorMoon = new Color(1f,1f,1f,1f);
	public Color colorSkyBase = new Color(1f,1f,1f,1f);
	public Color colorSkyBaseLow = new Color(1f,1f,1f,1f);
	public Color colorHorizon = new Color(1f,1f,1f,1f);
	public Color colorHorizonLow = new Color(1f,1f,1f,1f);
	public Color colorSkyAmbient = new Color(1f,1f,1f,1f);
	public Color colorClouds = new Color(1f,1f,1f,1f);
	public Color colorHighlightClouds = new Color(1f,1f,1f,1f);
	public Color colorSkyboxGround = new Color(0f,0f,0f,1f);

	int stepTime;
	float timeLerp = 0.0f;

	float calcTimeM = 0.0f;
	float setTimeM = 0.0f;
	int stepTimeM = 0;
	float timeLerpM = 0.0f;


	//TIMER VARIABLES
	public string displayTime = "";

	public bool autoTimeSync = false;
	public bool autoDateSync = false;
	public bool autoTime = false;
	public float timeCompression = 100.0f;
	public int currentSecond = 0;
	public int currentMinute = 45;
	public int currentHour = 5;
	public int currentDay = 22;
	public int currentMonth = 3;
	public int currentYear = 2013;

	public bool useAutoTime = false;
	public float useTimeCompression = 100.0f;
	public bool use24Clock = false;
	public AnimationCurve timeCurves;
	public int setHour = 5;
	public float useSecond = 0f;
	public float useMinute = 0f;
	public float displayHour = 0f;
	public float useHour = 0f;
	public float useDay = 0f;
	public float useMonth = 0f;
	public float useYear = 0f;

	public string hourMode = "";
		
	public int moonSecond = 0;
	public int moonMinute = 0;
	public int moonHour = 0;
	public int moonDay = 0;
	public int moonMonth = 0;
	public int moonYear = 0;


	public int starSecond = 0;
	public int starMinute = 0;
	public int starHour = 0;
	public int starDay = 0;
	public int starMonth = 0;
	public int starYear = 0;


	public bool leapYear = false;
	public int monthFac = 31;

	public Vector3 moonTime;

	public float setDay = 22000.0f;
	public float setStar = 22000.0f;
	public float setMoon = 22000.0f;
	public float setMonth = 22000.0f;
	public float setYear = 22000.0f;

	public float dayValue = 0.5f;
	public float starValue = 0.5f;
	public float moonValue = 0.0f;
	public float monthValue = 0.0f;
	public float yearValue = 0.0f;

	public float countSecond =  0.0f;
	public float countMinute =  0.0f;

	public float countSecondStar =  0.0f;
	public float countMinuteStar =  0.0f;

	public float countSecondMoon =  0.0f;
	public float countMinuteMoon =  0.0f;

	public bool cloudLinkToTime = false;


	private Transform nightSkyLightObject;
	private Matrix4x4 MV;

	public float starPos = 0.0f;

	public float galaxyPos = 0.0f;



	//Weather Variables
	public int currentWeatherTypeIndex = -1;
	public bool weather_forceUpdate = false;

	public bool weather_setAuto = false;
	public float weather_qualityCloud = 0.7f;
	public float weather_cloudAltAmt = 1.0f;

	public float weather_cloudAltoStratusAmt = 0.0f;
	public float weather_cloudCirrusAmt = 0.0f;

	public float weather_cloudCumulusAmt = 0.2f;
	public float weather_cloudScale = 0.5f;
	public float weather_cloudSpeed = 0.0f;

	public float weather_OvercastAmt = 0.0f;
	public float weather_RainAmt = 0.0f;
	public float weather_SnowAmt = 0.0f;
	public float weather_WindAmt = 0.0f;
	public float weather_WindDir = 0.0f;
	public float weather_FogAmt = 0.0f;
	public float weather_FogHeight = 0.0f;

	public float weather_autoForecastTime = 5.0f;
	public float weather_PatternTime = 0.0f;
	public float weather_TransitionTime = 1.0f;

	public float weather_temperature = 75.0f;
	public float weather_rainbow = 0.0f;
	public float weather_lightning = 0.0f;
	public float weather_lightningDir = 110.0f;
	public float weather_lightningRange = 180.0f;



	// auto weather variables
	private float w_isCloudy;
	private float w_isOvercast;
	private Vector4 w_cloudAmts;
	private Vector4 w_cloudTgts;
	private float w_cloudAmtCumulus;
	private float w_cloudTgtCumulus;
	private float w_overcastAmt;
	private float w_overcastTgt;
	private float w_windCAmt;		
	private float w_windDir;
	private float w_windAmt;
	private float w_windCTgt;
	private float w_windDirTgt;
	private float w_windTgt;
	private float w_rainAmt;
	private float w_rainTgt;
	private float w_snowAmt;
	private float w_snowTgt;
	private float w_lightningAmt;
	private float w_lightningTgt;

	private Tenkoku.Core.TenkokuCalculations calcComponent;
	private Vector2 weather_WindCoords = new Vector2(0f,0f);
	private Vector2 currCoords = new Vector2(0f,0f);
	private ParticleSystem rainSystem;
	private ParticleSystem rainFogSystem;
	private ParticleSystem rainSplashSystem;
	private ParticleSystem snowSystem;
	private ParticleSystem fogSystem;

	private ParticlePlanetHandler planetObjMercury;
	private ParticlePlanetHandler planetObjVenus;
	private ParticlePlanetHandler planetObjMars;
	private ParticlePlanetHandler planetObjJupiter;
	private ParticlePlanetHandler planetObjSaturn;
	private ParticlePlanetHandler planetObjUranus;
	private ParticlePlanetHandler planetObjNeptune;
	private Renderer planetRendererSaturn;
	private Renderer planetRendererJupiter;
	private Renderer planetRendererNeptune;
	private Renderer planetRendererUranus;
	private Renderer planetRendererMercury;
	private Renderer planetRendererVenus;
	private Renderer planetRendererMars;


	private Light lightObjectNight;
	private Light lightObjectWorld;

	private Renderer renderObjectMoon;
	private Renderer renderObjectGalaxy;

	private Renderer renderObjectRain;
	private Renderer renderObjectRainSplash;
	private Renderer renderObjectRainFog;
	private Renderer renderObjectFog;
	private Renderer renderObjectSnow;


	private Renderer renderObjectCloudPlane;
	private Renderer renderObjectCloudSphere;
	private Renderer renderObjectAurora;

	private ParticleSystem particleObjectRainFog;
	private ParticleSystem particleObjectRainSplash;
	private ParticleSystem particleObjectFog;


	//collect for GC
	private float setTime;
	private float calcTime;
	//private float yAmt;
	//private float setEcliptic;
	private float currClipDistance = 0.0f;
	private float iblActive = 0.0f;
	private float isLin = 0.0f;
	//private float setTenkoku = 0.0f;
	private float currIblActive = -1.0f;
	private float currIsLin = -1.0f;
	//private float currSetTenkoku = -1.0f;
	private bool doWeatherUpdate = false;
	private float chanceOfRain = -1.0f;

	private float moonApoSize = -1.0f;
	//private float moonInt = -1.0f;
	//private float moonWorlIntensity = -1.0f;
	private Color mDCol = new Color(0f,0f,0f,0f);
	//private float setMCol = -1.0f;
	//private float solarSetInten = -1.0f;
	public float ecldiff = -1.0f;
	//private float eclAnnular = -1.0f;
	private Transform tempTrans;
	private Vector3 useVec = new Vector3(0f,0f,0f);
	private float useMult = -1.0f;
	private float pVisFac = -1.0f;
	private float starAngle = -1.0f;
	private Quaternion starTarget;
	private int aquarialStart = 0;
	private float precRate = -1.0f;
	private float precNudge = -1.0f;
	private float precFactor = -1.0f;
	private float eclipticStarAngle = -1.0f; 
	//private float useGalaxyIntensity = -1.0f; 
	private Color galaxyColor = new Color(0f,0f,0f,0f);
	private float galaxyVis = -1.0f;
	private Color ambientCloudCol = new Color(0f,0f,0f,0f);
	private Color setSkyAmbient = new Color(0f,0f,0f,0f);
	private Color useAlpha = new Color(0f,0f,0f,0f);
	private Color setAmbCol = new Color(0f,0f,0f,0f);
	private Color setAmbientColor = new Color(0f,0f,0f,0f);
	private Color mixColor = new Color(0f,0f,0f,0f);
	private Color medCol  = new Color(0f,0f,0f,0f);
	private Color useAmbientColor = new Color(0f,0f,0f,0f);
	private float useOvercast = -1.0f;
	//private float sL = -1.0f;
	private float fxUseLight = -1.0f;
	private float lDiff = -1.0f;
	private Color rainCol = new Color(0f,0f,0f,0f);
	private Color splashCol = new Color(0f,0f,0f,0f);
	private Color rainfogCol = new Color(0f,0f,0f,0f);
	private float rainfogFac = -1.0f;	
	private Color fogCol = new Color(0f,0f,0f,0f);
	private float fogFac = -1.0f;	
	private Color snowCol = new Color(0f,0f,0f,0f);
	private float timerCloudMod = -1.0f;
	//private float cloudSpd = -1.0f;
	private Color setCloudFog1 = new Color(0f,0f,0f,0f);
	//private Color setCloudTint = new Color(0f,0f,0f,0f);
	//private Color setCloudBase = new Color(0f,0f,0f,0f);
	private Color setOverallCol = new Color(0f,0f,0f,0f);
	private Color bgSCol = new Color(0f,0f,0f,0f);

	private float altAdjust = -1.0f;
	public float fogDist = -1.0f;
	private float fogFull = -1.0f;
	private Color skyAmbientCol = new Color(0f,0f,0f,0f);
	private Color skyHorizonCol = new Color(0f,0f,0f,0f);
	private Color ambientGI = new Color(0f,0f,0f,0f);
	private float aurSpan = -1.0f;
	private float aurAmt = -1.0f;
	//private float aurPos = -1.0f;
	private float setTimeSpan = -1.0f;
	private float curveVal = 1.0f;
	private string setString = "format";
	private string eon = "";
	private int monthLength = -1;
	private int testMonth = -1;
	private float monthAddition = 0.0f;
	private int aM = 1;
	private float yearDiv = 365.0f;
	private float clampRes = 0.0f;
	private int px = 0;
	private RaycastHit hit;
	private float usePoint = 0.0f;
	private ParticleSystem.Particle[] setParticles;
	private Vector2 dir = new Vector2(0f,0f);
	private Vector3 tempAngle = new Vector3(0f,0f,0f);
	private int texPos = 0;
	private Color returnColor = new Color(0f,0f,0f,0f);
	//private float eclipticAngle;
	private float eclipticoffset;

	//private float sunAngle = -1.0f;
	private float sSDiv = -1.0f;
	private float sunCalcSize = -1.0f;
	//private float moonAngle = -1.0f;
	//private float moonClipAngle = -1.0f;
	//private float moonPhac = 5.0f;
	private float moonCalcSize;

	//private Color currColorSun = new Color(0f,0f,0f,0f);
	public float atmosphereDensity = 0.55f;
	public float horizonDensity = 2.0f;
	public float horizonDensityHeight = 0.34f;
	private float atmosTime = 1.0f;
	private float setAtmosphereDensity = 0.0f;

	public int fogDisperse = 25;
	public float fogDensity = 0.55f;

	public bool enableProbe = true;
	public float reflectionProbeFPS = 1.0f;

	private ReflectionProbe tenkokuReflectionObject;
	private float reflectionTimer = 0.0f;


	//sound variables
	public bool enableSoundFX = true;
	public bool enableTimeAdjust = true;
	public float overallVolume = 1.0f;
	public AudioClip audioWind;
	public AudioClip audioRain;
	public AudioClip audioTurb1;
	public AudioClip audioTurb2;
	public AudioClip audioAmbDay;
	public AudioClip audioAmbNight;
	public float volumeWind = 1.0f;
	public float volumeRain = 1.0f;
	public float volumeThunder = 1.0f;
	public float volumeTurb1 = 1.0f;
	public float volumeTurb2 = 1.0f;
	public float volumeAmbDay = 1.0f;
	public float volumeAmbNight = 1.0f;

	public AnimationCurve curveAmbDay24;
	public AnimationCurve curveAmbDayYR;
	public AnimationCurve curveAmbNight24;
	public AnimationCurve curveAmbNightYR;


	public Tenkoku.Core.Random TenRandom;
	public int randSeed;
	System.DateTime sysTime;



	//Transition variables
	float transitionTime = 0.0f;
	//float compressionTime = 0.0f;
	bool doTransition = false;
	string transitionStartTime = "";
	string transitionTargetTime;
	string transitionTargetDate;
	float transitionDuration = 5.0f;
	float transitionDirection = 1.0f;
	//bool transitionEase = true;
	//float transitionEaseTime = 1.0f;
	bool transitionCallback = false;
	GameObject transitionCallbackObject;
	private int setTransHour;
	private int setTransMinute;
	private int setTransSecond;
	private int startSecond;
	private int startMinute;
	private int startHour;
	private float timeVal;
	private float setTransVal;
	private int endTime = 0;
	private int startTime = 0;
	private int currTime = 0;
	private bool transSameDay = true;
	private bool endTransition = false;

	private string fogVals;
	private Camera fogCameraCam;
	private Tenkoku.Effects.TenkokuSkyBlur fogCameraBlur;
	private string setSun;
	private string lightVals;

	private int i;
	private string layerName;
	private Vector3 sunPos;
	//private Color sunColor;
	private Color moonFaceCol;
	private Color cloudSpeeds;
	private Color overcastCol;
	private Color WorldCol;


	private string dataString ;
	private string[] dataArray;
	private int pos1;
	private int pos2;
	private int length;
	private string func;
	private string dat;
	private int xP = 0;
	private int ax = 0;
	private string dataUpdate;
	private string[] data;
	private string setUpdate;
	private string[] values;
	private GameObject callbackObject;


	private float rOC;
	private float mMult;
	private float sunVert;
	private Vector3 delta;
	private Quaternion look;
	private float vertical;
	private Ray mRay;
	private Ray sRay;


	//Variables for Unity 5.3+ only
	#if UNITY_5_3 || UNITY_5_4 || UNITY_5_6 || UNITY_5_7 || UNITY_5_8 || UNITY_5_9
		private ParticleSystem.EmissionModule rainEmission;
		private ParticleSystem.EmissionModule rainFogEmission;
		private ParticleSystem.EmissionModule splashEmission;
		private ParticleSystem.EmissionModule fogEmission;
		private ParticleSystem.EmissionModule snowEmission;

		private ParticleSystem.ForceOverLifetimeModule rainForces;
		private ParticleSystem.ForceOverLifetimeModule rainFogForces;
		private ParticleSystem.ForceOverLifetimeModule fogForces;
		private ParticleSystem.ForceOverLifetimeModule snowForces;
	#endif




	//flag variables
	float currWeather_OvercastAmt = -1.0f;




	void Awake(){
		//GET CAMERA OBJECT
		if (Application.isPlaying){
			if (mainCamera == null){
				mainCamera = Camera.main.transform;
			}
		}

		//set custom random generator instance
		randSeed = System.Environment.TickCount;
		TenRandom = new Tenkoku.Core.Random(randSeed);

		//set System Time
		tenSystemTime = randSeed * 1f;
	}




	void Start () {

		//disconnect object from prefab connection
		#if UNITY_EDITOR
			PrefabUtility.DisconnectPrefabInstance(this.gameObject);
		#endif


		//turn off Antialiasing in forward mode
		if (mainCamera != null){
			if (mainCamera.gameObject.GetComponent<Camera>().renderingPath == RenderingPath.Forward){
	        	QualitySettings.antiAliasing = 0;
	    	}
	    }


		//get objects at start
		LoadObjects();

	}





	void LoadObjects() {

		//GET TENKOKU OBJECTS
		calcComponent = this.gameObject.GetComponent<Tenkoku.Core.TenkokuCalculations>() as Tenkoku.Core.TenkokuCalculations;
		
		//GET WINDZONE OBJECT
		tenkokuWindObject = GameObject.Find("Tenkoku_WindZone").GetComponent<WindZone>();


		//GET REFLECTION PROBE
		if (GameObject.Find("Tenkoku_ReflectionProbe") != null){
			tenkokuReflectionObject = GameObject.Find("Tenkoku_ReflectionProbe").GetComponent<ReflectionProbe>();
		}


		//GET CELESTIAL OBJECTS
		worldlightObject = GameObject.Find("LIGHT_World");
		cloudPlaneObject = GameObject.Find("fxCloudPlane").gameObject;
		sunlightObject = GameObject.Find("LIGHT_Sun");
		sunSphereObject = GameObject.Find("SkySphereSun");
		sunObject = GameObject.Find("Sun");
		eclipseObject = GameObject.Find("sun_solareclipsefx");
		moonSphereObject = GameObject.Find("SkySphereMoon");
		moonObject = GameObject.Find("Moon");
		moonlightObject = GameObject.Find("LIGHT_Moon").transform;
		skyObject = GameObject.Find("SkySphere");

		starfieldObject = GameObject.Find("SkySphereStar");
		starGalaxyObject = GameObject.Find("SkySphereGalaxy");

		if (GameObject.Find("StarRenderSystem") != null){
			starRenderSystem = GameObject.Find("StarRenderSystem").GetComponent<Tenkoku.Core.ParticleStarfieldHandler>() as Tenkoku.Core.ParticleStarfieldHandler;
			starParticleSystem = GameObject.Find("StarRenderSystem").GetComponent<Renderer>() as Renderer;
		}
		
		if (GameObject.Find("planetMars")) planetObjMars = GameObject.Find("planetMars").gameObject.GetComponent<Tenkoku.Core.ParticlePlanetHandler>() as Tenkoku.Core.ParticlePlanetHandler;
		if (GameObject.Find("planetMercury")) planetObjMercury = GameObject.Find("planetMercury").gameObject.GetComponent<Tenkoku.Core.ParticlePlanetHandler>() as Tenkoku.Core.ParticlePlanetHandler;
		if (GameObject.Find("planetVenus")) planetObjVenus =GameObject.Find("planetVenus").gameObject.GetComponent<Tenkoku.Core.ParticlePlanetHandler>() as Tenkoku.Core.ParticlePlanetHandler;
		if (GameObject.Find("planetJupiter")) planetObjJupiter = GameObject.Find("planetJupiter").gameObject.GetComponent<Tenkoku.Core.ParticlePlanetHandler>() as Tenkoku.Core.ParticlePlanetHandler;
		if (GameObject.Find("planetSaturn")) planetObjSaturn = GameObject.Find("planetSaturn").gameObject.GetComponent<Tenkoku.Core.ParticlePlanetHandler>() as Tenkoku.Core.ParticlePlanetHandler;
		if (GameObject.Find("planetUranus")) planetObjUranus = GameObject.Find("planetUranus").gameObject.GetComponent<Tenkoku.Core.ParticlePlanetHandler>() as Tenkoku.Core.ParticlePlanetHandler;
		if (GameObject.Find("planetNeptune")) planetObjNeptune = GameObject.Find("planetNeptune").gameObject.GetComponent<Tenkoku.Core.ParticlePlanetHandler>() as Tenkoku.Core.ParticlePlanetHandler;
			
		if (planetObjSaturn != null) planetRendererSaturn = GameObject.Find("planetSaturn").GetComponent<Renderer>() as Renderer;
		if (planetObjJupiter != null) planetRendererJupiter = GameObject.Find("planetJupiter").GetComponent<Renderer>() as Renderer;
		if (planetObjNeptune != null) planetRendererNeptune = GameObject.Find("planetNeptune").GetComponent<Renderer>() as Renderer;
		if (planetObjUranus != null) planetRendererUranus = GameObject.Find("planetUranus").GetComponent<Renderer>() as Renderer;
		if (planetObjMercury != null) planetRendererMercury = GameObject.Find("planetMercury").GetComponent<Renderer>() as Renderer;
		if (planetObjVenus != null) planetRendererVenus = GameObject.Find("planetVenus").GetComponent<Renderer>() as Renderer;
		if (planetObjMars != null) planetRendererMars = GameObject.Find("planetMars").GetComponent<Renderer>() as Renderer;


		nightSkyLightObject = GameObject.Find("LIGHT_NightSky").transform;

		//GET SOUND OBJECT
		ambientSoundObject = GameObject.Find("AMBIENT_SOUNDS").gameObject.GetComponent<Tenkoku.Core.TenkokuGlobalSound>() as Tenkoku.Core.TenkokuGlobalSound;

		//GET FLARE OBJECTS
	    //sunFlare = sunObject.GetComponent(LensFlare);
	    

		//GET WEATHER OBJECTS
		if (GameObject.Find("fxRain")) rainSystem = GameObject.Find("fxRain").gameObject.GetComponent<ParticleSystem>();
		if (GameObject.Find("fxRainFog")) rainFogSystem = GameObject.Find("fxRainFog").gameObject.GetComponent<ParticleSystem>();
		if (GameObject.Find("fxRainSplash")) rainSplashSystem = GameObject.Find("fxRainSplash").gameObject.GetComponent<ParticleSystem>();
		if (GameObject.Find("fxSnow")) snowSystem = GameObject.Find("fxSnow").gameObject.GetComponent<ParticleSystem>();
		if (GameObject.Find("fxFog")) fogSystem = GameObject.Find("fxFog").gameObject.GetComponent<ParticleSystem>();
		

		//GET AURORA OBJECTS
		renderObjectAurora = GameObject.Find("fxAuroraPlane").gameObject.GetComponent<Renderer>();
		renderObjectAurora.enabled = false;

		//GET COMPONENT REFERENCES
		lightObjectNight = nightSkyLightObject.GetComponent<Light>();
		lightObjectWorld = worldlightObject.GetComponent<Light>();


		renderObjectMoon = moonObject.GetComponent<Renderer>();
		renderObjectGalaxy = starGalaxyObject.GetComponent<Renderer>();

	if (Application.isPlaying){
		if (rainSystem) renderObjectRain = rainSystem.GetComponent<Renderer>();
		if (rainSplashSystem) renderObjectRainSplash = rainSplashSystem.GetComponent<Renderer>();
		if (rainFogSystem) renderObjectRainFog = rainFogSystem.GetComponent<Renderer>();
		if (fogSystem) renderObjectFog = fogSystem.GetComponent<Renderer>();
		if (snowSystem) renderObjectSnow = snowSystem.GetComponent<Renderer>();

		if (rainFogSystem) particleObjectRainFog = rainFogSystem.GetComponent<ParticleSystem>();
		if (rainSplashSystem) particleObjectRainSplash = rainSplashSystem.GetComponent<ParticleSystem>();
		if (fogSystem) particleObjectFog = fogSystem.GetComponent<ParticleSystem>();
	}

		renderObjectCloudPlane = cloudPlaneObject.GetComponent<Renderer>();

		fogCameraCam = GameObject.Find("Tenkoku_SkyFog").GetComponent<Camera>();
		fogCameraBlur = GameObject.Find("Tenkoku_SkyFog").GetComponent<Tenkoku.Effects.TenkokuSkyBlur>() as Tenkoku.Effects.TenkokuSkyBlur;


	}
	    
	    


	void HandleGlobalSound(){


	    //--------------------------------------
	    //---    CALCULATE MASTER VOLUME    ----
	    //--------------------------------------
		overallVolume = Mathf.Clamp01(overallVolume);


	    //---------------------------------------
	    //---    CALCULATE ELEMENT VOLUME    ----
	    //---------------------------------------
		ambientSoundObject.volWind = Mathf.Lerp(0.0f,1.5f,weather_WindAmt) * volumeWind * overallVolume;
		ambientSoundObject.volTurb1 = Mathf.Lerp(0.0f,2.0f,weather_WindAmt) * volumeTurb1 * overallVolume;
		ambientSoundObject.volTurb2 = Mathf.Lerp(-1.0f,1.5f,weather_WindAmt) * volumeTurb2 * overallVolume;
		ambientSoundObject.volRain = volumeRain * (weather_RainAmt*1.5f) * overallVolume;


	    //---------------------------------------------
	    //---    CALCULATE AMBIENT AUDIO CURVES    ----
	    //---------------------------------------------
		ambientSoundObject.volAmbNight = volumeAmbNight * curveAmbNight24.Evaluate(dayValue) * curveAmbNightYR.Evaluate(yearValue) * overallVolume;
		ambientSoundObject.volAmbDay = volumeAmbDay * curveAmbDay24.Evaluate(dayValue) * curveAmbDayYR.Evaluate(yearValue) * overallVolume;

	    //-----------------------------------------
	    //---    SEND DATA TO SOUND HANDLER    ----
	    //-----------------------------------------
	    ambientSoundObject.enableSounds = enableSoundFX;
	    ambientSoundObject.enableTimeAdjust = enableTimeAdjust;

		if (audioWind != null){
			ambientSoundObject.audioWind = audioWind;
		} else {
			ambientSoundObject.audioWind = null;
		}

		if (audioTurb1 != null){
			ambientSoundObject.audioTurb1 = audioTurb1;
		} else {
			ambientSoundObject.audioTurb1 = null;
		}

		if (audioTurb2 != null){
			ambientSoundObject.audioTurb2 = audioTurb2;
		} else {
			ambientSoundObject.audioTurb2 = null;
		}

		if (audioRain != null){
			ambientSoundObject.audioRain = audioRain;
		} else {
			ambientSoundObject.audioRain = null;
		}

		if (audioAmbDay != null){
			ambientSoundObject.audioAmbDay = audioAmbDay;
		} else {
			ambientSoundObject.audioAmbDay = null;
		}

		if (audioAmbNight != null){
			ambientSoundObject.audioAmbNight = audioAmbNight;
		} else {
			ambientSoundObject.audioAmbNight = null;
		}


	}






	void Update(){


	    if (!doTransition){
	    	useAutoTime = autoTime;
		}

		// EDITOR MODE SPECIFIC
		if (!Application.isPlaying){

			//set project layer masks
			tenLayerMasks = new List<string>();
			for (i = 0; i < 32; i++){
				layerName = LayerMask.LayerToName(i);
				tenLayerMasks.Add(layerName);
			}

			//get objects in editor mode
			LoadObjects();

	    	//rotate sun in editor mode
	    	sunlightObject.transform.LookAt(sunSphereObject.transform);
			worldlightObject.transform.localRotation = sunlightObject.transform.localRotation;
			lightObjectNight.transform.localRotation = moonObject.transform.localRotation;

			//set layers
			if (lightObjectWorld.gameObject.activeInHierarchy){
				lightObjectWorld.cullingMask = lightLayer;
				lightObjectNight.cullingMask = lightLayer;
			}


			//recalculate time
			TimeUpdate();
		}


		//track camera
		if (Application.isPlaying){
			UpdatePositions();
		}

	}





	void UpdatePositions(){

		// -------------------------------
		// --   SET SKY POSITIONING   ---
		// -------------------------------
		if (Application.isPlaying){

			// track positioning
			if (useCamera != null){
				skyObject.transform.position = useCamera.transform.position;
			
				//sky sizing based on camera
				if (useCameraCam != null){
				if (currClipDistance != useCameraCam.farClipPlane){
					currClipDistance = useCameraCam.farClipPlane;
					//setSkySize = currClipDistance;
					setSkyUseSize = currClipDistance/20.0f;

					sunSphereObject.transform.localScale = new Vector3(setSkyUseSize,setSkyUseSize,setSkyUseSize);
					moonSphereObject.transform.localScale = new Vector3(setSkyUseSize,setSkyUseSize,setSkyUseSize);

					starfieldObject.transform.localScale = new Vector3(setSkyUseSize,setSkyUseSize,setSkyUseSize);
					starGalaxyObject.transform.localScale = new Vector3(0.5f,0.5f,0.5f);

				}
				}
			}
		}
	}







	void LateUpdate () {

	//if (Application.isPlaying){

		//SET VERSION NUMBER
		tenkokuVersionNumber = "1.1.1";

		//UPDATE SYSTEM TIMER
		//tenSystemTime += Time.deltaTime;
		//Shader.SetGlobalFloat("_tenkokuTimer", tenSystemTime);


		// PLAY MODE SPECIFIC
		//if (Application.isPlaying){
			
			//get main camera object
			if (cameraTypeIndex == 0){
				if (Camera.main != null){
					mainCamera = Camera.main.transform;
				}
				manualCamera = null;
			}
			if (cameraTypeIndex == 1){
				if (manualCamera != null){
					mainCamera = manualCamera;
				} else {
					if (Camera.main != null){
						mainCamera = Camera.main.transform;
					}
				}
			}
			if (useCamera != mainCamera){ 
				useCamera = mainCamera;
			}


		if (Application.isPlaying){
			//set project layer masks
			tenLayerMasks = new List<string>();
			for (i = 0; i < 32; i++){
				layerName = LayerMask.LayerToName(i);
				tenLayerMasks.Add(layerName);
			}
		}


		//TURN OFF BUILT-IN FOG
		if (enableFog) UnityEngine.RenderSettings.fog = false;

		//TURN OFF ANTI ALIASING
		if (enableFog) QualitySettings.antiAliasing = 0;

		//SYNC TIME TO SYSTEM
		if (Application.isPlaying){
			if (autoTimeSync || autoDateSync){

				sysTime = System.DateTime.Now;
				
				if (autoTimeSync){
					currentHour = Mathf.FloorToInt(sysTime.Hour);
					currentMinute = Mathf.FloorToInt(sysTime.Minute);
					currentSecond = Mathf.FloorToInt(sysTime.Second);
					useTimeCompression = 1.0f;
				}
				if (autoDateSync){
					currentYear = Mathf.FloorToInt(sysTime.Year);
					currentMonth = Mathf.FloorToInt(sysTime.Month);
					currentDay = Mathf.FloorToInt(sysTime.Day);
				}

				

			} else {
				if (!doTransition){
					useTimeCompression = timeCompression * curveVal;
				}
			}
		}


		//SET LIGHTING LAYERS
		if (Application.isPlaying){
			if (lightObjectWorld.gameObject.activeInHierarchy){
				lightObjectWorld.cullingMask = lightLayer;
				lightObjectNight.cullingMask = lightLayer;
			}
		}


	    // -----------------------------
		// --   GET CAMERA OBJECTS   ---
		// -----------------------------

		if (Application.isPlaying){
			if (useCameraCam == null && mainCamera != null){
				useCamera = mainCamera;
				useCameraCam = useCamera.GetComponent<Camera>();
				useCameraCam.clearFlags = CameraClearFlags.Skybox;
			}
		}

		if (useCameraCam != null){
			MV = useCameraCam.worldToCameraMatrix.inverse;
			Shader.SetGlobalMatrix("_Tenkoku_CameraMV",MV);
		}

		if (useCamera != mainCamera && mainCamera != null){
			useCamera = mainCamera;
			useCameraCam = useCamera.GetComponent<Camera>();
		}



	    // -------------------------------
		// --   SET REFLECTION PROBE   ---
		// -------------------------------
		if (tenkokuReflectionObject != null){

			if (enableProbe && Application.isPlaying){

				//enable probe during play mode
				tenkokuReflectionObject.enabled = true;
				tenkokuReflectionObject.mode = UnityEngine.Rendering.ReflectionProbeMode.Realtime;
				tenkokuReflectionObject.refreshMode = UnityEngine.Rendering.ReflectionProbeRefreshMode.ViaScripting;

				//set size to world size
				if (useCameraCam != null){
					tenkokuReflectionObject.size = new Vector3(useCameraCam.farClipPlane*2f,useCameraCam.farClipPlane,useCameraCam.farClipPlane*2f);
				}

				//handle probe update when set to "scripting"
				if (tenkokuReflectionObject.refreshMode == UnityEngine.Rendering.ReflectionProbeRefreshMode.ViaScripting){
					reflectionTimer += Time.deltaTime;
					if (reflectionTimer >= (1.0f/reflectionProbeFPS)){
						tenkokuReflectionObject.RenderProbe(null);
						reflectionTimer = 0.0f;
					}
				}


			} else {
				//disable probe update during scene-mode
				tenkokuReflectionObject.mode = UnityEngine.Rendering.ReflectionProbeMode.Baked;
				tenkokuReflectionObject.refreshMode = UnityEngine.Rendering.ReflectionProbeRefreshMode.OnAwake;
			}
			
		}



	    // --------------------
		// --   SET FLAGS   ---
		// --------------------
		//set ible active
		iblActive = 0.0f;

		if (iblActive != currIblActive){
			currIblActive = iblActive;
			Shader.SetGlobalFloat("_tenkokuIBLActive",iblActive);
		}
		
		//Set Linear Mode on Shader
		isLin = 0.0f;
		if (QualitySettings.activeColorSpace == ColorSpace.Linear) isLin = 1.0f;
		if (isLin != currIsLin){
			currIsLin = isLin;
			Shader.SetGlobalFloat("_tenkokuIsLinear",isLin);
		}




	    // --------------------------------
		// --   CALCULATE WEATHER   ---
		// --------------------------------
		if (weatherTypeIndex == 1){

			//calculate weather pattern timer and initialize update
			doWeatherUpdate = false;
			weather_PatternTime += Time.deltaTime;
					
			//inherit variables
			if (currentWeatherTypeIndex != weatherTypeIndex ){
				currentWeatherTypeIndex = weatherTypeIndex;
				doWeatherUpdate = true;
			}

			//check for weather update
			if (weather_PatternTime > (weather_autoForecastTime*60.0f) && weather_autoForecastTime >= 0.05f) doWeatherUpdate = true;
			if (weather_forceUpdate) doWeatherUpdate = true;
			
			if (doWeatherUpdate){
				weather_forceUpdate = false;
				weather_PatternTime = 0.0f;
				
				//determine next random weather pattern
				w_isCloudy = Mathf.Clamp(TenRandom.Next(-0.25f,1.5f),0.0f,1.0f);
				
				//record current weather
				w_cloudAmts.y = weather_cloudAltoStratusAmt;
				w_cloudAmts.z = weather_cloudCirrusAmt;
				w_cloudAmtCumulus = weather_cloudCumulusAmt;
				w_overcastAmt = weather_OvercastAmt;
				w_windCAmt = weather_cloudSpeed;
				w_windAmt = weather_WindAmt;
				w_rainAmt = weather_RainAmt;
				w_lightningAmt = weather_lightning;

				//set clear weather default
				w_cloudTgts = new Vector4(0f,0f,0f,0f);
				w_cloudTgtCumulus = 0.0f;
				w_overcastTgt = 0.0f;
				w_windCTgt = 0.0f;
				w_windTgt = 0.0f;
				w_rainTgt = 0.0f;
				w_lightningTgt = 0.0f;

				//set clouds
				w_cloudTgts.y = Mathf.Clamp(Mathf.Lerp(0.0f,TenRandom.Next(0.0f,0.4f),w_isCloudy),0.0f,0.4f); //AltoCumulus
				w_cloudTgts.z = Mathf.Clamp(Mathf.Lerp(0.0f,TenRandom.Next(0.0f,0.8f),w_isCloudy),0.0f,0.8f); //Cirrus
				w_cloudTgtCumulus = Mathf.Clamp(Mathf.Lerp(0.0f,TenRandom.Next(0.1f,1.25f),w_isCloudy),0.0f,1.0f); // Cumulus
				if (w_cloudTgtCumulus > 0.8f){
					w_overcastTgt = Mathf.Clamp(Mathf.Lerp(0.0f,TenRandom.Next(-1.0f,1.0f),w_isCloudy),0.0f,1.0f); // overcast amount
				}
				
				//set weather
				chanceOfRain = Mathf.Clamp(TenRandom.Next(-1.0f,1.0f),0.0f,1.0f);
				w_rainTgt = Mathf.Lerp(0.0f,TenRandom.Next(0.2f,1.4f),w_overcastTgt*chanceOfRain);
				w_lightningTgt = Mathf.Lerp(0.0f,TenRandom.Next(0.0f,0.6f),w_overcastTgt*chanceOfRain);

				//set wind
				w_windCTgt = TenRandom.Next(0.1f,0.3f)*0.1f;
				w_windTgt = TenRandom.Next(0.1f,0.5f)+Mathf.Lerp(0.0f,0.5f,weather_OvercastAmt);
				
			}

			//set weather systems
			weather_cloudAltoStratusAmt = Mathf.SmoothStep(w_cloudAmts.y,w_cloudTgts.y,(weather_PatternTime/(weather_TransitionTime*60.0f)));
			weather_cloudCirrusAmt = Mathf.SmoothStep(w_cloudAmts.z,w_cloudTgts.z,(weather_PatternTime/(weather_TransitionTime*60.0f)));
			weather_cloudCumulusAmt = Mathf.SmoothStep(w_cloudAmtCumulus,w_cloudTgtCumulus,(weather_PatternTime/(weather_TransitionTime*60.0f)));
			weather_OvercastAmt = Mathf.SmoothStep(w_overcastAmt,w_overcastTgt,(weather_PatternTime/(weather_TransitionTime*60.0f)));
			weather_cloudSpeed = Mathf.SmoothStep(w_windCAmt,w_windCTgt,(weather_PatternTime/(weather_TransitionTime*60.0f)));
			weather_WindAmt = Mathf.SmoothStep(w_windAmt,w_windTgt,(weather_PatternTime/(weather_TransitionTime*60.0f)));
			weather_RainAmt = Mathf.SmoothStep(w_rainAmt,w_rainTgt,(weather_PatternTime/(weather_TransitionTime*60.0f))*2.0f);
			weather_lightning = Mathf.SmoothStep(w_lightningAmt,w_lightningTgt,(weather_PatternTime/(weather_TransitionTime*60.0f))*2.0f);

			//extra modifiers
			weather_RainAmt *= Mathf.Lerp(-1.0f,1.0f,weather_OvercastAmt);
			weather_lightning *= Mathf.Lerp(-3.0f,0.5f,weather_OvercastAmt);
			volumeAmbDay = Mathf.Lerp(1.0f,-2.0f,weather_OvercastAmt);

		}


		//Handle Temperature
		Shader.SetGlobalFloat("_Tenkoku_HeatDistortAmt",Mathf.Lerp(0.0f,0.04f,Mathf.Clamp(weather_temperature-90.0f,0.0f,120.0f)/30.0f));


		//Handle Rainbow Rendering
		rOC = 1.0f-Mathf.Clamp(weather_OvercastAmt*2f,0.0f,1.0f);
		Shader.SetGlobalFloat("_tenkoku_rainbowFac1",Mathf.Clamp(Mathf.Lerp(0.0f,1.25f,weather_rainbow),0.0f,rOC));
		Shader.SetGlobalFloat("_tenkoku_rainbowFac2",Mathf.Clamp(Mathf.Lerp(-2.0f,1.0f,weather_rainbow),0.0f,rOC));



	    // --------------------
		// --   SET sOUND   ---
		// --------------------
		HandleGlobalSound();
		

	    // -------------------------------------
		// --   CALCULATE WIND COORDINATES   ---
		// -------------------------------------
		weather_WindCoords = TenkokuConvertAngleToVector(weather_WindDir);
		tenkokuWindObject.transform.eulerAngles = new Vector3(tenkokuWindObject.transform.eulerAngles.x,weather_WindDir,tenkokuWindObject.transform.eulerAngles.z);
		tenkokuWindObject.windMain = weather_WindAmt;
		tenkokuWindObject.windTurbulence = Mathf.Lerp(0.0f,0.4f,weather_WindAmt);
		tenkokuWindObject.windPulseMagnitude = Mathf.Lerp(0.0f,2.0f,weather_WindAmt);
		tenkokuWindObject.windPulseFrequency = Mathf.Lerp(0.0f,0.4f,weather_WindAmt);


	    // --------------------------------
		// --   SET ECLIPTIC ROTATION   ---
		// --------------------------------
	    //yAmt = ((calcComponent.yamt+calcComponent.day)/365.242f)*2.0f;
	    //setEcliptic = Mathf.PingPong(yAmt*23.4f*2f,23.4f*2f);
	    //setEcliptic = Mathf.Sin(((calcComponent.yamt+calcComponent.day)/(365.242f*0.15915f)))*15f;
		useLatitude = setLatitude;



	    // ----------------------------
		// --   SET DAYLIGHT TIME   ---
		// ----------------------------
		calcTime = Vector3.Angle((sunObject.transform.position - skyObject.transform.position),skyObject.transform.up) / 180.0f;
		calcTime = 1.0f-calcTime;
		setTime = calcTime*1.0f*colorRamp.width;
		stepTime = Mathf.FloorToInt(setTime);
		timeLerp = setTime - stepTime;


	    // ----------------------------
		// --   SET MOONLIGHT TIME   ---
		// ----------------------------
		calcTimeM = Vector3.Angle((moonObject.transform.position - skyObject.transform.position),skyObject.transform.up) / 180.0f;
		calcTimeM = 1.0f-calcTimeM;
		setTimeM = calcTimeM*1.0f*colorRamp.width;
		stepTimeM = Mathf.FloorToInt(setTimeM);
		timeLerpM = setTimeM - stepTimeM;


		
		// --------------
		// --   SUN   ---
		// --------------
		//if (sunTypeIndex == 2){
		//	sunIsVisible = false;
		//} else {
		//	sunIsVisible = true;
		//}
		

			sunSphereObject.gameObject.SetActive(true);
	    	sunObject.gameObject.SetActive(true);
		    sunlightObject.gameObject.SetActive(true);
		    

		   	//solar ecliptic movement
			calcComponent.CalculateNode(1); //sunstatic
			//if (calcComponent.azimuth > 180.0f){
				//eclipticAngle = (calcComponent.azimuth);
			//} else {
				//eclipticAngle = (calcComponent.azimuth);
			//}
			eclipticoffset = 0.0f;

		    sunObject.transform.localPosition = new Vector3(-0.51f,0f,0f);
		    sunSphereObject.transform.localPosition = new Vector3(0f,0f,0f);

			//sun initial position and light
			//direction based on Solar Day Calculation
		    calcComponent.CalculateNode(2); //sun
			sunSphereObject.transform.localEulerAngles = new Vector3(0.0f, 90f+calcComponent.azimuth+setRotation+0.5f, -180.0f+calcComponent.altitude-0.5f);


		    //sun apparent horizon size
		    //due to optical atmospheric refraction as well as relative viusal proportion artifacts
		    //at the horizon, the sun appears larger at the horizon than it does while overhead.
		    //we're artificially changing the size of the sun to simulate this visual trickery.
		    sSDiv = Mathf.Abs((sunSphereObject.transform.eulerAngles.z-180.0f)/180.0f);
		    if (sSDiv == 0.0f) sSDiv = 1.0f;
		    if (sSDiv < 0.5f && sSDiv > 0.0f) sSDiv = 0.5f + (0.5f - sSDiv);
		    sunCalcSize = sunSize + Mathf.Lerp(0.0f,(sunSize * Mathf.Lerp(-2.0f,2.0f,sSDiv)),Mathf.Lerp(-1.0f,1.0f,sSDiv));
		    
		    //set explicit size of sun in Custom mode
		    sunCalcSize = sunSize*0.5f;
		    Shader.SetGlobalFloat("_Tenkoku_SunSize",sunSize+0.005f);

			sunPos.x = sunObject.transform.position.x/360.0f;
			sunPos.y = sunObject.transform.position.y/360.0f;
			sunPos.z = sunObject.transform.position.z/360.0f;
			Shader.SetGlobalColor("_Tenkoku_SunPos", new Vector4(sunPos.x,sunPos.y,sunPos.z,0.0f));

			sunObject.transform.localScale = new Vector3(sunCalcSize,sunCalcSize,sunCalcSize);

		
		    //solar parhelion
		    //models the non-circular orbit of the earth.  The sun is not a constant distance
		    //varying over the course of the year, this offsets the perceived sunset and
		    //sunrise times from where they would be given a perfectly circular orbit.
		    //--- no code yet ---
		    
		    //sun color
		    colorSun = DecodeColorKey("sun");
		    //sunColor = colorSun * (ambientCol.r);

		    //sun lightsource, point toward camera
		    if (useCamera != null){
		    	sunlightObject.transform.LookAt(useCamera.transform);
		    }





	    // ---------------
		// --   MOON   ---
		// ---------------

		if (moonTypeIndex == 2){
			moonIsVisible = false;
		} else {
			moonIsVisible = true;
		}
		
		if (!moonIsVisible){
		    renderObjectMoon.enabled = false;
		} else {
			renderObjectMoon.enabled = true;
		}
		

		//Moon Realistic Movement
	    //moon position and light direction
	    //rotate the moon around a spherical axis centered on the earth
		moonObject.transform.localPosition = new Vector3(-0.51f,0f,0f);
		moonSphereObject.transform.localPosition = new Vector3(0f,0f,0f);

		calcComponent.CalculateNode(3); //moon
		moonSphereObject.transform.localEulerAngles = new Vector3(0f, 90f+calcComponent.azimuth+setRotation+0.15f, -180f+calcComponent.altitude-0.5f);

		//moonAngle = 0.0f;
	    //if (moonTypeIndex == 1){
	    	//Moon Simple Movement
	    //	moonAngle = moonPos;
	    //} else {
	    	//Realistic Moon Movement
		//	moonAngle = 0.0f;
		//}
		




		//CALCULATE MOON LIGHT AND PHASE

		//Calculate Moon Light Coverage
		moonLightAmt = Quaternion.Angle(sunlightObject.transform.rotation,moonlightObject.transform.rotation) / 180.0f;

		// Manual Quaternion Angle Construction
		// We do this instead of Unity's Quaternion.Angle() function because we don't want it automatically clamped.
		mres = moonlightObject.transform.rotation * Quaternion.Inverse(sunlightObject.transform.rotation);
		moonPhase =  (Mathf.Acos(mres.w) * 2.0f * 57.2957795f) / 360f;




	    //moon libration
	    //as the moon travels across the lunar month it appears to wobbleback and
	    //forth as it transits from one phase to another.  This is called "libration"
	    //var monthAngle = (monthValue);
	    //if (monthAngle == 0.0) monthAngle = 1.0;
	    //if (monthAngle < 0.5 && monthAngle > 0.0) monthAngle = 0.5 + (0.5 - monthAngle);
	    //moonObject.transform.localEulerAngles.z = 0.0-Mathf.SmoothStep(336.0,384.0,Mathf.Lerp(-1.0,1.0,moonClipAngle));


	    //moon horizon size - turned off for now
	    //due to optical atmospheric refraction as well as relative viusal proportion artifacts
	    //at the horizon, the moon appears larger at the horizon than it does while overhead.
	    //we're artificially changing the size of the moon to simulate this visual trickery.
	    //also due to libration the moon also appears larger and smaller over the course of it's phase.
	    //var mSDiv : float = Mathf.Abs(moonSphereObject.transform.eulerAngles.z/180.0);
	    //if (mSDiv == 0.0) mSDiv = 1.0;
	    //if (mSDiv < 0.5 && mSDiv > 0.0) mSDiv = 0.5 + (0.5 - mSDiv);


	     //set explicit size of sun in Custom mode
	    moonCalcSize = moonSize*1.21f;

	    // moon apogee and perigree
	    moonApoSize = Mathf.Lerp(1.2f,0.82f,calcComponent.moonApogee);
	    moonCalcSize *=  moonApoSize;
	    moonObject.transform.localScale = new Vector3(moonCalcSize,moonCalcSize,moonCalcSize);

	    //moon color
		colorMoon = DecodeColorKey("moon")*1.0f;
		colorSkyAmbient = DecodeColorKey("skyambient");

		     
	    moonLightIntensity = Mathf.Clamp(moonLightIntensity,0.0f,1.0f);
	    //moonInt = monthValue;
	    //if (monthValue < 0.5f) moonInt = (1.0f - monthValue);
	    //moonInt = sunSphereObject.transform.localEulerAngles.z / 360.0f;
	    //moonInt = 1.0f;

	    //moonWorlIntensity = 1.0f * (1.0f-ambientCol.r) * colorMoon.r;

	    //moon day fade
	    // we apply an extra fade during daylight hours so the moon recedes into the sky.
	    // it is still visible, yet it's visiblilty is superceded by the bright atmosphere.
	    moonDayIntensity = Mathf.Clamp(moonDayIntensity,0.0f,1.0f);
	    mDCol = new Color(0.5f,0.5f,0.5f,1.0f);
	    mDCol.a = Mathf.Lerp(1.0f,moonDayIntensity,colorSkyAmbient.r);

	    renderObjectMoon.sharedMaterial.SetColor("_Color", Color.Lerp(mDCol,(ambientCol*colorSun),ambientCol.r)*mDCol);
		renderObjectMoon.sharedMaterial.SetColor("_AmbientTint",ambientCol);

		//moon light intensity
	    //setMCol = moonWorlIntensity;
		renderObjectMoon.sharedMaterial.SetColor("_AmbientTint",colorSkyAmbient);

		//set moon face light color
		moonFaceCol = colorMoon*(1.0f-DecodeColorKey("ambient").r);
		Shader.SetGlobalColor("Tenkoku_MoonLightColor",moonFaceCol);




	    // ---------------------------------
		// --   LIGHTNING HANDLING   ---
		// ---------------------------------

	 	//SET SKY CAMERA POSITIONS
		if (useCamera != null){
			//set global shader for camera forward direction
			Shader.SetGlobalVector("Tenkoku_Vec_CameraFwd", useCamera.transform.forward);
			Shader.SetGlobalVector("Tenkoku_Vec_SunFwd", -sunlightObject.transform.forward); 
			Shader.SetGlobalVector("Tenkoku_Vec_MoonFwd", -moonlightObject.transform.forward);
		}	
		
		



	    // ------------------
		// --   PLANETS   ---
		// ------------------
		useVec = new Vector3(0f,0.35f,0.75f);
		useMult = 1.0f;
		pVisFac = Mathf.Lerp(0.75f,0.1f,colorSkyAmbient.r);
		
		if (Application.isPlaying){

			//enable visibility
			if (planetRendererSaturn != null) planetRendererSaturn.enabled = true;
			if (planetRendererJupiter != null) planetRendererJupiter.enabled = true;
			if (planetRendererNeptune != null) planetRendererNeptune.enabled = true;
			if (planetRendererUranus != null) planetRendererUranus.enabled = true;
			if (planetRendererMercury != null) planetRendererMercury.enabled = true; 
			if (planetRendererVenus != null) planetRendererVenus.enabled = true;
			if (planetRendererMars != null) planetRendererMars.enabled = true;
	    

			//set planetary positions
			if (planetObjMercury){
			calcComponent.CalculateNode(4); //mercury
			planetObjMercury.transform.localPosition = new Vector3(0f,0f,0f);
			planetObjMercury.transform.localEulerAngles = new Vector3(0f, 90.0f+calcComponent.azimuth+setRotation, -180.0f+calcComponent.altitude-0.5f);
			tempTrans = planetObjMercury.transform;
			tempTrans.localPosition = new Vector3(0f,0f,0f);
			tempTrans.Translate(useVec * (eclipticoffset*useMult), Space.World);
		    planetObjMercury.planetOffset = tempTrans.localPosition;
		 	planetObjMercury.planetVis = pVisFac * planetIntensity;
		 	}

		 	if (planetObjVenus){
		 	calcComponent.CalculateNode(5); //venus
			planetObjVenus.transform.localPosition = new Vector3(0f,0f,0f);
			planetObjVenus.transform.localEulerAngles = new Vector3(0f, 90.0f+calcComponent.azimuth+setRotation, -180.0f+calcComponent.altitude-0.5f);
			tempTrans = planetObjVenus.transform;
			tempTrans.localPosition = new Vector3(0f,0f,0f);
			tempTrans.Translate(useVec * (eclipticoffset*useMult), Space.World);
		    planetObjVenus.planetOffset = tempTrans.localPosition;
		 	planetObjVenus.planetVis = Mathf.Clamp(pVisFac,0.1f,1.0f) * planetIntensity;
		 	}

		 	if (planetObjMars){
		  	calcComponent.CalculateNode(6); //mars
			planetObjMars.transform.localPosition = new Vector3(0f,0f,0f);
			planetObjMars.transform.localEulerAngles = new Vector3(0f, 90.0f+calcComponent.azimuth+setRotation, -180.0f+calcComponent.altitude-0.5f);
			tempTrans = planetObjMars.transform;
			tempTrans.localPosition = new Vector3(0f,0f,0f);
			tempTrans.Translate(useVec * (eclipticoffset*useMult), Space.World);
		    planetObjMars.planetOffset = tempTrans.localPosition;
		 	planetObjMars.planetVis = Mathf.Clamp(pVisFac,0.02f,1.0f) * planetIntensity;
		 	}

		 	if (planetObjJupiter){
		  	calcComponent.CalculateNode(7); //jupiter
			planetObjJupiter.transform.localPosition = new Vector3(0f,0f,0f);
			planetObjJupiter.transform.localEulerAngles =  new Vector3(0f, 90.0f+calcComponent.azimuth+setRotation, -180.0f+calcComponent.altitude-0.5f);
			tempTrans = planetObjJupiter.transform;
			tempTrans.localPosition = new Vector3(0f,0f,0f);
			tempTrans.Translate(useVec * (eclipticoffset*useMult), Space.World);
		    planetObjJupiter.planetOffset = tempTrans.localPosition;
		    planetObjJupiter.planetVis = Mathf.Clamp(pVisFac,0.05f,1.0f) * planetIntensity;
		    }

		    if (planetObjSaturn){
		 	calcComponent.CalculateNode(8); //saturn
			planetObjSaturn.transform.localPosition = new Vector3(0f,0f,0f);
			planetObjSaturn.transform.localEulerAngles = new Vector3(0f, 90.0f+calcComponent.azimuth+setRotation, -180.0f+calcComponent.altitude-0.5f);
			tempTrans = planetObjSaturn.transform;
			tempTrans.localPosition = new Vector3(0f,0f,0f);
			tempTrans.Translate(useVec * (eclipticoffset*useMult), Space.World);
		    planetObjSaturn.planetOffset = tempTrans.localPosition;
		  	planetObjSaturn.planetVis = pVisFac * planetIntensity;
		    }

		    if (planetObjUranus){
			calcComponent.CalculateNode(9); //uranus
			planetObjUranus.transform.localPosition = new Vector3(0f,0f,0f);
			planetObjUranus.transform.localEulerAngles = new Vector3(0f, 90.0f+calcComponent.azimuth+setRotation, -180.0f+calcComponent.altitude-0.5f);
			tempTrans = planetObjUranus.transform;
			tempTrans.localPosition = new Vector3(0f,0f,0f);
			tempTrans.Translate(useVec * (eclipticoffset*useMult), Space.World);
		    planetObjUranus.planetOffset = tempTrans.localPosition; 
			planetObjUranus.planetVis = pVisFac * planetIntensity;
			}

			if (planetObjNeptune){
			calcComponent.CalculateNode(10); //neptune
			planetObjNeptune.transform.localPosition = new Vector3(0f,0f,0f);
			planetObjNeptune.transform.localEulerAngles = new Vector3(0f, 90.0f+calcComponent.azimuth+setRotation, -180.0f+calcComponent.altitude-0.5f);
			tempTrans = planetObjNeptune.transform;
			tempTrans.localPosition = new Vector3(0f,0f,0f);
			tempTrans.Translate(useVec * (eclipticoffset*useMult), Space.World);
		    planetObjNeptune.planetOffset = tempTrans.localPosition; 
		    planetObjNeptune.planetVis = pVisFac * planetIntensity;
		    }
		    
	    }


	    // ----------------
		// --   STARS   ---
		// ----------------

	    //SIDEAREAL DAY CALCULATION
		//The sideareal day is more accurate than the solar day, and is
		//the actual determining calculation for the passage of a full year.
		if (starTypeIndex == 0 || starTypeIndex == 2){
		
		    starAngle = -1.0f;
		    starTarget = Quaternion.Euler((useLatitude), 183.0f, -starAngle);
		    starfieldObject.transform.rotation = starTarget;
			starfieldObject.transform.eulerAngles = new Vector3(starfieldObject.transform.eulerAngles.x,starfieldObject.transform.eulerAngles.y, -(((calcComponent.UT/23.99972f))*360.0f) - ((setLongitude)+95.0f) - (360.0f*Mathf.Abs(Mathf.Floor(calcComponent.day/365.25f)-(calcComponent.day/365.25f))) );

		    //CALCULATE PRECESSIONAL MOVEMENT
			//precessional movement occurs at a rate of 1 degree every 71.666 years, thus each precessional epoch
			//takes up exactly 32 degrees on the spherical celestial star plane. this is calculated on the
			//assumption that the Aquarial age begins in the year 2600ad.  if you disagree and would like to
			//see other aquarian start calibrations (year 2148 for example), simply change the 'aquarialStart' variable.
			aquarialStart = 2600;
			precRate = 71.6f;
			precNudge = 258.58f; //this value simply helps set the exact position of the star sphere.
			precFactor = ((aquarialStart/precRate)*(aquarialStart))+((aquarialStart-currentYear)/precRate)+precNudge;
			starfieldObject.transform.eulerAngles = new Vector3(starfieldObject.transform.eulerAngles.x,starfieldObject.transform.eulerAngles.y, starfieldObject.transform.eulerAngles.z-precFactor-4.0f);

		} else {
			starfieldObject.transform.eulerAngles = new Vector3(starfieldObject.transform.eulerAngles.x, starfieldObject.transform.eulerAngles.y, starPos);
		}

		starfieldObject.transform.eulerAngles = new Vector3(starfieldObject.transform.eulerAngles.x, setRotation, starfieldObject.transform.eulerAngles.z);

				

	    //solar ecliptic movement
	    eclipticStarAngle = (starValue);
	    if (eclipticStarAngle == 0.0) eclipticStarAngle = 1.0f;
	    if (eclipticStarAngle < 0.5f && eclipticStarAngle > 0.0f) eclipticStarAngle = 0.5f + (0.5f - eclipticStarAngle);
	    eclipticStarAngle += 1.35f;

	   
	    //GALAXY / Skybox Rendering
		renderObjectGalaxy.enabled = true;
		//useGalaxyIntensity = galaxyIntensity;
	    if (galaxyTypeIndex == 2 && starTypeIndex == 2){
			renderObjectGalaxy.enabled = false;
	    } else {
	    	renderObjectGalaxy.enabled = true;

	    	// Realistic Galaxy Rotation (match star positions)
			if (galaxyTypeIndex == 0 || galaxyTypeIndex == 2){
				starGalaxyObject.transform.localEulerAngles = new Vector3(18.1f,281.87f,338.4f);
				starGalaxyObject.transform.localScale = new Vector3(1f,0.92f,0.98f);
			}

			// Custom Galaxy Rotation
			if (galaxyTypeIndex == 1){
				starGalaxyObject.transform.localEulerAngles = new Vector3(starGalaxyObject.transform.localEulerAngles.x, starGalaxyObject.transform.localEulerAngles.y, galaxyPos);
			}

	    }
	    galaxyColor = new Color(1.0f,1.0f,1.0f,1.0f);
	   	galaxyVis = (1.0f-(colorSkyAmbient.r*1.0f)) * galaxyIntensity;

	    renderObjectGalaxy.sharedMaterial.SetFloat("_GIntensity",galaxyVis);
	    renderObjectGalaxy.sharedMaterial.SetColor("_Color",galaxyColor);

		renderObjectGalaxy.sharedMaterial.SetFloat("_useGlxy",galaxyTypeIndex*1.0f);
		renderObjectGalaxy.sharedMaterial.SetTexture("_GTex",galaxyTex);

		renderObjectGalaxy.sharedMaterial.SetFloat("_useCube",galaxyTexIndex*1.0f);
		renderObjectGalaxy.sharedMaterial.SetTexture("_CubeTex",galaxyCubeTex);

		renderObjectGalaxy.sharedMaterial.SetFloat("_useStar",starTypeIndex*1.0f);
	    renderObjectGalaxy.sharedMaterial.SetFloat("_SIntensity",starIntensity);


	    if (starRenderSystem != null){
	    	if (starTypeIndex == 2){
				if (starParticleSystem.enabled) starParticleSystem.enabled = false;
		    } else {
		    	if (!starParticleSystem.enabled) starParticleSystem.enabled = true;
	    		starRenderSystem.starBrightness.a = starIntensity;
		    }
	    }
	    
	    



	    // --------------
	    // --   SKY   ---   
	    //---------------                                                                                                                                                                                                                                                  
	    //get ramp colors
		ambientShift = Mathf.Clamp(ambientShift,0.0f,1.0f);
	    colorSkyBase = DecodeColorKey("skybase");
	    ambientCol = DecodeColorKey("skyambient");
	    ambientCloudCol = DecodeColorKey("ambientcloud");           
	                   
		setSkyAmbient = colorSkyAmbient;
		setSkyAmbient.r += ambientShift;
		setSkyAmbient.g += ambientShift;
		setSkyAmbient.b += ambientShift;
		useAlpha = new Color(1.0f,1.0f,1.0f, (1.0f-setSkyAmbient.r));
		
		setAmbCol = useAlpha;
		setAmbCol.a = useAlpha.a*0.75f;
		setAmbCol.a = useAlpha.a*1.5f;
		if (setAmbCol.a > 1.0f) setAmbCol.a = 1.0f;

		colorSkyBaseLow = colorSkyBase;
		colorHorizonLow = colorHorizon;
		
		colorSkyBase = (colorSkyBase * 0.8f);
		colorHorizon = Color.Lerp(colorHorizon,(colorSkyBase),0.9f)*1.1f;

		
		
		

	    // ------------------
	    // --   AMBIENT   ---   
	    //-------------------
		mixColor = colorSun;
		setAmbientColor.r = 1.0f-mixColor.r;
		setAmbientColor.g = 1.0f-mixColor.g;
		setAmbientColor.b = 1.0f-mixColor.b;
		medCol = new Color(0.3f,0.3f,0.3f,1.0f);
		useAmbientColor = Color.Lerp((Color.Lerp(colorSkyBase,setAmbientColor,0.7f)),new Color(0f,0f,0f,1f),0.12f);

		useAmbientColor = Color.Lerp(useAmbientColor,medCol,0.5f) * colorSkyAmbient;
		useAmbientColor *= (sunBright) * skyBrightness;
		
		//HORIZON
	    colorHorizon = DecodeColorKey("colorhorizon")*1.2f; 

		if (useAmbient){
			useAmbientColor = DecodeColorKey("ambientcolor");
			useColorAmbient = colorAmbient;

			useColorAmbient.a = colorAmbient.a * Mathf.Lerp(1.0f,0.0f,weather_OvercastAmt*4);
			useAmbientColor = Color.Lerp(useAmbientColor,new Color(0.5f,0.5f,0.5f,1f),weather_OvercastAmt*3);

			useAmbientColor = Color.Lerp(useAmbientColor,useAmbientColor*colorAmbient,useColorAmbient.a);


			// Unity 5 no longer respects the previous RenderSettings for ambient color.
			// this may be a bug, but in the meantime the below settings duplicate
			// the original intended behavior.
			//UnityEngine.RenderSettings.ambientMode = UnityEngine.Rendering.AmbientMode.Flat;
			//UnityEngine.RenderSettings.ambientSkyColor = Color.Lerp((new Color(0.38f,0.6f,0.69f,0.7f)*(nightBrightness*0.5f)), useAmbientColor, setSkyAmbient.r);
			//UnityEngine.RenderSettings.ambientIntensity = 1.0f;

			// the original intended behavior.
			UnityEngine.RenderSettings.ambientMode = UnityEngine.Rendering.AmbientMode.Trilight;
			UnityEngine.RenderSettings.ambientGroundColor = Color.Lerp((new Color(0.38f,0.6f,0.69f,0.7f)*(nightBrightness*0.5f)), useAmbientColor, setSkyAmbient.r);
			Color skyAmb = DecodeColorKey("colorhorizon")*0.6f;
			skyAmb = Color.Lerp(skyAmb, new Color(1f,1f,1f,1f)*Mathf.Lerp(0.25f,1.75f,nightBrightness*setSkyAmbient.r), weather_OvercastAmt*2);

			UnityEngine.RenderSettings.ambientSkyColor = skyAmb*(Mathf.Lerp(nightBrightness+Mathf.Lerp(0.25f,1.75f,nightBrightness), 1f, setSkyAmbient.r));
			UnityEngine.RenderSettings.ambientEquatorColor = Color.Lerp(
				UnityEngine.RenderSettings.ambientGroundColor,
				UnityEngine.RenderSettings.ambientSkyColor,
				0.25f);
			
			UnityEngine.RenderSettings.ambientIntensity = 1.0f;



		}
		



	 
	    // ----------------------------------
	    // --      LENS FLARE EFFECTS       --
	    // -----------------------------------
	    /*
	    //set default flare
	    sunFlare.color = new Color(1f,1f,1f,1f);

	    //fade flare based on overcast
	    sunFlare.color = Color.Lerp(sunFlare.color,new Color(0f,0f,0f,0f),weather_OvercastAmt*2.0f);
		sunFlare.brightness = Mathf.Lerp(1.0f,0.0f,weather_OvercastAmt*2.0);

	    //fade flare based on gamma
	    sunFlare.color *= Mathf.Lerp(0.25f,1.0f,isLin);
		sunFlare.brightness *= Mathf.Lerp(0.25f,1.0f,isLin);

	    //fade flare based on brightness
	    sunFlare.color *= Mathf.Lerp(0.0f,1.0f,sunBright*0.6f);
		sunFlare.brightness *= Mathf.Lerp(0.0f,1.0f,sunBright*0.6f);

		//fade flare based on occlusion
		//(soon)
		*/





	    // --------------------------------
	    // --      WEATHER EFFECTS       --
	    // --------------------------------
	    if (Application.isPlaying){

		    //manage particle systems
			if (rainSystem != null){
				#if UNITY_5_3 || UNITY_5_4 || UNITY_5_6 || UNITY_5_7 || UNITY_5_8 || UNITY_5_9
					rainEmission = rainSystem.emission;
					rainEmission.enabled = true;
					rainEmission.rate = new ParticleSystem.MinMaxCurve(weather_RainAmt,Mathf.Lerp(2,5000,weather_RainAmt));
					if (rainSystem.particleCount <= 10){
						renderObjectRain.enabled = false;
					} else {
						renderObjectRain.enabled = true;
					}
					//add force
					rainForces = rainSystem.forceOverLifetime;
					rainForces.enabled = true;
					rainForces.x = new ParticleSystem.MinMaxCurve(-weather_WindCoords.x * (weather_WindAmt*46f));
					rainForces.y = new ParticleSystem.MinMaxCurve(weather_WindCoords.y * (weather_WindAmt*46f));
				#else
					renderObjectRain.enabled = true;	
					rainSystem.enableEmission = true;
					rainSystem.emissionRate = Mathf.FloorToInt(Mathf.Lerp(0,5000,weather_RainAmt));
				#endif
			}

			if (rainFogSystem != null){
				#if UNITY_5_3 || UNITY_5_4 || UNITY_5_6 || UNITY_5_7 || UNITY_5_8 || UNITY_5_9
					rainFogEmission = rainFogSystem.emission;
					rainFogEmission.enabled = true;
					rainFogEmission.rate = new ParticleSystem.MinMaxCurve(weather_RainAmt,Mathf.Lerp(2,800,weather_RainAmt));
					if (rainFogSystem.particleCount <= 20){
						renderObjectRainFog.enabled = false;
					} else {
						renderObjectRainFog.enabled = true;
					}
					//add force
					rainFogForces = rainFogSystem.forceOverLifetime;
					rainFogForces.enabled = true;
					rainFogForces.x = new ParticleSystem.MinMaxCurve(-weather_WindCoords.x * (weather_WindAmt*4f));
					rainFogForces.y = new ParticleSystem.MinMaxCurve(weather_WindCoords.y * (weather_WindAmt*4f));
				#else
					rainFogSystem.enableEmission = true;
					renderObjectRainFog.enabled = true;
					rainFogSystem.emissionRate = Mathf.FloorToInt(Mathf.Lerp(0,800,weather_RainAmt));
				#endif
			}

			if (rainSplashSystem != null){
				#if UNITY_5_3 || UNITY_5_4 || UNITY_5_6 || UNITY_5_7 || UNITY_5_8 || UNITY_5_9
					splashEmission = rainSplashSystem.emission;
					splashEmission.enabled = true;
					splashEmission.rate = new ParticleSystem.MinMaxCurve(weather_RainAmt,Mathf.Lerp(50,800,weather_RainAmt));
					if (rainSplashSystem.particleCount <= 10){
						renderObjectRainSplash.enabled = false;
					} else {
						renderObjectRainSplash.enabled = true;
					}
				#else
					rainSplashSystem.enableEmission = true;
					renderObjectRainSplash.enabled = true;
					rainSplashSystem.emissionRate = Mathf.FloorToInt(Mathf.Lerp(0,800,weather_RainAmt));
				#endif
			}

			if (fogSystem != null){
				#if UNITY_5_3 || UNITY_5_4 || UNITY_5_6 || UNITY_5_7 || UNITY_5_8 || UNITY_5_9
					fogEmission = fogSystem.emission;
					fogEmission.enabled = true;
					fogEmission.rate = new ParticleSystem.MinMaxCurve(weather_FogAmt,Mathf.Lerp(2,1200,weather_FogAmt));
					if (fogSystem.particleCount <= 10){
						renderObjectFog.enabled = false;
					} else {
						renderObjectFog.enabled = true;
					}
					//add force
					fogForces = fogSystem.forceOverLifetime;
					fogForces.enabled = true;
					fogForces.x = new ParticleSystem.MinMaxCurve(-weather_WindCoords.x * (weather_WindAmt*6));
					fogForces.y = new ParticleSystem.MinMaxCurve(weather_WindCoords.y * (weather_WindAmt*6));
				#else
					fogSystem.enableEmission = true;
					renderObjectFog.enabled = true;
					fogSystem.emissionRate = Mathf.FloorToInt(Mathf.Lerp(0,1200,weather_FogAmt));
				#endif
			}
			
			if (snowSystem != null){
				#if UNITY_5_3 || UNITY_5_4 || UNITY_5_6 || UNITY_5_7 || UNITY_5_8 || UNITY_5_9
					snowEmission = snowSystem.emission;
					snowEmission.enabled = true;
					snowEmission.rate = new ParticleSystem.MinMaxCurve(weather_SnowAmt,Mathf.Lerp(2,5000,weather_SnowAmt));
					if (snowSystem.particleCount <= 10){
						renderObjectSnow.enabled = false;
					} else {
						renderObjectSnow.enabled = true;
					}
					//add force
					snowSystem.transform.localPosition = new Vector3(snowSystem.transform.localPosition.x, Mathf.Lerp(0.24f,0.14f,weather_WindAmt), snowSystem.transform.localPosition.z);
					snowForces = snowSystem.forceOverLifetime;
					snowForces.enabled = true;
					snowForces.x = new ParticleSystem.MinMaxCurve(-weather_WindCoords.x * (weather_WindAmt*2f));
					snowForces.y = new ParticleSystem.MinMaxCurve(weather_WindCoords.y * (weather_WindAmt*2f));
				#else
					snowSystem.enableEmission = true;
					renderObjectSnow.enabled = true;
					snowSystem.emissionRate = Mathf.FloorToInt(Mathf.Lerp(0,2000,weather_SnowAmt));
				#endif

				//set system force (using 'weather_WindCoords')
				//not yet available, hopefully will be available in a Unity update
			}
		
		}


	    //Overcast Calculations
	    useOvercast = Mathf.Clamp(Mathf.Lerp(-1.0f,1.0f,weather_OvercastAmt),0.0f,1.0f);
		colorSkyBase = Color.Lerp(colorSkyBase,new Color(0.3f,0.4f,0.5f,1.0f)*ambientCol.r,useOvercast);
		colorHorizon = Color.Lerp(colorHorizon,new Color(0.7f,0.8f,0.9f,1.0f)*ambientCol.r,useOvercast);



		//apply color and lighting
		fxUseLight = Mathf.Clamp(4.0f,0.1f,0.8f);
		lDiff = ((lightObjectWorld.intensity - 0.0f)/0.68f);
		lDiff += Mathf.Lerp(0.0f,0.8f,useOvercast);
		fxUseLight = Mathf.Lerp(0.0f,lightObjectWorld.intensity,lDiff);

	    rainCol = new Color(0.65f,0.75f,0.75f,1.0f) * fxUseLight;
	    if (renderObjectRainSplash) renderObjectRain.sharedMaterial.SetColor("_TintColor",new Color(rainCol.r,rainCol.g,rainCol.b,0.12f)*Mathf.Lerp(0.35f,1.0f,ambientCol.r));
		 
	    splashCol = new Color(1.0f,1.0f,1.0f,1.0f) * fxUseLight;
	    if (renderObjectRainSplash) renderObjectRainSplash.sharedMaterial.SetColor("_TintColor",new Color(splashCol.r,splashCol.g,splashCol.b,0.65f));
	    
	    rainfogCol = new Color(1.0f,1.0f,1.0f,1.0f) * fxUseLight;
	   	rainfogFac = Mathf.Lerp(0.004f,0.025f,Mathf.Clamp01((weather_RainAmt-0.5f)*2.0f));	
	    if (renderObjectRainFog) renderObjectRainFog.sharedMaterial.SetColor("_TintColor",new Color(rainfogCol.r,rainfogCol.g,rainfogCol.b,rainfogFac));
	    
	    fogCol = Color.Lerp(new Color(1f,1f,1f,1f),new Color(1f,1f,1f,1f),0.6f) * fxUseLight;
	    fogFac = Mathf.Lerp(0.02f,0.04f,Mathf.Clamp01(weather_FogAmt));	
	    if (renderObjectFog) renderObjectFog.sharedMaterial.SetColor("_TintColor",new Color(fogCol.r,fogCol.g,fogCol.b,fogFac));
	   
	    snowCol = new Color(0.85f,0.85f,0.85f,1f) * fxUseLight;
	    if (renderObjectSnow) renderObjectSnow.sharedMaterial.SetColor("_TintColor",new Color(snowCol.r,snowCol.g,snowCol.b,0.45f));

	    //clamp weather systems
	    if (particleObjectRainFog && particleObjectRainFog.particleCount > 0.0f) ClampParticle("rain fog");
		if (particleObjectRainSplash && particleObjectRainSplash.particleCount > 0.0f) ClampParticle("rain splash");
		if (particleObjectFog && particleObjectFog.particleCount > 0.0f) ClampParticle("fog");



	    // --------------
	    // --   FOG   ---   
	    //---------------
		if (enableFog){
			Shader.SetGlobalFloat("_Tenkoku_FogStart",fogAtmosphere);
			Shader.SetGlobalFloat("_Tenkoku_FogEnd",fogDistance);
			Shader.SetGlobalFloat("_Tenkoku_FogDensity",fogDensity);

		    if (useCameraCam != null){
				Shader.SetGlobalFloat("_Tenkoku_shaderDepth",fogDistance/useCameraCam.farClipPlane);
			}


		} else {
			Shader.SetGlobalFloat("_Tenkoku_FogDensity",0.0f);
		}

	        

	    // ------------------------
	    // --   CLOUD SPHERES   ---   
	    //-------------------------
	    
	    colorClouds = DecodeColorKey("colorcloud");  
	    colorHighlightClouds = DecodeColorKey("ambientcloud");  
	     
	    colorHighlightClouds = Color.Lerp(colorHighlightClouds,new Color(0.19f,0.19f,0.19f,1.0f),weather_OvercastAmt*2.0f) * skyAmbientCol;

	    colorClouds = Color.Lerp(colorClouds,colorClouds*colorOverlay,colorOverlay.a);
	    colorHighlightClouds = Color.Lerp(colorHighlightClouds,colorHighlightClouds*colorOverlay,colorOverlay.a);

	    
	    timerCloudMod = 1.0f;
	    if (cloudLinkToTime){
	    	if (!useAutoTime){
	    		timerCloudMod = 0.0f;
	    	} else {
	    		if (Application.isPlaying) timerCloudMod = useTimeCompression;
	    	}
	    }
		

		//calculate cloud rotation
		cloudRotSpeed = weather_cloudSpeed * timerCloudMod;
	    cloudOverRot += (Time.deltaTime * cloudRotSpeed * 0.005f);

		cloudOverRot = Mathf.Clamp(cloudOverRot,0.0f,1.0f);

	  	//SET CLOUD SHADER SETTINGS
	    if (renderObjectCloudPlane != null){

	    	//set wind/cloud speed
	    	cloudSpeeds.r = 0.1f * cloudOverRot; //altostratus
			cloudSpeeds.g = 0.7f * cloudOverRot; //Cirrus
			cloudSpeeds.b = 1.0f * cloudOverRot; //cumulus
			cloudSpeeds.a = 2.0f * cloudOverRot; //overcast

	    	renderObjectCloudPlane.sharedMaterial.SetColor("_cloudSpd",cloudSpeeds);
		   	currCoords.x = (currCoords.x+(Time.deltaTime*cloudRotSpeed*weather_WindCoords.x*0.08f));
		   	currCoords.y = (currCoords.y+(Time.deltaTime*cloudRotSpeed*weather_WindCoords.y*0.08f));


			Shader.SetGlobalColor("windCoords",new Vector4(currCoords.x,currCoords.y,0f,0f));

	   		renderObjectCloudPlane.sharedMaterial.SetFloat("_amtCloudS",weather_cloudAltoStratusAmt);
	   		renderObjectCloudPlane.sharedMaterial.SetFloat("_amtCloudC",weather_cloudCirrusAmt);
	   		renderObjectCloudPlane.sharedMaterial.SetFloat("_amtCloudO",Mathf.Clamp01(weather_OvercastAmt*2.0f));

			renderObjectCloudPlane.sharedMaterial.SetFloat("_sizeCloud",weather_cloudCumulusAmt);
	   		renderObjectCloudPlane.sharedMaterial.SetFloat("_cloudHeight",Mathf.Lerp(10.0f,70.0f,weather_cloudCumulusAmt));


			//set cloud scale
			if (useCameraCam != null){
				cloudPlaneObject.transform.localScale = new Vector3(0.5f,0.5f,0.1f);
				cloudPlaneObject.transform.localPosition = new Vector3(cloudPlaneObject.transform.localPosition.x, -1.0f, cloudPlaneObject.transform.localPosition.z);
				cloudSc = weather_cloudScale;
			}

	   		renderObjectCloudPlane.sharedMaterial.SetTextureScale("_MainTex", new Vector2(cloudSc,cloudSc));
	   		renderObjectCloudPlane.sharedMaterial.SetTextureScale("_CloudTexB", new Vector2(cloudSc,cloudSc));
	   		if (useCameraCam != null && renderObjectCloudPlane != null){
	   			renderObjectCloudPlane.sharedMaterial.SetFloat("_TenkokuDist",1200f);
	   		}
	   	}

	    

	    // ------------------------
	    // --   CLOUD OBJECTS   ---   
	    //-------------------------

	    setCloudFog1 = colorSkyBase*1.1f;
	    setCloudFog1.a = 1.0f;
	    setCloudFog1.r *= 0.65f;
	    setCloudFog1.g *= 0.8f;

		colorHorizon = Color.Lerp(colorHorizon,colorHorizon*0.5f,weather_OvercastAmt);
		//setCloudBase = Color.Lerp(colorHorizon*1.8f,colorHorizon*0.4f,weather_OvercastAmt);
		//setCloudTint = Color.Lerp(colorSkyBase*0.8f,colorSkyBase*0.2f,weather_OvercastAmt*3.0f);
	    setCloudFog1 = Color.Lerp(setCloudFog1,new Color(0.5f,0.5f,0.5f,1.0f),weather_OvercastAmt);



		
		// -------------------------------
	    // --   Set Global Tint Color   --
	    // -------------------------------
	    Shader.SetGlobalColor("_TenkokuSkyColor",DecodeColorKey("skybase"));
	    Shader.SetGlobalColor("tenkoku_globalTintColor",colorOverlay);
	    Shader.SetGlobalColor("tenkoku_globalSkyColor",colorSky);

	 	// -------------------------------
	    // --   Altitude Adjustment   --
	    // -------------------------------
	    // adjust colors and alpha based on altitude height attributes.
	    // can simulate entering and leaving planet atmosphere;
	    altAdjust = 5000.0f;
	    Shader.SetGlobalFloat("tenkoku_altitudeAdjust",altAdjust);
	    
	    
		// -------------------------
	    // --   BRDF Sky Object   --
	    // -------------------------
	    if (useCamera != null){
	    if (useCameraCam != null){
	    	fogDist = (1.0f/useCameraCam.farClipPlane)*(0.9f + fogDistance);
	    	fogFull = useCameraCam.farClipPlane;
	    }
		}
		skyAmbientCol = ambientCol * Mathf.Lerp(1.0f,0.7f,useOvercast);
		skyHorizonCol = colorHorizon * Mathf.Lerp(1.0f,0.8f,useOvercast);

		Shader.SetGlobalColor("_Tenkoku_SkyHorizonColor",skyHorizonCol);
		Shader.SetGlobalFloat("_tenkokufogFull",floatRound(fogFull));
		Shader.SetGlobalFloat("_tenkokufogStretch",floatRound(fogDist));
		Shader.SetGlobalFloat("_tenkokufogAtmospheric",floatRound(fogAtmosphere));
		Shader.SetGlobalFloat("_Tenkoku_SkyBright",floatRound(useSkyBright));
		Shader.SetGlobalFloat("_Tenkoku_NightBright",floatRound(nightBrightness));
		Shader.SetGlobalColor("_TenkokuAmbientColor",skyAmbientCol);

		//adjust and set atmospheric density based on day factor
		atmosTime = Vector3.Angle((sunObject.transform.position - skyObject.transform.position),skyObject.transform.up) / 180.0f;
		atmosTime = Mathf.Clamp(Mathf.Lerp(-0.75f,1.75f,(atmosTime)),0.0f,0.5f);

		setAtmosphereDensity = (horizonDensity * 2.0f * atmosTime);

		Shader.SetGlobalFloat("_Tenkoku_AtmosphereDensity",floatRound(atmosphereDensity));
		Shader.SetGlobalFloat("_Tenkoku_HorizonDensity",floatRound(setAtmosphereDensity));
		Shader.SetGlobalFloat("_Tenkoku_HorizonHeight",floatRound(horizonDensityHeight));

		if (enableFog){
			Shader.SetGlobalFloat("_Tenkoku_FogDensity",floatRound(fogDensity));
		}

		//overcast color
		overcastCol = DecodeColorKey("ambientcolor");
		overcastCol.a = weather_OvercastAmt;
		Shader.SetGlobalColor("_Tenkoku_overcastColor",overcastCol);



	    // assign colors
	    if (useCamera != null){
		    if (useCameraCam != null){
		    	useCameraCam.backgroundColor = DecodeColorKey("skybase")*Color.Lerp(new Color(1f,1f,1f,1f),new Color(0f,0f,0f,1f),atmosphereDensity*0.25f);
		    	useCameraCam.backgroundColor = Color.Lerp(new Color(0f,0f,0f,1f),useCameraCam.backgroundColor,Mathf.Clamp(atmosphereDensity*2f,0.0f,1.0f));
		    	useCameraCam.backgroundColor *= Mathf.Clamp(skyBrightness,0.0f,1.0f);
			}
		    nightSkyLightObject.transform.eulerAngles = new Vector3(60.0f,useCamera.transform.eulerAngles.y,useCamera.transform.eulerAngles.z);
		}

	    if (fogCameraCam != null){
	    	fogCameraCam.backgroundColor = DecodeColorKey("skybase")*Color.Lerp(new Color(1f,1f,1f,1f),new Color(0f,0f,0f,1f),atmosphereDensity*0.25f);
	    	fogCameraCam.backgroundColor = Color.Lerp(new Color(0f,0f,0f,1f),fogCameraCam.backgroundColor,Mathf.Clamp(atmosphereDensity*2f,0.0f,1.0f));
	    	fogCameraCam.backgroundColor *= Mathf.Clamp(skyBrightness,0.0f,1.0f);

			// OVER CAST FOG BLURRING
			if (weather_OvercastAmt != currWeather_OvercastAmt){
				currWeather_OvercastAmt = weather_OvercastAmt;
				fogVals = Mathf.FloorToInt(Mathf.Lerp(3.0f,6.0f,weather_OvercastAmt)).ToString();
				fogVals += ","+Mathf.Lerp(0.1f,2.0f,weather_OvercastAmt).ToString();
				fogCameraCam.SendMessage("SetFogValues",fogVals,SendMessageOptions.DontRequireReceiver);
			}
		}


		if (UnityEngine.RenderSettings.skybox != null){
			UnityEngine.RenderSettings.skybox.SetFloat("_TenkokuColorFac",Mathf.Lerp(1.0f,1.0f,weather_OvercastAmt));
			UnityEngine.RenderSettings.skybox.SetFloat("_TenkokuExposureFac",Mathf.Lerp(1.0f,1.0f,weather_OvercastAmt));
			UnityEngine.RenderSettings.skybox.SetColor("_GroundColor",colorSkyboxGround);
			
		} else {
			Debug.Log("Tenkoku Warning: No Skybox material has been set!");
		}



	    setOverallCol = colorSun*ambientCol*Color.Lerp(new Color(1f,1f,1f,1f),colorOverlay,colorOverlay.a);
		Shader.SetGlobalColor("_overallCol",setOverallCol);

		bgSCol = skyHorizonCol;
	    // convert camera color to linear space when appropriate
	    // the scene camera does not render background colors in linear space, so we need to
	    // convert the background color to linear before assigning to in-scene shaders.
	    if (QualitySettings.activeColorSpace == ColorSpace.Linear){
		    bgSCol.r = Mathf.GammaToLinearSpace(bgSCol.r);
		    bgSCol.g = Mathf.GammaToLinearSpace(bgSCol.g);
		    bgSCol.b = Mathf.GammaToLinearSpace(bgSCol.b);
		}
		Shader.SetGlobalColor("tenkoku_backgroundColor",bgSCol);

		
		//set GI Ambient
		ambientGI = DecodeColorKey("ambientGI");
		

		//handle gamma brightness
		useSkyBright = skyBrightness;
		if (QualitySettings.activeColorSpace != ColorSpace.Linear){
			useSkyBright *= 0.42f;
		}

		//assign blur settings
	    if (fogCameraBlur != null){
			fogCameraBlur.iterations = fogDisperse;
			fogCameraBlur.blurSpread = 0.1f;
		}

		useSkyBright = useSkyBright * Mathf.Lerp(1.0f,10.0f,ambientGI.r);

		// ------------------------
	    // --   Cloud Objects   --
	    // ------------------------
	    Shader.SetGlobalColor("_TenkokuCloudColor",colorClouds);
	    Shader.SetGlobalColor("_TenkokuCloudHighlightColor",colorHighlightClouds);

		//set cloud effect distance color
		Shader.SetGlobalFloat("_fogStretch",floatRound(fogDist));
		Shader.SetGlobalColor("_Tenkoku_SkyColor",colorSkyBase*ambientCloudCol.r*setOverallCol*useSkyBright);
		Shader.SetGlobalColor("_Tenkoku_HorizonColor",colorHorizon*setOverallCol*useSkyBright);
		Shader.SetGlobalFloat("_Tenkoku_Ambient",ambientCloudCol.r);
		Shader.SetGlobalFloat("_Tenkoku_AmbientGI",ambientGI.r);
		
		//set aurora settings
		auroraIsVisible = false;
		if (auroraTypeIndex == 0){
			auroraIsVisible = true;
		}

		
		if (!auroraIsVisible || auroraLatitude > Mathf.Abs(setLatitude)){
			renderObjectAurora.enabled = false;
		} else {
		 	renderObjectAurora.enabled = true;
		 	
		 	aurSpan = 30.0f;
		 	aurAmt = Mathf.Lerp(0.0f,1.0f,Mathf.Clamp((Mathf.Abs(setLatitude)-auroraLatitude)/aurSpan,0.0f,1.0f));
		 	Shader.SetGlobalFloat("_Tenkoku_AuroraSpd",auroraSpeed);
			Shader.SetGlobalFloat("_Tenkoku_AuroraAmt",auroraIntensity*aurAmt);	
		}
		

	    

	    // ----------------------------
	    // --   WORLD LIGHT OBJECT   --
	    // ----------------------------
	 	worldlightObject.transform.rotation = sunlightObject.transform.rotation;
	 	WorldCol = Color.Lerp(DecodeColorKey("sun") * ambientCol.r, new Color(ambientCol.r,ambientCol.r,ambientCol.r,1)*0.7f,Mathf.Clamp01(weather_OvercastAmt*2.0f));

		//tint light color with overall color
		WorldCol = WorldCol * Color.Lerp(new Color(1.0f,1.0f,1.0f,1.0f),colorOverlay,colorOverlay.a);

		sC = WorldCol.grayscale;
		WorldCol = Color.Lerp(new Color(sC,sC,sC,WorldCol.a), WorldCol, sunSat);

	 	lightObjectWorld.color = WorldCol;
	    lightObjectWorld.intensity = 2.0f * (sunBright*0.68f) * ambientCol.r;   
	   	//lightObjectWorld.shadowStrength = Mathf.Lerp(1.0f,0.25f,weather_OvercastAmt);

		Shader.SetGlobalColor("_TenkokuSunColor",WorldCol);


	    // ------------------------------
	    // --   HANDLE IMAGE EFFECTS   --
	    // ------------------------------
	    if (useCamera != null){

			lightVals = sunlightObject.transform.name;
			rayCol = DecodeColorKey("sunray");
			rayCol = Color.Lerp(rayCol,new Color(rayCol.r,rayCol.r,rayCol.r,rayCol.a), Mathf.Clamp(weather_OvercastAmt*2f,0.0f,1.0f));
			lightVals += ","+rayCol.r.ToString();
			lightVals += ","+rayCol.g.ToString();
			lightVals += ","+rayCol.b.ToString();

			setSun = "1";
			if (!useSunRays) setSun = "0";
			lightVals += ","+setSun;
			lightVals += ","+Mathf.Lerp(0.0f,20.0f,sunRayIntensity).ToString("#.0000");
			lightVals += ","+sunRayLength.ToString("#.0000");

			useCamera.SendMessage("SetLightValues",lightVals,SendMessageOptions.DontRequireReceiver);
	    }



	    // ----------------------------
	    // --   NIGHT LIGHT OBJECT   --
	    // ----------------------------
	 	nightSkyLightObject.transform.rotation = moonlightObject.transform.rotation;
	    lightObjectNight.intensity = Mathf.Lerp(0.0f,1.0f,(nightBrightness)*1.2f*(1.0f-ambientCol.r))+(moonBright*(1.0f-ambientCol.r)) * (1.0f-weather_OvercastAmt);


		mC = new Color(0.25f,0.33f,0.36f,1.0f).grayscale;
		lightObjectNight.color = Color.Lerp(new Color(mC,mC,mC,1.0f), new Color(0.25f,0.33f,0.36f,1.0f), moonSat);


		//modulate night intensity based on altitude
		if (lightObjectNight != null && moonSphereObject != null){

			//find the light factor based on the rotation of the light
			delta = moonSphereObject.transform.position - moonObject.transform.position;
			look = Quaternion.LookRotation(delta);
			vertical = look.eulerAngles.x;

			if (vertical > 90.0f) vertical = 90f - (vertical-90.0f);
			vertical = Mathf.Clamp01((vertical) / 10.0f); //degrees

			//set the light intensity
			lightObjectNight.intensity = lightObjectNight.intensity * Mathf.Lerp(0.0f,1.0f,vertical);

		}


		//modulate day intensity based on altitude
		if (lightObjectWorld != null && sunSphereObject != null){

			//find the light factor based on the rotation of the light
			delta = sunSphereObject.transform.position - sunObject.transform.position;
			look = Quaternion.LookRotation(delta);
			vertical = look.eulerAngles.x;
			
			sunVert = vertical;
			if (vertical > 90.0f) vertical = 90f - (vertical-90.0f);
			vertical = Mathf.Clamp01(vertical / 10.0f); //degrees

			//set the light intensity
			lightObjectWorld.intensity = lightObjectWorld.intensity * Mathf.Lerp(0.0f,1.0f,vertical);

			//modulate moon intensity via sun angle
			if (lightObjectNight != null){
				
				mMult = Mathf.Clamp01((356.0f-sunVert)/5.0f);
				if (sunVert > 90.0f) mMult = 1.0f;
				if (lightObjectWorld.intensity > 0.0f) mMult = 0.0f;
				lightObjectNight.intensity = lightObjectNight.intensity * mMult;
			}
		}






	    // ---------------------------------
		// --   SOLAR ECLIPSE HANDLING   ---
		// ---------------------------------
		ecldiff = 0.0f;
		if (useCamera != null){
			mRay = new Ray(useCamera.transform.position,moonObject.transform.position);
			sRay = new Ray(useCamera.transform.position,sunObject.transform.position);
			ecldiff = (Vector3.Angle(sRay.direction,mRay.direction));
			ecldiff = 1.0f-(ecldiff);
		}
		
		Shader.SetGlobalFloat("_Tenkoku_EclipseFactor",Mathf.Clamp01(Mathf.Lerp(1.0f,0.0f,Mathf.Clamp01(ecldiff*1.1f))));
		lightObjectWorld.intensity *= Mathf.Clamp01(Mathf.Lerp(1.0f,0.0f,Mathf.Clamp01(ecldiff*1.1f)));
		if (eclipseObject != null && ecldiff > 0.0f){
			eclipseObject.transform.eulerAngles = new Vector3(eclipseObject.transform.eulerAngles.x, eclipseObject.transform.eulerAngles.y, eclipseObject.transform.eulerAngles.z+Time.deltaTime*4.0f);
			eclipseObject.transform.localScale = new Vector3(1f,1f,1f) *  Mathf.Lerp(1f,7f,ecldiff);
		}





		//handle gamma world lighting brightness
		if (QualitySettings.activeColorSpace != ColorSpace.Linear){
			lightObjectWorld.intensity = lightObjectWorld.intensity*0.8f;
		}

		//set minimum world light setting
		if (allowMultiLights){
			lightObjectWorld.intensity = Mathf.Clamp(lightObjectWorld.intensity,0.001f,8.0f);
			lightObjectWorld.renderMode = LightRenderMode.ForcePixel;
			lightObjectNight.renderMode = LightRenderMode.ForcePixel;
			lightObjectNight.shadows = LightShadows.Soft;
			lightObjectWorld.enabled = true;
			lightObjectNight.enabled = true;
		} else {
			lightObjectNight.renderMode = LightRenderMode.ForceVertex;
			lightObjectNight.shadows = LightShadows.None;
			lightObjectWorld.intensity = Mathf.Clamp(lightObjectWorld.intensity,0.0f,8.0f);
			if (lightObjectWorld.color.a <= 0.6f){
				lightObjectWorld.enabled = false;
				lightObjectNight.enabled = true;
			} else {
				lightObjectWorld.enabled = true;
				lightObjectNight.enabled = false;
			}
		}

		Shader.SetGlobalColor("_Tenkoku_Daylight",lightObjectWorld.color * (lightObjectWorld.intensity*0.5f) * (calcTime));
		Shader.SetGlobalColor("_Tenkoku_Nightlight",lightObjectNight.color * (lightObjectNight.intensity + nightBrightness) * (1.0f-calcTime) * 2f);

	}





	void FixedUpdate(){
		TimeUpdate();
	}



	void TimeUpdate(){

	    //------------------------------
	    //---    CALCULATE TIMER    ----
	    //------------------------------

		//TRANSITION TIME
		if (doTransition){
			Tenkoku_TransitionTime();
		}



		//AUTO INCREASE TIME
		if (useAutoTime && !autoTimeSync && Application.isPlaying){

			//calculate time compression curve
			curveVal = timeCurves.Evaluate(dayValue);
			
			if (useTimeCompression < 3600.0f){
				setTimeSpan = (Time.fixedDeltaTime * useTimeCompression);
				countSecond += setTimeSpan;
				countSecondMoon += Mathf.FloorToInt(setTimeSpan*0.92068f);
				countSecondStar += Mathf.FloorToInt(setTimeSpan*0.9333342f);
			} else {
				setTimeSpan = (Time.fixedDeltaTime * (useTimeCompression/60.0f));
				countMinute += setTimeSpan;
				countMinuteMoon += Mathf.FloorToInt(setTimeSpan*0.92068f);
				countMinuteStar += Mathf.FloorToInt(setTimeSpan*0.9333342f);
			}
		}
		


		if (Mathf.Abs(countSecond) >= 1.0f){
			currentSecond += Mathf.FloorToInt(countSecond);
			countSecond = 0.0f;
		}
		if (Mathf.Abs(countMinute) >= 1.0f){
			currentMinute += Mathf.FloorToInt(countMinute);
			countMinute = 0.0f;
		}
			
		if (Mathf.Abs(countSecondMoon) >= 1.0f){
			moonSecond += Mathf.FloorToInt(countSecondMoon);
			countSecondMoon = 0.0f;
		}
		if (Mathf.Abs(countMinuteMoon) >= 1.0f){
			moonMinute += Mathf.FloorToInt(countMinuteMoon);
			countMinuteMoon = 0.0f;
		}
			
		if (Mathf.Abs(countSecondStar) >= 1.0f){
			starSecond += Mathf.FloorToInt(countSecondStar);
			countSecondStar = 0.0f;
		}
		if (Mathf.Abs(countMinuteStar) >= 1.0f){
			starMinute += Mathf.FloorToInt(countMinuteStar);
			countMinuteStar = 0.0f;
		}
			
			
		//RECALCULATE DATE and TIME
		RecalculateTime();
		
		//SET DISPLAY TIME
		displayTime = DisplayTime("[ hh:mm:ss am] [ M/D/Y ad]");


	}





	public string DisplayTime( string format ){

		//format string examples:
		// "M/D/Y H:M:S"
		setString = format;
		eon = "ad";
		useHour = setHour;
		
		displayHour = setHour;
		if (use24Clock){
			hourMode = "AM";
			if (useHour > 12){
				displayHour -= 12;
				hourMode = "PM";
			}
		} else {
			hourMode = "";
		}

		
		if (currentYear < 0) eon = "bc";
		setString = setString.Replace("hh",useHour.ToString("00"));
		setString = setString.Replace("mm",currentMinute.ToString("00"));
		setString = setString.Replace("ss",currentSecond.ToString("00"));
		setString = setString.Replace("Y",Mathf.Abs(currentYear).ToString());
		setString = setString.Replace("M",currentMonth.ToString());
		setString = setString.Replace("D",currentDay.ToString());
		setString = setString.Replace("ad",eon.ToString());
		
		if (use24Clock){
			setString = setString.Replace("am",hourMode.ToString());
		} else {
			setString = setString.Replace("am","");
		}
		
		return setString;
	}





	public int RecalculateLeapYear( int checkMonth, int checkYear){

		//check for leap Year (by div 4 method)
		leapYear = false;
		if ( (checkYear / 4.0f) == Mathf.FloorToInt(checkYear / 4.0f) ) leapYear = true;

		//double check for leap Year (by div 100 + div 400 method)
		if ((checkYear / 100.0f) == Mathf.FloorToInt(checkYear / 100.0f)){
			if ((checkYear / 400.0f) != Mathf.FloorToInt(checkYear / 400.0f)) leapYear = false;
		}
		
		//calculate month length
		monthLength = 31;
		testMonth = Mathf.FloorToInt(checkMonth);
		if (testMonth == 4 || testMonth == 6 || testMonth == 9 || testMonth == 11) monthLength = 30;
		if (testMonth == 2 && !leapYear) monthLength = 28;
		if (testMonth == 2 && leapYear) monthLength = 29;

		return monthLength;
	}




	void RecalculateTime(){
		
		//getLeapYear
		monthFac = RecalculateLeapYear(currentMonth,currentYear);

		//clamp and pass all values
		if (currentSecond > 59 || currentSecond < 0) currentMinute += Mathf.FloorToInt(currentSecond/60.0f);
		if (currentSecond > 59) currentSecond = 0;
		if (currentSecond < 0) currentSecond = 59;
		if (currentMinute > 59 || currentMinute < 0.0) currentHour += Mathf.FloorToInt(currentMinute/60.0f);
		if (currentMinute > 59) currentMinute = 0;
		if (currentMinute < 0) currentMinute = 59;

		if (currentHour > 23 || currentHour < 0) currentDay += Mathf.CeilToInt((currentHour/24.0f));
		if (currentHour > 23) currentHour = 0;
		if (currentHour < 0) currentHour = 23;

		if (currentDay > monthFac || currentDay < 1) currentMonth += Mathf.CeilToInt((currentDay/(monthFac*1.0f))-1.0f);
		if (currentDay > monthFac) currentDay = 1;
		if (currentDay < 1) currentDay = RecalculateLeapYear(currentMonth-1,currentYear);
		if (currentMonth > 12 || currentMonth < 1) currentYear += Mathf.CeilToInt((currentMonth/12.0f)-1f);
		if (currentMonth > 12) currentMonth = 1;
		if (currentMonth < 1) currentMonth = 12;
		if (currentYear == 0) currentYear = 1;
		
		//clamp and pass all moon values
		if (moonSecond > 59 || moonSecond < 0) moonMinute += Mathf.FloorToInt(moonSecond/60.0f);
		if (moonSecond > 59) moonSecond = 0;
		if (moonSecond < 0) moonSecond = 59;
		if (moonMinute > 59 || moonMinute < 0.0) moonHour += Mathf.FloorToInt(moonMinute/60.0f);
		if (moonMinute > 59) moonMinute = 0;
		if (moonMinute < 0) moonMinute = 59;
		if (moonHour > 24 || moonHour < 1) moonDay += Mathf.CeilToInt((moonHour/24.0f)-1f);
		if (moonHour > 24) moonHour = 1;
		if (moonHour < 1) moonHour = 24;
		if (moonDay > monthFac || moonDay < 1) moonMonth += Mathf.CeilToInt((moonDay/(monthFac*1.0f))-1.0f);
		if (moonDay > monthFac) moonDay = 1;
		if (moonDay < 1) moonDay = RecalculateLeapYear(moonMonth-1,currentYear);
		if (moonMonth > 12 || moonMonth < 1) moonYear += Mathf.CeilToInt((moonMonth/12.0f)-1f);
		if (moonMonth > 12) moonMonth = 1;
		if (moonMonth < 1) moonMonth = 12;
		
		//clamp and pass all star values
		if (starSecond > 59 || starSecond < 0) starMinute += Mathf.FloorToInt(starSecond/60.0f);
		if (starSecond > 59) starSecond = 0;
		if (starSecond < 0) starSecond = 59;
		if (starMinute > 59 || starMinute < 0.0) starHour += Mathf.FloorToInt(starMinute/60.0f);
		if (starMinute > 59) starMinute = 0;
		if (starMinute < 0) starMinute = 59;
		if (starHour > 24 || starHour < 1) starDay += Mathf.CeilToInt((starHour/24.0f)-1f);
		if (starHour > 24) starHour = 1;
		if (starHour < 1) starHour = 24;
		if (starDay > monthFac || starDay < 1) starMonth += Mathf.CeilToInt((starDay/(monthFac*1.0f))-1.0f);
		if (starDay > monthFac) starDay = 1;
		if (starDay < 1) starDay = RecalculateLeapYear(starMonth-1,currentYear);
		if (starMonth > 12 || starMonth < 1) starYear += Mathf.CeilToInt((starMonth/12.0f)-1f);
		if (starMonth > 12) starMonth = 1;
		if (starMonth < 1) starMonth = 12;
		
		
		if (!use24Clock && setHour > 12){
			setHour = currentHour + 12;
		} else {
			setHour = currentHour;
		}
		setHour = currentHour;




		//CALCULATE TIMERS
		setDay = ((setHour-1) * 3600.0f) + (currentMinute * 60.0f) + (currentSecond * 1.0f);
		setStar = ((starHour-1) * 3600.0f) + (starMinute * 60.0f) + (starSecond * 1.0f);
		monthAddition = 0.0f;

		for (aM = 1; aM < currentMonth; aM++){
			monthAddition += (RecalculateLeapYear( aM, currentYear) * 1.0f);
		}
		setYear = monthAddition+(currentDay-1f)+((currentSecond + (currentMinute*60f) + (setHour*3600f))/86400.0f);
		setMonth = (Mathf.Floor(moonDay-1)*86400.0f) + (Mathf.Floor(moonHour-1) * 3600.0f) + (Mathf.Floor(moonMinute) * 60.0f) + (Mathf.Floor(moonSecond) * 1.0f);
		setMoon = (Mathf.Floor(moonDay-1)*86400.0f)+(Mathf.Floor(moonHour-1f) * 3600.0f) + (Mathf.Floor(moonMinute) * 60.0f) + (Mathf.Floor(moonSecond) * 1.0f);
		setStar = (Mathf.Floor(starMonth-1)*30.41666f)+Mathf.Floor(starDay-1f)+(Mathf.Floor((starSecond) + (Mathf.Floor(starMinute)*60f) + (Mathf.Floor(starHour-1)*3600f))/86400.0f);


		//CLAMP VALUES
		yearDiv = 365.0f;
		if (leapYear) yearDiv = 366.0f;
		if (setYear > (86400.0f *  yearDiv)) setYear = 0.0f;
		if (setYear < 0.0f) setYear = (86400.0f *  yearDiv);
		

		//CALCULATE VALUES
		dayValue = setDay / 86400.0f;
		monthValue = setMonth / (86400.0f * 29.530589f);
		yearValue = setYear / yearDiv;
		starValue = setDay / 86400.0f;
		starValue -= (setStar / 365.0f);
		moonValue = setDay / 86400.0f;
		moonValue -= setMoon / ((86400.0f) * 29.6666f);
		
		//SEND TIME TO CALCULATIONS COMPONENT
		calcComponent.y = currentYear;
		calcComponent.m = currentMonth;
		calcComponent.D = currentDay;
		calcComponent.UT = 1.0f*currentHour+(1.0f*currentMinute/60.0f)+((1.0f*currentSecond/60.0f)/60.0f);


		calcComponent.local_latitude = setLatitude;
		calcComponent.local_longitude = setLongitude;	
	}



	public float floatRound(float inFloat){
		//var retFloat : float = 0.0f;
		//retFloat = Mathf.Round(inFloat*1000.0f)/1000.0f;
		//return retFloat;
		return inFloat;
	}





	void ClampParticle( string px_system ){

		clampRes = 0.0f;
		px = 0;
		usePoint = 0.0f;
		
		//clamp rain fog particles to ground (raycast)
		if (px_system == "rain fog"){
		clampRes = 1.0f;
		setParticles = new ParticleSystem.Particle[particleObjectRainFog.particleCount];
		particleObjectRainFog.GetParticles(setParticles);
		for (px = 0; px < particleObjectRainFog.particleCount; px++){
			if (setParticles[px].remainingLifetime >= 2.5f){
				if ((px/clampRes) == (Mathf.Floor(px/clampRes))*1.0f){
				if (Physics.Raycast(new Vector3(setParticles[px].position.x,5000.0f,setParticles[px].position.z), -Vector3.up, out hit, 10000.0f)){
					usePoint = hit.point.y;
				}	
				}
				setParticles[px].position = new Vector3(setParticles[px].position.x, usePoint, setParticles[px].position.z);
			}
		}
		particleObjectRainFog.SetParticles(setParticles,setParticles.Length);
		particleObjectRainFog.Play();
		}

		
		
	    
		//clamp rain fog particles to ground (raycast)
		if (px_system == "rain splash"){
		clampRes = 1.0f;
		setParticles = new ParticleSystem.Particle[particleObjectRainSplash.particleCount];
		particleObjectRainSplash.GetParticles(setParticles);
		for (px = 0; px < particleObjectRainSplash.particleCount; px++){
			
			#if UNITY_5_3 || UNITY_5_4 || UNITY_5_6 || UNITY_5_7 || UNITY_5_8 || UNITY_5_9
				setParticles[px].startColor = new Color(setParticles[px].startColor.r,setParticles[px].startColor.g,setParticles[px].startColor.b, 0.0f);
			#else
				setParticles[px].color = new Color(setParticles[px].color.r,setParticles[px].color.g,setParticles[px].color.b, 0.0f);
			#endif

			if (setParticles[px].remainingLifetime >= 0.05f){
				if ((px/clampRes) == (Mathf.Floor(px/clampRes))*1.0f){
				if (Physics.Raycast(new Vector3(setParticles[px].position.x,5000.0f,setParticles[px].position.z), -Vector3.up, out hit, 10000.0f)){
					usePoint = hit.point.y;
				}
				}
				setParticles[px].position = new Vector3(setParticles[px].position.x, usePoint, setParticles[px].position.z);

				#if UNITY_5_3 || UNITY_5_4 || UNITY_5_6 || UNITY_5_7 || UNITY_5_8 || UNITY_5_9
					setParticles[px].startColor = new Color(setParticles[px].startColor.r,setParticles[px].startColor.g,setParticles[px].startColor.b, TenRandom.Next(25.0f,150.0f));
				#else
					setParticles[px].color = new Color(setParticles[px].color.r,setParticles[px].color.g,setParticles[px].color.b, TenRandom.Next(25.0f,150.0f));
				#endif
			}
		}
		particleObjectRainSplash.SetParticles(setParticles,setParticles.Length);
		particleObjectRainSplash.Play();
		}


		//clamp fog particles to ground (raycast)
		if (px_system == "fog"){
		clampRes = 1.0f;
		setParticles = new ParticleSystem.Particle[particleObjectFog.particleCount];
		particleObjectFog.GetParticles(setParticles);
		for (px = 0; px < particleObjectFog.particleCount; px++){
			if (setParticles[px].remainingLifetime >= 4.8f){
				if ((px/clampRes) == (Mathf.Floor(px/clampRes))*1.0f){
				if (Physics.Raycast(new Vector3(setParticles[px].position.x,5000.0f,setParticles[px].position.z), -Vector3.up, out hit, 10000.0f)){
					usePoint = hit.point.y;
				}	
				}
				if (usePoint > weather_FogHeight) usePoint = weather_FogHeight;
				setParticles[px].position = new Vector3(setParticles[px].position.x, usePoint, setParticles[px].position.z);
			}
		}
		particleObjectFog.SetParticles(setParticles,setParticles.Length);
		particleObjectFog.Play();
		}
		
		
		
	}



	public Vector2 TenkokuConvertAngleToVector(float convertAngle) {

		dir = new Vector3(0f,0f);
		tempAngle = new Vector3(0f,0f,0f);
		if (convertAngle <= 180.0f){
			tempAngle = Vector3.Slerp(Vector3.forward,-Vector3.forward,(convertAngle)/180.0f);
			dir = new Vector2(tempAngle.x,-tempAngle.z);
		}
		if (convertAngle > 180.0f){
			tempAngle = Vector3.Slerp(-Vector3.forward,Vector3.forward,(convertAngle-180.0f)/180.0f);
			dir = new Vector2(-tempAngle.x,-tempAngle.z);
		}
		
		return dir;
	}



	public Color DecodeColorKey( string position ) {

		//positions
		texPos = 0;
		if (position == "sun") texPos = 144;
		if (position == "sunray") texPos = 144;
		if (position == "ambientcolor") texPos = 81;
		if (position == "moon") texPos = 59;
		if (position == "skyambient") texPos = 37;
		if (position == "skybase") texPos = 100;
		if (position == "ambientcloud") texPos = 186;
		if (position == "colorhorizon") texPos = 121;
		if (position == "colorcloud") texPos = 166;
		if (position == "ambientGI") texPos = 15;
		
		//decode texture
		returnColor = new Color(0f,0f,0f,1f);
		if (colorRamp != null){
			returnColor = Color.Lerp((colorRamp.GetPixel(stepTime, texPos)),(colorRamp.GetPixel(stepTime+1, texPos)), timeLerp);
		}
		
		if (position == "moon" && colorRamp != null){
			returnColor = Color.Lerp((colorRamp.GetPixel(stepTimeM, texPos)),(colorRamp.GetPixel(stepTimeM+1, texPos)), timeLerpM);
		}

		//modulate colors based on atmospheric density
		if (position == "sun"){
			//returnColor = Color.Lerp(Color(1,1,1,1),returnColor,atmosphereDensity);
			//returnColor = Color.Lerp(returnColor,Color(0.5,0.1,0,1),atmosphereDensity*0.3);
			//returnColor = Color.Lerp(returnColor,Color(0.0,0.0,0,1),atmosphereDensity*0.1);
		}
		if (position == "sunray"){
			returnColor.r = Mathf.Pow(returnColor.r,3.2f);
			returnColor.g = Mathf.Pow(returnColor.g,3.2f);
			returnColor.b = Mathf.Pow(returnColor.b,3.2f);
		}


		//linear and gamma conversion
		if (QualitySettings.activeColorSpace != ColorSpace.Linear){

			//specific color shift
			if (position == "skybase"){
				returnColor.r *= 0.4646f;
				returnColor.g *= 0.4646f;
				returnColor.b *= 0.4646f;
			}	
		}
		
		return returnColor;	    
	}



	// SET TENKOKU DATA
	//the SetData function is useful in accessing
	//tenkoku weather and time variables from C#
	//components.  Please see documentation for
	//more information on usage.
	public void Tenkoku_SetData( string data){

		//split
	    dataArray = data.Split(","[0]);

		for (ax = 0; ax < (dataArray.Length); ax++){

			//get positions
			for (xP = 0; xP < dataArray[ax].Length; xP++){
				if (dataArray[ax].Substring(xP,1) == "(") pos1 = xP;
				if (dataArray[ax].Substring(xP,1) == ")") pos2 = xP;
			}

			//decode data
			length = pos2-(pos1+1);
			func = dataArray[ax].Substring(0,pos1).ToLower();
			dat = dataArray[ax].Substring(pos1+1,length);

			//run functions
			if (func=="currentyear") currentYear = int.Parse(dat);
			if (func=="currentmonth") currentMonth = int.Parse(dat);
			if (func=="currentday") currentDay = int.Parse(dat);
			if (func=="currenthour") currentHour = int.Parse(dat);
			if (func=="currentminute") currentMinute = int.Parse(dat);
			if (func=="currentsecond") currentSecond = int.Parse(dat);

			if (func=="setlatitude") setLatitude = float.Parse(dat);
			if (func=="setlongitude") setLongitude = float.Parse(dat);

			if (func=="weather_cloudaltostratusamt") weather_cloudAltoStratusAmt = float.Parse(dat);
			if (func=="weather_cloudcirrusamt") weather_cloudCirrusAmt = float.Parse(dat);
			if (func=="weather_cloudcumulusamt") weather_cloudCumulusAmt = float.Parse(dat);
			if (func=="weather_overcastamt") weather_OvercastAmt = float.Parse(dat);
			if (func=="weather_cloudscale") weather_cloudScale = float.Parse(dat);
			if (func=="weather_cloudspeed") weather_cloudSpeed = float.Parse(dat);
			if (func=="weather_rainamt") weather_RainAmt = float.Parse(dat);
			if (func=="weather_snowamt") weather_SnowAmt = float.Parse(dat);
			if (func=="weather_fogamt") weather_FogAmt = float.Parse(dat);
			if (func=="weather_windamt") weather_WindAmt = float.Parse(dat);
			if (func=="weather_winddir") weather_WindDir = float.Parse(dat);

			if (func=="weathertypeindex") weatherTypeIndex = int.Parse(dat);
			if (func=="weather_autoforecasttime") weather_autoForecastTime = float.Parse(dat);
			if (func=="weather_transitiontime") weather_TransitionTime = float.Parse(dat);
			if (func=="weather_forceupdate") weather_forceUpdate = true;


			bool setSync = false;
			if (func=="autotimesync"){ 
				if (float.Parse(dat) == 1.0) setSync = true;
				if (dat == "true") setSync = true;
				autoTimeSync = setSync;
			}
			setSync = false;
			if (func=="autodatesync"){ 
				if (float.Parse(dat) == 1.0) setSync = true;
				if (dat == "true") setSync = true;
				autoDateSync = setSync;
			}
			setSync = false;
			if (func=="autotime"){ 
				if (float.Parse(dat) == 1.0) setSync = true;
				if (dat == "true") setSync = true;
				autoTime = setSync;
			}

			if (func=="timecompression") timeCompression = float.Parse(dat);

			if (func=="maincamera"){
				if (GameObject.Find(dat) != null){
					mainCamera = GameObject.Find(dat).transform; 
				}
			}


		}


	}







	// ENCODE SETTINGS TO STRING
	// this is useful to quickly encode
	// Tenkoku settings over a server.
	public string Tenkoku_EncodeData() {

		//run functions
		dataString = currentYear.ToString()+",";
		dataString += currentMonth.ToString()+",";
		dataString += currentDay.ToString()+",";
		dataString += currentHour.ToString()+",";
		dataString += currentMinute.ToString()+",";
		dataString += currentSecond.ToString()+",";

		dataString += setLatitude.ToString()+",";
		dataString += setLongitude.ToString()+",";

		dataString += weather_cloudAltoStratusAmt.ToString()+",";
		dataString += weather_cloudCirrusAmt.ToString()+",";
		dataString += weather_cloudCumulusAmt.ToString()+",";
		dataString += weather_OvercastAmt.ToString()+",";
		dataString += weather_cloudScale.ToString()+",";
		dataString += weather_cloudSpeed.ToString()+",";
		dataString += weather_RainAmt.ToString()+",";
		dataString += weather_SnowAmt.ToString()+",";
		dataString += weather_FogAmt.ToString()+",";
		dataString += weather_WindAmt.ToString()+",";
		dataString += weather_WindDir.ToString()+",";

		dataString += weatherTypeIndex.ToString()+",";
		dataString += weather_autoForecastTime.ToString()+",";
		dataString += weather_TransitionTime.ToString()+",";

		dataUpdate = weather_forceUpdate ? "1" : "0";
		dataString += dataUpdate+",";

		dataString += weather_temperature.ToString()+",";
		dataString += weather_rainbow.ToString()+",";
		dataString += weather_lightning.ToString()+",";
		dataString += weather_lightningDir.ToString()+",";
		dataString += weather_lightningRange.ToString()+",";

		//save random marker
		dataString += randSeed.ToString()+",";

		//current cloud coordinates
		dataString += currCoords.x.ToString()+",";
		dataString += currCoords.y.ToString();


		return dataString;
	}





	// DECODE SETTINGS FROM STRING
	// this is useful to quickly decode
	// Tenkoku settings over a server.
	public void Tenkoku_DecodeData( string dataString ){

	    data = dataString.Split(","[0]);

	    //set functions
		currentYear = int.Parse(data[0]);
		currentMonth = int.Parse(data[1]);
		currentDay = int.Parse(data[2]);
		currentHour = int.Parse(data[3]);
		currentMinute = int.Parse(data[4]);
		currentSecond = int.Parse(data[5]);

		setLatitude = float.Parse(data[6]);
		setLongitude = float.Parse(data[7]);

		weather_cloudAltoStratusAmt = float.Parse(data[8]);
		weather_cloudCirrusAmt = float.Parse(data[9]);
		weather_cloudCumulusAmt = float.Parse(data[10]);
		weather_OvercastAmt = float.Parse(data[11]);
		weather_cloudScale = float.Parse(data[12]);
		weather_cloudSpeed = float.Parse(data[13]);
		weather_RainAmt = float.Parse(data[14]);
		weather_SnowAmt = float.Parse(data[15]);
		weather_FogAmt = float.Parse(data[16]);
		weather_WindAmt = float.Parse(data[17]);
		weather_WindDir = float.Parse(data[18]);

		weatherTypeIndex = int.Parse(data[19]);
		weather_autoForecastTime = float.Parse(data[20]);
		weather_TransitionTime = float.Parse(data[21]);

		setUpdate = data[22];
		if (setUpdate == "1"){
			weather_forceUpdate = true;
		} else {
			weather_forceUpdate = false;
		}

		weather_temperature = float.Parse(data[23]);
		weather_rainbow = float.Parse(data[24]);
		weather_lightning = float.Parse(data[25]);
		weather_lightningDir = float.Parse(data[26]);
		weather_lightningRange = float.Parse(data[27]);

		//decode random marker
		randSeed = int.Parse(data[28]);
		TenRandom = new Tenkoku.Core.Random( randSeed );

		//current cloud coordinates
		currCoords.x = float.Parse(data[29]);
		currCoords.y = float.Parse(data[30]);

	}








	// TRANSITION TIME CAPTURE
	public void Tenkoku_SetTransition(string startTime, string targetTime, int duration, float direction){
		transitionStartTime = startTime;
		transitionTargetTime = targetTime;
		transitionDuration = duration;
		transitionDirection = direction;
		doTransition = true;
	}
	public void Tenkoku_SetTransition(string startTime, string targetTime, int duration, float direction, GameObject callbackObject){
		transitionStartTime = startTime;
		transitionTargetTime = targetTime;
		transitionDuration = duration;
		transitionDirection = direction;
		if (callbackObject != null){
			transitionCallbackObject = callbackObject;
			transitionCallback = true;
		}
		doTransition = true;
	}


	// DEPRECATED! - TRANSITION TIME CAPTURE (C# SEND MESSAGE)
	//Note this function is deprecated and will be removed at some point in the future!
	void Tenkoku_SendTransition( string transVal ){
		values = transVal.Split(","[0]);
		transitionStartTime = values[0];
		transitionTargetTime = values[1];
		transitionDuration = float.Parse(values[2]);
		transitionDirection = float.Parse(values[3]);
		if (values.Length == 5){
			callbackObject = GameObject.Find(values[4]);
			if (callbackObject != null){
				transitionCallbackObject = callbackObject;
				transitionCallback = true;
			}
		}
		doTransition = true;
		Debug.Log("Note: Tenkoku_SendTransition() has been deprecated, and will be removed in a future update.  Please use Tenkoku_SetTransition instead.");

	}



	// DO TIME TRANSITIONS
	void Tenkoku_TransitionTime(){

		//Initialize
		if (transitionTime <= 0.0f){

			//clamp direction
			if (transitionDirection > 0.0f) transitionDirection = 1.0f;
			if (transitionDirection < 0.0f) transitionDirection = -1.0f;

			//calculate ending time
			setTransHour = Mathf.Clamp(System.Int32.Parse(transitionTargetTime.Substring(0,2)),0,23);
			setTransMinute = Mathf.Clamp(System.Int32.Parse(transitionTargetTime.Substring(3,2)),0,59);
			setTransSecond = Mathf.Clamp(System.Int32.Parse(transitionTargetTime.Substring(6,2)),0,59);
			endTime = setTransSecond + (setTransMinute*60) + (setTransHour*3600);

			//calculate starting time
			if (transitionStartTime == null || transitionStartTime == ""){
				startHour = currentHour;
				startMinute = currentMinute;
				startSecond = currentSecond;
			} else {
				startHour = Mathf.Clamp(System.Int32.Parse(transitionStartTime.Substring(0,2)),0,23);
				startMinute = Mathf.Clamp(System.Int32.Parse(transitionStartTime.Substring(3,2)),0,59);
				startSecond = Mathf.Clamp(System.Int32.Parse(transitionStartTime.Substring(6,2)),0,59);
				currentHour = startHour;
				currentMinute = startMinute;
				currentSecond = startSecond;
			}
			startTime = startSecond + (startMinute*60) + (startHour*3600);

			//calculate same day flag
			transSameDay = true;
			if (transitionDirection == 1.0f && endTime < startTime) transSameDay = false;
			if (transitionDirection == -1.0f && endTime > startTime) transSameDay = false;

			//set transition target value
			if (transitionDirection == 1.0f){
				if (endTime > startTime && transSameDay) timeVal = (endTime - startTime);
				if (endTime < startTime && !transSameDay) timeVal = ((86400f - startTime) + endTime);
			}
			if (transitionDirection == -1.0f){
				if (endTime < startTime && transSameDay) timeVal = (startTime - endTime);
				if (endTime > startTime && !transSameDay) timeVal = ((86400f - endTime) + startTime);
			}
			setTransVal = timeVal/transitionDuration;
		}

		//track current time
		currTime = currentSecond + (currentMinute*60) + (currentHour*3600);
		if (transitionDirection == 1.0f && currTime <= endTime) transSameDay = true;
		if (transitionDirection == -1.0f && currTime >= endTime) transSameDay = true;

		//check for transition end
		endTransition = false;
		if (transitionDirection == 1.0f && currTime >= (endTime) && transSameDay) endTransition = true;
		if (transitionDirection == -1.0f && currTime <= (endTime) && transSameDay) endTransition = true;

		if (transitionTime > transitionDuration){
			Debug.Log(transitionTime);
			endTransition = true;
		}

		//END TRANSITION
		if (endTransition){
			useAutoTime = false;
			transitionTime = 0.0f;
			doTransition = false;
			currentHour = setTransHour;
			currentMinute = setTransMinute;
			currentSecond = setTransSecond;

			//Send Callback (optional)
			if (transitionCallback){
				if (transitionCallbackObject != null){
					transitionCallbackObject.SendMessage("CaptureTenkokuCallback",SendMessageOptions.DontRequireReceiver);
				}
			}
			transitionCallbackObject = null;
			transitionCallback = false;

		//DO TRANSITION
		} else {
			useAutoTime = true;
			transitionTime += Time.deltaTime;
			//useTimeCompression = setTransVal*transitionDirection*(Mathf.SmoothStep(0.0f,1.0f,transitionTime/transitionDuration));
			useTimeCompression = setTransVal * transitionDirection;

		}

	}




}
}