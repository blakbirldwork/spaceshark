---------------------------------------------------
TENKOKU - DYNAMIC SKY

Copyright �2016 Tanuki Digital
Version 1.1.1
---------------------------------------------------


----------------------------
THANK YOU FOR YOUR PURCHASE!
----------------------------
Thank you for buying TENKOKU and supporting Tanuki Digital!
It's people like you that allow us to build and improve our software! 
if you have any questions, comments, or requests for new features
please visit the Tanuki Digital Forums and post your feedback:

http://tanukidigital.com/forum/

or email us directly at: konnichiwa@tanukidigital.com



----------------------
REGISTER YOUR PURCHASE
----------------------
Did you purchase Tenkoku - Dynamic Sky on the Unity Asset Store?
Registering at Tanuki Digital.com gives you immediate access to new downloads, updates, and exclusive content as well as Tenkoku and Tanuki Digital news and info.  Fill out the registration forum using your Asset Store "OR" Order Number here:

http://www.tanukidigital.com/tenkoku/index.php?register=1



----------------------
SUPPORT
----------------------
If you have questions about Tenkoku, need help with a feature, or think you've identified a bug please let us know either in the Unity forum or on the Tanuki Digital forum below.

Unity Forum Thread: http://forum.unity3d.com/threads/tenkoku-dynamic-sky.318166/
Tanuki Digital Forum: http://tanukidigital.com/forum/

You can also email us directly at: konnichiwa@tanukidigital.com



----------------------
DOCUMENTATION
----------------------
Please read the Tenkoku documentation files for more in-depth customization information.
http://tanukidigital.com/tenkoku/documentation



-------------
INSTALLATION
-------------
I. IMPORT TENKOKU FILES INTO YOUR PROJCT
Go to: �Assets -> Import Package -> Custom Package...� in the Unity Menu and select the �tenkoku_dynamicsky_ver1.x.unitypackage� file. This will open an import dialog box. Click the import button and all the Tenkoku files will be imported into your project list.

II. ADD THE TENKOKU MODULE TO YOUR SCENE
1) Drag the Tenkoku DynamicSky prefab located in the �/PREFABS� folder into your scene list.
2) If it isn�t set already, make sure to set the Tenkoku DynamicSky�s position in the transform settings to 0,0,0

III. ADD TENKOKU EFFECTS TO YOUR CAMERA
1) Click on your main camera object and add the Tenkoku Fog effect by going to Component-->Image Effects-->Tenkoku-->Tenkoku Fog.
Note: For best results this effect should be placed to render BEFORE your Tonemapping effect(if applicable).

(optional)
2) Click on your main camera object and add the Tenkoku Sun Shaft effect by going to Component-->Image Effects-->Tenkoku-->Tenkoku Sun Shafts.
Note: For best results this effect should be placed to render AFTER your Tonemapping effect(if applicable).


A Note About Scene Cameras:
Tenkoku relies on tracking your main scene camera in order to properly update in the scene.  By default Tenkoku attempts to auto locate your camera by selecting the camera in your scene with the �MainCamera� tag.  Alternatively you can set it to manual mode and drag the appropriate camera into the �Scene Camera� slot.




-------------
NOTES
-------------
A Note about C# versus JS codebases:
In version 1.1.0, Tenkoku is available in both C# and JS.  This is a temporary situation. Development going forward will be done in C# exclusively, and future updates will only be available in the C# codebase.

A Note On Accuracy:
Moon and planet position calculations are currently accurate for years ranging between 1900ca - 2100ca.  The further away from the year 2000ca that you get (in either direction) the more noticeable calculation errors will become.  Additional calculation methods are currently being looked at to increase the accuracy range for these objects.



-------------------------------
RELEASE NOTES - Version 1.1.1
-------------------------------

WHAT'S NEW
- Added Public variable moonPhase that calculates current phase of moon (0.0f - 1.0f), where 0.5 = full moon.
- Added Public variable moonLightAmt that calculates current light reflectance factor of the moon (0.0f - 1.0f)
- Added Sound effect "Adjust via timescale" option which will morph sound based on scene Timescale settings (on by default).
- Improved ambient rendering with sky color weighting (using Trilight ambient mode).

CHANGES
- Lowered default light bias to 0.05.  This should fix 'unattached' shadows rendering error.
- Adjusted default sun color balance to be less warm during midday hours.
- Adjusted default ambient light color balance.

BUG FIXES
- Fixed Build namespace error, which had prevented build compilation.
- Fixed Error with Speed Curve not affecting timescale properly.
- Fixed issue with sun light disengaging too early when multi-lighting is turned off.
- Fixed issue with multi-lighting not turning moon lighting on correctly in some instances.
- Overcast setting no longer effects light shadow setting, and instead adjusts diffuse lighting amounts.
- Tenkoku_EncodeData() and Tenkoku_DecodeData() function set to be publicly accessable.
- Fixed Tenkoku_SetTransition() function to be publicly accessable.
- Fixed issue where transitions would not stop at various target times.
- Slight tweak to Galaxy texture positioning for better alignment.
- Added more blur to Galaxy texture, to remove pixelization.
- Fixed Unity 5.4 (beta) bug relating to editor loading, causing error message spam.
- Fixed error message spam when no camera is active in the scene.
- Included Temperature, rainbow, and lightning data into Encode()/Decode() server functions
- Fixed error with encoding/decoding cloud position data.
- Fixed problem with saving setting changes in Unity 5.3+



----------------------------
CREDITS
----------------------------
- Lunar image adapted from texture work by James Hastings-Trew.  Used with permission.
http://planetpixelemporium.com

- Galaxy image adapted from an open source image made available by the European Southern Observatory(ESO/S. Brunier):
https://www.eso.org/public/usa/images/eso0932a/

- Star position and magnitude data is taken from the Yale Bright Star Catalog, 5th Edition:
http://tdc-www.harvard.edu/catalogs/bsc5.html (archive)
http://heasarc.gsfc.nasa.gov/W3Browse/star-catalog/bsc5p.html (data overview)

- Calculation algorithms for Sun, Moon, and Planet positions have been adapted from work published by Paul Schlyter, in his paper 'Computing Planetary Positions' which can be viewed here: http://www.stjarnhimlen.se/comp/ppcomp.html  I've taken liberties with his procedure where I thought appropriate or where I found it best suits the greater Tenkoku system.
