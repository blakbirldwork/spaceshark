// Shader created with Shader Forge v1.32 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.32;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:3138,x:32719,y:32712,varname:node_3138,prsc:2|emission-2181-OUT;n:type:ShaderForge.SFN_Color,id:7961,x:31608,y:32963,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_7961,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Tex2d,id:3102,x:31608,y:32763,ptovrint:False,ptlb:Texture_Unlit,ptin:_Texture_Unlit,varname:node_3102,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Lerp,id:4416,x:31832,y:32943,varname:node_4416,prsc:2|A-3102-RGB,B-7961-RGB,T-7778-OUT;n:type:ShaderForge.SFN_Slider,id:7778,x:31451,y:33137,ptovrint:False,ptlb:Texture_or_Color,ptin:_Texture_or_Color,varname:node_7778,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_Fresnel,id:3188,x:31497,y:33313,varname:node_3188,prsc:2|EXP-8698-OUT;n:type:ShaderForge.SFN_Blend,id:349,x:32069,y:33088,varname:node_349,prsc:2,blmd:6,clmp:True|SRC-3188-OUT,DST-4416-OUT;n:type:ShaderForge.SFN_ValueProperty,id:8698,x:31301,y:33330,ptovrint:False,ptlb:FallOff-Out_sise,ptin:_FallOffOut_sise,varname:node_8698,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.94;n:type:ShaderForge.SFN_Blend,id:6096,x:32244,y:33198,varname:node_6096,prsc:2,blmd:10,clmp:True|SRC-9097-OUT,DST-349-OUT;n:type:ShaderForge.SFN_Fresnel,id:4332,x:31497,y:33468,varname:node_4332,prsc:2|EXP-8471-OUT;n:type:ShaderForge.SFN_ValueProperty,id:8471,x:31338,y:33502,ptovrint:False,ptlb:FallOff-in_sise,ptin:_FallOffin_sise,varname:_node_8698_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.32;n:type:ShaderForge.SFN_OneMinus,id:9097,x:31667,y:33468,varname:node_9097,prsc:2|IN-4332-OUT;n:type:ShaderForge.SFN_Multiply,id:2181,x:32434,y:33185,varname:node_2181,prsc:2|A-6096-OUT,B-6096-OUT;proporder:7961-3102-7778-8698-8471;pass:END;sub:END;*/

Shader "Shader Forge/Unlit_SpaceShips_01" {
    Properties {
        _Color ("Color", Color) = (0.5,0.5,0.5,1)
        _Texture_Unlit ("Texture_Unlit", 2D) = "white" {}
        _Texture_or_Color ("Texture_or_Color", Range(0, 1)) = 1
        _FallOffOut_sise ("FallOff-Out_sise", Float ) = 0.94
        _FallOffin_sise ("FallOff-in_sise", Float ) = 0.32
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma only_renderers d3d9 d3d11 glcore gles gles3 metal 
            #pragma target 3.0
            uniform float4 _Color;
            uniform sampler2D _Texture_Unlit; uniform float4 _Texture_Unlit_ST;
            uniform float _Texture_or_Color;
            uniform float _FallOffOut_sise;
            uniform float _FallOffin_sise;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
////// Lighting:
////// Emissive:
                float node_3188 = pow(1.0-max(0,dot(normalDirection, viewDirection)),_FallOffOut_sise);
                float4 _Texture_Unlit_var = tex2D(_Texture_Unlit,TRANSFORM_TEX(i.uv0, _Texture_Unlit));
                float3 node_4416 = lerp(_Texture_Unlit_var.rgb,_Color.rgb,_Texture_or_Color);
                float3 node_6096 = saturate(( saturate((1.0-(1.0-node_3188)*(1.0-node_4416))) > 0.5 ? (1.0-(1.0-2.0*(saturate((1.0-(1.0-node_3188)*(1.0-node_4416)))-0.5))*(1.0-(1.0 - pow(1.0-max(0,dot(normalDirection, viewDirection)),_FallOffin_sise)))) : (2.0*saturate((1.0-(1.0-node_3188)*(1.0-node_4416)))*(1.0 - pow(1.0-max(0,dot(normalDirection, viewDirection)),_FallOffin_sise))) ));
                float3 emissive = (node_6096*node_6096);
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
