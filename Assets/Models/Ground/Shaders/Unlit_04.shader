// Shader created with Shader Forge v1.32 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.32;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:3138,x:32719,y:32712,varname:node_3138,prsc:2|normal-4112-RGB,emission-4085-OUT;n:type:ShaderForge.SFN_Fresnel,id:9116,x:31616,y:33302,varname:node_9116,prsc:2|EXP-5050-OUT;n:type:ShaderForge.SFN_Vector1,id:5050,x:31437,y:33324,varname:node_5050,prsc:2,v1:2;n:type:ShaderForge.SFN_Add,id:4085,x:32458,y:33589,varname:node_4085,prsc:2|A-280-OUT,B-7415-OUT,C-8217-OUT;n:type:ShaderForge.SFN_Multiply,id:280,x:31987,y:33310,varname:node_280,prsc:2|A-9007-OUT,B-8006-OUT;n:type:ShaderForge.SFN_Vector3,id:8006,x:31616,y:33453,varname:node_8006,prsc:2,v1:0.2720588,v2:0.2720588,v3:0.2720588;n:type:ShaderForge.SFN_Fresnel,id:9251,x:31485,y:34121,varname:node_9251,prsc:2|EXP-8900-OUT;n:type:ShaderForge.SFN_Fresnel,id:3140,x:31616,y:33161,varname:node_3140,prsc:2|EXP-8121-OUT;n:type:ShaderForge.SFN_Vector1,id:8121,x:31437,y:33183,varname:node_8121,prsc:2,v1:5;n:type:ShaderForge.SFN_Add,id:9007,x:31796,y:33213,varname:node_9007,prsc:2|A-3140-OUT,B-9116-OUT;n:type:ShaderForge.SFN_Slider,id:8900,x:31149,y:34140,ptovrint:False,ptlb:Fresnel size,ptin:_Fresnelsize,varname:node_8900,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.55,max:5;n:type:ShaderForge.SFN_Power,id:5341,x:31673,y:34096,varname:node_5341,prsc:2|VAL-9166-OUT,EXP-9251-OUT;n:type:ShaderForge.SFN_Slider,id:9166,x:31328,y:34050,ptovrint:False,ptlb:Fresnel Power,ptin:_FresnelPower,varname:node_9166,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.168,max:1;n:type:ShaderForge.SFN_Vector3,id:9295,x:31673,y:34222,varname:node_9295,prsc:2,v1:0,v2:0,v3:0;n:type:ShaderForge.SFN_Lerp,id:8217,x:31894,y:34162,varname:node_8217,prsc:2|A-5341-OUT,B-9295-OUT,T-4915-OUT;n:type:ShaderForge.SFN_Slider,id:4915,x:31499,y:34347,ptovrint:False,ptlb:Lerp to VBlack,ptin:_LerptoVBlack,varname:_node_8900_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.95,max:1;n:type:ShaderForge.SFN_Color,id:7216,x:31738,y:33580,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_7216,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.8235295,c2:0.5921569,c3:0.4117647,c4:1;n:type:ShaderForge.SFN_Color,id:4112,x:32297,y:32718,ptovrint:False,ptlb:Normal Color,ptin:_NormalColor,varname:node_4112,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:0.1176471,c3:0.682353,c4:1;n:type:ShaderForge.SFN_Blend,id:7053,x:31927,y:33713,varname:node_7053,prsc:2,blmd:2,clmp:True|SRC-7216-RGB,DST-8023-RGB;n:type:ShaderForge.SFN_Lerp,id:7415,x:32163,y:33677,varname:node_7415,prsc:2|A-7216-RGB,B-7053-OUT,T-3405-OUT;n:type:ShaderForge.SFN_Slider,id:3405,x:31724,y:33938,ptovrint:False,ptlb:Texture or Color,ptin:_TextureorColor,varname:node_3405,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.556,max:1;n:type:ShaderForge.SFN_Color,id:8023,x:31738,y:33750,ptovrint:False,ptlb:node_8023,ptin:_node_8023,varname:node_8023,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5019608,c2:0.5019608,c3:0.5019608,c4:1;proporder:8900-9166-4915-7216-4112-3405-8023;pass:END;sub:END;*/

Shader "Shader Forge/Unlit_04" {
    Properties {
        _Fresnelsize ("Fresnel size", Range(0, 5)) = 0.55
        _FresnelPower ("Fresnel Power", Range(0, 1)) = 0.168
        _LerptoVBlack ("Lerp to VBlack", Range(0, 1)) = 0.95
        _Color ("Color", Color) = (0.8235295,0.5921569,0.4117647,1)
        _NormalColor ("Normal Color", Color) = (0,0.1176471,0.682353,1)
        _TextureorColor ("Texture or Color", Range(0, 1)) = 0.556
        _node_8023 ("node_8023", Color) = (0.5019608,0.5019608,0.5019608,1)
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma only_renderers d3d9 d3d11 glcore gles gles3 metal 
            #pragma target 3.0
            uniform float _Fresnelsize;
            uniform float _FresnelPower;
            uniform float _LerptoVBlack;
            uniform float4 _Color;
            uniform float4 _NormalColor;
            uniform float _TextureorColor;
            uniform float4 _node_8023;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float4 posWorld : TEXCOORD0;
                float3 normalDir : TEXCOORD1;
                float3 tangentDir : TEXCOORD2;
                float3 bitangentDir : TEXCOORD3;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalLocal = _NormalColor.rgb;
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
////// Lighting:
////// Emissive:
                float node_5341 = pow(_FresnelPower,pow(1.0-max(0,dot(normalDirection, viewDirection)),_Fresnelsize));
                float3 emissive = (((pow(1.0-max(0,dot(normalDirection, viewDirection)),5.0)+pow(1.0-max(0,dot(normalDirection, viewDirection)),2.0))*float3(0.2720588,0.2720588,0.2720588))+lerp(_Color.rgb,saturate((1.0-((1.0-_node_8023.rgb)/_Color.rgb))),_TextureorColor)+lerp(float3(node_5341,node_5341,node_5341),float3(0,0,0),_LerptoVBlack));
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
