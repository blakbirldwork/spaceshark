// Shader created with Shader Forge v1.32 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.32;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:3138,x:32719,y:32712,varname:node_3138,prsc:2|normal-35-RGB,emission-4085-OUT;n:type:ShaderForge.SFN_Tex2d,id:4813,x:32168,y:33606,ptovrint:False,ptlb:Diffusion,ptin:_Diffusion,varname:_Diffusion,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:0a06ae48f12937b4fbe3fe9ed6b1df9b,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:35,x:32188,y:32604,ptovrint:False,ptlb:NormalMap,ptin:_NormalMap,varname:_NormalMap,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:0ff048dd0c9dddd4f8b637057d6ff334,ntxv:3,isnm:True;n:type:ShaderForge.SFN_Fresnel,id:9116,x:31616,y:33302,varname:node_9116,prsc:2|EXP-5050-OUT;n:type:ShaderForge.SFN_Vector1,id:5050,x:31437,y:33324,varname:node_5050,prsc:2,v1:2;n:type:ShaderForge.SFN_Add,id:4085,x:32458,y:33589,varname:node_4085,prsc:2|A-280-OUT,B-4813-RGB,C-8217-OUT;n:type:ShaderForge.SFN_Multiply,id:280,x:31987,y:33310,varname:node_280,prsc:2|A-9007-OUT,B-8006-OUT;n:type:ShaderForge.SFN_Vector3,id:8006,x:31616,y:33453,varname:node_8006,prsc:2,v1:0.2720588,v2:0.2720588,v3:0.2720588;n:type:ShaderForge.SFN_Fresnel,id:9251,x:31595,y:33867,varname:node_9251,prsc:2|EXP-8900-OUT;n:type:ShaderForge.SFN_Fresnel,id:3140,x:31616,y:33161,varname:node_3140,prsc:2|EXP-8121-OUT;n:type:ShaderForge.SFN_Vector1,id:8121,x:31437,y:33183,varname:node_8121,prsc:2,v1:5;n:type:ShaderForge.SFN_Add,id:9007,x:31796,y:33213,varname:node_9007,prsc:2|A-3140-OUT,B-9116-OUT;n:type:ShaderForge.SFN_Slider,id:8900,x:31259,y:33886,ptovrint:False,ptlb:Fresnel size,ptin:_Fresnelsize,varname:node_8900,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.44,max:5;n:type:ShaderForge.SFN_Power,id:5341,x:31783,y:33842,varname:node_5341,prsc:2|VAL-9166-OUT,EXP-9251-OUT;n:type:ShaderForge.SFN_Slider,id:9166,x:31438,y:33796,ptovrint:False,ptlb:Fresnel Power,ptin:_FresnelPower,varname:node_9166,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.011,max:1;n:type:ShaderForge.SFN_Vector3,id:9295,x:31783,y:33968,varname:node_9295,prsc:2,v1:0,v2:0,v3:0;n:type:ShaderForge.SFN_Lerp,id:8217,x:31974,y:33908,varname:node_8217,prsc:2|A-5341-OUT,B-9295-OUT,T-4915-OUT;n:type:ShaderForge.SFN_Slider,id:4915,x:31626,y:34090,ptovrint:False,ptlb:Lerp to VBlack,ptin:_LerptoVBlack,varname:_node_8900_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.345,max:1;proporder:4813-35-8900-9166-4915;pass:END;sub:END;*/

Shader "Shader Forge/Unlit_01" {
    Properties {
        _Diffusion ("Diffusion", 2D) = "white" {}
        _NormalMap ("NormalMap", 2D) = "bump" {}
        _Fresnelsize ("Fresnel size", Range(0, 5)) = 0.44
        _FresnelPower ("Fresnel Power", Range(0, 1)) = 0.011
        _LerptoVBlack ("Lerp to VBlack", Range(0, 1)) = 0.345
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma only_renderers d3d9 d3d11 glcore gles gles3 metal 
            #pragma target 3.0
            uniform sampler2D _Diffusion; uniform float4 _Diffusion_ST;
            uniform sampler2D _NormalMap; uniform float4 _NormalMap_ST;
            uniform float _Fresnelsize;
            uniform float _FresnelPower;
            uniform float _LerptoVBlack;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 bitangentDir : TEXCOORD4;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 _NormalMap_var = UnpackNormal(tex2D(_NormalMap,TRANSFORM_TEX(i.uv0, _NormalMap)));
                float3 normalLocal = _NormalMap_var.rgb;
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
////// Lighting:
////// Emissive:
                float node_9007 = (pow(1.0-max(0,dot(normalDirection, viewDirection)),5.0)+pow(1.0-max(0,dot(normalDirection, viewDirection)),2.0));
                float4 _Diffusion_var = tex2D(_Diffusion,TRANSFORM_TEX(i.uv0, _Diffusion));
                float node_5341 = pow(_FresnelPower,pow(1.0-max(0,dot(normalDirection, viewDirection)),_Fresnelsize));
                float3 node_9295 = float3(0,0,0);
                float3 emissive = ((node_9007*float3(0.2720588,0.2720588,0.2720588))+_Diffusion_var.rgb+lerp(float3(node_5341,node_5341,node_5341),node_9295,_LerptoVBlack));
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
