﻿using UnityEngine;
using UnityEditor;
using NUnit.Framework;

public class NewEditorTest {

    private GameObject gameObject;

    [Test]
	public void EditorTest() {
		//Arrange
		gameObject = new GameObject();

		//Act
		//Try to rename the GameObject
		var newGameObjectName = "Player";
		gameObject.name = newGameObjectName;

		//Assert
		//The object has a new name
		Assert.AreNotSame(newGameObjectName, gameObject.name);
	}

    [Test]
    public void CheckName()
    {
        Assert.IsNotEmpty(gameObject.name, "Player");
    }

}
