﻿using UnityEngine;
using UnityEngine.PlaymodeTests;
using UnityEngine.Assertions;
using System.Collections;

[PlayModeTest]
public class NewPlayModeTestWithMethods {

	[PlayModeTest]
	public void NewPlayModeTestWithMethodsSimplePasses() {
		// Use the Assert class to test conditions.
	}

	[PlayModeTest]
	public IEnumerator NewPlayModeTestWithMethodsWithEnumeratorPasses()	{
		// Use the Assert class to test conditions.
		// yield to skip a frame
		yield return null;
	}
}
