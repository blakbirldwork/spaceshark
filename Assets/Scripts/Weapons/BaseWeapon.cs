﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class BaseWeapon : MonoBehaviour {
    public GameObject bulletPrefab;
    public Transform spownPosition;
    public float FireRate = 1f;
    protected float currRate;


   public virtual void Update()
    {
        if(currRate != FireRate)
        {
            currRate += Time.deltaTime;
        }
    }

    public virtual bool isCanFire()
    {
        return currRate >= FireRate;
    }
   
    public virtual void RpcTryShoot(Vector3 position, Quaternion rotation, Vector3 vector)
    {
        
        if (currRate >= FireRate)
        {
            var bullet = PoolManager.SpawnObject(bulletPrefab, spownPosition.position, rotation).GetComponent<Bullet>();
            bullet.SetAgle(vector);
            currRate = 0;
        }
    }

    public virtual void RpcTryShoot()
    {
       
    }
}
