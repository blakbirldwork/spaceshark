﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ShootData
{
    public Vector3 position;
    public Quaternion rotation;
    public Vector3 vector;
}