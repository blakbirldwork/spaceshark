﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System.Linq;

public class LevelPoolManager : Photon.PunBehaviour {
    public static LevelPoolManager Instance;


    [SerializeField]
    private GameObject fuelPrefab;
    public Dictionary<string,List<GameObject>> Pool;
  

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
            DestroyImmediate(gameObject);

        OnStartServer();
    }

 

    public void OnStartServer()
    {
      
       //s PoolManager.WarmPool(fuelPrefab, 20);
        Pool = new Dictionary<string, List<GameObject>>();
        AddToPool(fuelPrefab);
     
    }

    public void AddToPool(GameObject obj)
    {
        
        if (!Pool.ContainsKey(obj.name))
        {
           var somePool = new List<GameObject>();
            for (int i = 0; i < 20; i++)
            {
                GameObject go = PhotonNetwork.Instantiate(fuelPrefab.name, new Vector3(0, 0, 0), Quaternion.identity,0);
                go.SetActive(false);
                somePool.Add(go);
            }
            Pool.Add(obj.name, somePool);
        }


    }

    public GameObject GetObj(string name)
    {

        if (name == "fuel")
            name = fuelPrefab.name;

        if (!Pool.ContainsKey(name))
        {
            foreach (var item in Pool)
            {
                Debug.Log(item.Key);
            }
            Debug.LogWarning("Pool not found");
            return null;
        }
        var obj = Pool[name].FirstOrDefault(x => x.activeSelf == false);
        obj.SetActive(true);
        return obj;
    }

  
    private void CmdMoveObj(string name, Vector3 pos)
    {
        if (name == "fuel")
            name = fuelPrefab.name;

        if (!Pool.ContainsKey(name))
        {
            foreach (var item in Pool)
            {
                Debug.Log(item.Key);
            }
            Debug.LogWarning("Pool not found");
         
        }
        var obj = Pool[name].FirstOrDefault(x => x.activeSelf == false);
        obj.SetActive(true);
        obj.transform.position = pos;
      
    }

    public void GetObj(string name, Vector3 pos)
    {
        CmdMoveObj(name,pos);
    }

}
