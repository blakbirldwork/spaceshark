using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour
{
	public float accel;
    private float lifetime = 5f;
    private Vector3 angle;



    public void SetAgle(Vector3 quat)
    {
        angle = quat;
    }

    private void OnTriggerEnter(Collider hit)
    {
        Debug.Log(hit.gameObject.name);
        if (hit.tag == TriggerStrings.PLAYER || hit.tag == TriggerStrings.AI_BOT)
        {

            GameObject other;
            if (hit.transform.parent != null)
                other = hit.transform.parent.gameObject;
            else
                other = hit.gameObject;


            var component = other.gameObject.GetComponent<IDamage>();
            if (component != null)
            {
                component.Damage();
            }
        }
    }

    void OnEnable()
	{
        StartCoroutine("WaitFinish");
    }

    private void FixedUpdate()
    {
        transform.Translate(Vector3.forward * accel * Time.deltaTime);
    }

    IEnumerator WaitFinish()
    {
        yield return new WaitForSeconds(lifetime);
        Finish();
    }

	void Finish()
	{
		PoolManager.ReleaseObject(this.gameObject);

		//Note: 
		// This takes the gameObject instance, and NOT the prefab instance.
		// Without this call the object will never be available for re-use!
		// gameObject.SetActive(false) is automatically called
	}
}
