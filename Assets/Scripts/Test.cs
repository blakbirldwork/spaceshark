﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Test : MonoBehaviour {
    public Transform leftSaw;
    public Vector3 force = new Vector3(0,0,1);
    public float speed = 1;

    public bool isStart;
    public bool isStop;

    public int agle = 10;
    // Use this for initialization
    void Start () {
        Rotate();
    }
	
    void Rotate()
    {
        Debug.Log("Rotate");
        agle += 5;
        if (agle >= 360)
            agle = 0;

        force.z = agle;
        leftSaw.DORotate(force, speed).OnComplete(Rotate);
    }

	// Update is called once per frame
	void Update () {
        if (isStart)
        {
            Rotate();
            isStart = false;
        }
        if (isStop)
        {
            leftSaw.DOPause();
            leftSaw.localEulerAngles = new Vector3(0,90,0);
            isStop = false;
        }
    }
}
