﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class LevelSettings : Singleton<LevelSettings> {

    [SerializeField]   UIButton forsageButton1;
    [SerializeField]   Button restartButton;
    [SerializeField]   ButtonEvents forsageButton;
    public NguiSpeedCorect speedCorect;
    public NguiSpeedCorect forsageSpeedCorect;
    public UISlider FuelSlider;
    public UISlider ForsageSlider;
    public Action<float> OnForsageChange;
    public Action<float> OnSpeedChange;
    public Action OnRestartClick;
    public Action OnShootClick;
    public Action<bool> OnForsagetClick;
    public EasyJoystick joystick;
    public Transform[] jumpPoints;

    public Vector3[] spounPositions = {
        new Vector3(-2.5f, 3f ,-175.5f),
        new Vector3(-82.6f, 3f ,-154.85f),
        new Vector3(-144.5f, 5f ,-99.7f),
        new Vector3(-175f, 4f ,-15.66f),
        new Vector3(-162.97f, 6f ,65.1f),
        new Vector3(-107.85f, 3f ,138.5f),
        new Vector3(-16.4f, 10f ,174.5f),
        new Vector3(56f, 3f ,165.5f),
        new Vector3(130f, 12.66f ,116f),
        new Vector3(172.92f, 6f ,29.4f),
        new Vector3(150f, 18f ,-88.11f),
        new Vector3(130f, 7f ,-117.61f),
        new Vector3(102.64f, 3f ,-142.37f),
        new Vector3(55.7f, 14f ,-165.89f)
    };
    private Vector3 currStartPosition;

    // Use this for initialization
    void Start ()
    {
        speedCorect.OnValueChange += onSpeedChange;
        forsageSpeedCorect.OnValueChange += onForsageChange;
        currStartPosition = spounPositions[0];
        string shipName = PlayerPrefs.GetString("PlayerShip");
        PhotonNetwork.Instantiate(shipName, currStartPosition, Quaternion.identity, 0);
        jumpPoints = transform.GetComponentsInChildren<Transform>();
    }

    private void Update()
    {
        if(forsageButton1.state == UIButtonColor.State.Pressed)
        {
            OnForsageDown();
        }
        else 
        {
            OnForsageUp();
        }
    }

    private void onForsageChange(float obj)
    {
        if (OnForsageChange != null)
            OnForsageChange(obj);
    }

    private void onSpeedChange(float obj)
    {
        if (OnSpeedChange != null)
            OnSpeedChange(obj);
    }

    public void onShootClick()
    {
        if (OnShootClick != null)
            OnShootClick();
    }

    public void OnForsageUp()
    {
        if (OnForsagetClick != null)
            OnForsagetClick(false);
    }

    public void OnForsageDown()
    {
        if (OnForsagetClick != null)
            OnForsagetClick(true);
    }

    private void OnRestartClickButton()
    {
        if (OnRestartClick != null)
            OnRestartClick();
    }
}
