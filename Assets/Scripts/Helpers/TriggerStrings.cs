﻿public class TriggerStrings  {

    public const string ZERRO_POINT = "ZerroPoint";
    public const string CAMERA_RIG = "CameraRig";
    public const string PLAYER = "Player";
    public const string AI_BOT = "Ai_bot";
    public const string FLOR = "Flor";
    public const string CELING = "ceiling";
    
}

public class AnimatorHelper
{
    public const string WALK = "Walk";
    public const string DEATH_DIR = "DeathDir";
    public const string DEATH_MOVE = "DeathMove";
    public const string IDLE = "Idle";
}
