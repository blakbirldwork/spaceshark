﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class BasePickableItem : Photon.PunBehaviour {

    private bool isActive;

    [SerializeField]
    float fuel = 10;
	// Use this for initialization
	void Start () {
        transform.eulerAngles = new Vector3(-70, 0, 0);
	}
	
	// Update is called once per frame
	void Update () {

        transform.Rotate(1,1,1);
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == TriggerStrings.PLAYER)
        {
          photonView.RPC("CmdTeelToPick", PhotonTargets.AllViaServer,other.transform.root.name);
        }
    }

    private void OnStateChange(bool state)
    {
        gameObject.SetActive(state);
    }

    [PunRPC]
    void CmdTeelToPick(string uniqId)
    {
        Debug.Log(uniqId);
        isActive = false;
        GameObject go = GameObject.Find(uniqId);
        go.GetComponent<ShipUserController>().AddFuel(fuel);
        OnStateChange(isActive);      
    }
}
