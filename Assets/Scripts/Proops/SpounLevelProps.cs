﻿using Ai;
using UnityEngine;

public class SpounLevelProps : Photon.PunBehaviour, IPunObservable
{
    public GameObject FuelPrefab;
    public GameObject Drone;
    public GameObject Drone1;
    public GameObject DroneSwarm;
    public GameObject DroneSaw;
    public GameObject DroneSaw2;
    public GameObject DroneFlyBig;
    public GameObject GroundDrone;
    public GameObject JumpDrone;
    public GameObject BolidDrone;
    public Transform[] FuelPoints;
    public Transform[] GroundPatrolPoints1;
    public Transform[] GroundPatrolPoints2;
    public Transform[] JumpPoints;
    public bool isUp;

    private bool isActivated;

    public void OnStartServer()
    {
        isActivated = true;
        //Random Instantiate Fly Drones or Fuel
        for (int i = 0; i < FuelPoints.Length; i++)
        {

            int random = Random.Range(1, 20);
            if (random < 0)
            {
                GameObject go = PhotonNetwork.InstantiateSceneObject(Drone1.name, FuelPoints[i].transform.position, Quaternion.identity,0, null);
            }
            else if (random <= 6)
            {
                GameObject go = PhotonNetwork.InstantiateSceneObject(Drone.name, FuelPoints[i].transform.position, Quaternion.identity, 0, null);
            }
            else if(random <= 9)
            {
                GameObject go = PhotonNetwork.InstantiateSceneObject(DroneSwarm.name, FuelPoints[i].transform.position, Quaternion.identity, 0, null);
            }
            else if (random <= 12)
            {
                GameObject go = PhotonNetwork.InstantiateSceneObject(DroneFlyBig.name, FuelPoints[i].transform.position, Quaternion.identity, 0, null);
            }
            else if (random <= 15)
            {
                GameObject go = PhotonNetwork.InstantiateSceneObject(DroneSaw.name, FuelPoints[i].transform.position, Quaternion.identity, 0, null);
            }
            else if (random <= 17)
            {
                GameObject go = PhotonNetwork.InstantiateSceneObject(DroneSaw2.name, FuelPoints[i].transform.position, Quaternion.identity, 0, null);
            }
            else if (random <= 20)
            {
                Vector3 pos = FuelPoints[i].transform.position;
                for (int d = 0; d < 4; d++)
                {
                    pos.y += i + 2;
                    PhotonNetwork.InstantiateSceneObject(BolidDrone.name, pos , Quaternion.identity, 0, null);
                }
               
             
            }
        }

        // Instantiate Ground Drones
        var goGround = PhotonNetwork.Instantiate(GroundDrone.name, GroundPatrolPoints1[0].position, Quaternion.identity,0);
        goGround.GetComponent<SimpleGroundDrone>().AddPatrolPositions(GroundPatrolPoints1);

        GameObject goGround2 = PhotonNetwork.Instantiate(GroundDrone.name, GroundPatrolPoints2[0].position, Quaternion.identity,0);
        goGround2.GetComponent<SimpleGroundDrone>().AddPatrolPositions(GroundPatrolPoints2);

        //Instantiate JumpDrone
        var gojump = PhotonNetwork.Instantiate(JumpDrone.name, GroundPatrolPoints1[0].position, Quaternion.identity, 0);
        gojump.GetComponent<JumpDrone>().AddPatrolPositions(JumpPoints);
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
        {
            // We own this player: send the others our data
            stream.SendNext(this.isActivated);
        }
        else
        {
            // Network player, receive data
            this.isActivated = (bool)stream.ReceiveNext();
        }
    }

    private void Start()
    {
        if (!PhotonNetwork.player.IsMasterClient)
            return;

        if (!isActivated)
            OnStartServer();
        else
            enabled = false;
    }

}
