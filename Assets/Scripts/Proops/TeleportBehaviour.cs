﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportBehaviour : MonoBehaviour {

    public TeleportBehaviour Teleport;
    public Transform TeleportPlace;
    private void OnTriggerEnter(Collider other)
    {
        Vector3 pos = other.transform.root.position;
        pos.x = Teleport.TeleportPlace.transform.position.x;

        other.transform.root.position = pos;
    }
}
