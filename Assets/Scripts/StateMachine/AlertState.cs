﻿using System;
using UnityEngine;
using Ai;

namespace StateMachine
{

    public class AlertState : IEnemyState
    {
        private readonly SimpleDrone enemy;
        private float searchTimer;

        public AlertState(SimpleDrone statePatternEnemy)
        {
            enemy = statePatternEnemy;
        }

        public void OnTriggerEnter(Collider other)
        {

        }

        public void ToAlertState()
        {
            Debug.Log("Can't transition to same state");
        }


        public void ToPatrolState()
        {
            enemy.currentState = enemy.patrolState;
            searchTimer = 0f;
        }

        public virtual void ToDeathState()
        {

        }

        private void Look()
        {

        }

        private void Search()
        {
            searchTimer += Time.deltaTime;
            if (searchTimer >= enemy.searchingDuration)
                ToPatrolState();
        }

        public void UpdateState()
        {
            Look();
            Search();
        }

        public void Dead()
        {
            throw new NotImplementedException();
        }
    }
}