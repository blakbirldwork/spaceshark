﻿using UnityEngine;
using Random = UnityEngine.Random;
using Ai;

namespace StateMachine
{

    public class SawPatrolState : IEnemyState
    {
        protected DroneDanger enemy;
        protected int nextWayPoint;
        protected Vector3 startPosition;
        protected float waiTimeCurr = 0;
        protected float waiTime = 3f;

        public SawPatrolState(DroneDanger statePatternEnemy)
        {
            enemy = statePatternEnemy;
            startPosition = enemy.transform.position;
        }

        public virtual void UpdateState()
        {
            if (waiTimeCurr > 0)
            {
                waiTimeCurr -= Time.deltaTime;
                if (waiTimeCurr <= 0)
                    enemy.CastToAnimator("Hide");
            }
            Patrol();
        }

        public virtual void OnTriggerEnter(Collider other)
        {
            if (waiTimeCurr <= 0)
            {
                if (other.gameObject.CompareTag("Player"))
                {
                    var component = other.gameObject.transform.root.GetComponent<ShipUserController>();
                    if (component != null)
                    {
                        (component as ShipUserController).Damage();
                        other.transform.root.GetComponent<Rigidbody>().AddExplosionForce(enemy.explosiveForce, enemy.transform.position, 5, 1 , ForceMode.Impulse);
                    }
                }
                enemy.StartRotation();
                enemy.CastToAnimator("Play");
                //   enemy.Hit();
                waiTimeCurr = waiTime;
            }
        }

        public virtual void ToDeathState()
        {
            enemy.currentState = enemy.deathlState;
        }

        public virtual void ToPatrolState()
        {
            Debug.Log("Can't transition to same state");
        }

        public virtual void ToAlertState()
        {
            Debug.Log("Find PLayer");
            enemy.currentState = enemy.alertState;
        }

        protected virtual void Patrol()
        {
            if (enemy.transform.position.y > 60)
            {
                enemy.changepPoscurr = -10;
            }
            else if (enemy.transform.position.y < 3)
            {
                enemy.changepPoscurr = 5;
            }

            if (enemy.changepPoscurr > 0)
            {
                enemy.changepPoscurr -= Time.deltaTime;
                if (enemy.changepPoscurr < 0)
                {
                    enemy.changepPoscurr = Random.Range(enemy.changepPosMin, enemy.changepPosMax) * -1;
                    enemy.moveDirection.x *= -1;
                }
            }
            else
            {
                enemy.changepPoscurr += Time.deltaTime;
                if (enemy.changepPoscurr > 0)
                {
                    enemy.changepPoscurr = Random.Range(enemy.changepPosMin, enemy.changepPosMax);
                    enemy.moveDirection.x *= -1;
                }
            }

            //Rotation left-right
            var angle = new Vector3();
            if (enemy.moveDirection.x > 0f)
                angle.y = 90;
            else if (enemy.moveDirection.x < 0f)
                angle.y = -90;

            Quaternion target = Quaternion.Euler(angle.x, angle.y, angle.z);
            enemy.shipRoot.localRotation = Quaternion.RotateTowards(enemy.shipRoot.localRotation, target, Time.deltaTime * enemy.rotateSpeed);
            //  enemy.transform.Translate(new Vector3(enemy.moveDirection.x, enemy.changepPoscurr, 0) * ((enemy.speed) * Time.deltaTime));
            enemy.my_Rigidbody.AddForce(new Vector3(enemy.moveDirection.x * -1, enemy.changepPoscurr, 0) * ((enemy.speed * 2) * Time.deltaTime), ForceMode.Impulse);
        }

        public virtual void Dead()
        {
            ToDeathState();
        }
    }
}