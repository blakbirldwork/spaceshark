﻿using System;
using UnityEngine;
using Ai;

namespace StateMachine
{

    public class SwarmDeathState : IEnemyState
    {
        private readonly DroneFlySwarm enemy;
        private bool isDeath;
        private float timer;

        public SwarmDeathState(DroneFlySwarm statePatternEnemy)
        {
            enemy = statePatternEnemy;

        }

        public void Dead()
        {
            throw new NotImplementedException();
        }

        public void OnTriggerEnter(Collider other)
        {
            // throw new NotImplementedException();
        }

        public void ToAlertState()
        {
            throw new NotImplementedException();
        }

        public virtual void ToDeathState()
        {

        }

        public void ToPatrolState()
        {
            enemy.transform.position = enemy.InitPosition;
            enemy.currentState = enemy.patrolState;
            enemy.gameObject.SetActive(true);
            foreach (var item in enemy.SwarmDrones)
            {
                item.SetActive(true);
            }
            isDeath = false;
        }

        public void UpdateState()
        {
            if (isDeath == false)
            {
                isDeath = true;
                enemy.SpounDeathPart();
                // enemy.gameObject.SetActive(false);
                Vector3 pos = new Vector3();
                pos.x = UnityEngine.Random.Range(1000, 10000);
                pos.y = UnityEngine.Random.Range(1000, 10000);
                pos.z = UnityEngine.Random.Range(1000, 10000);
                enemy.transform.position = pos;
            }
            else
            {
                timer += Time.deltaTime;
                if (timer > 40)
                {
                    timer = 0;
                    ToPatrolState();
                }
            }
        }
    }
}