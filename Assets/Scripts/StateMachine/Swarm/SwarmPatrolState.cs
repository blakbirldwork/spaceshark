﻿using UnityEngine;
using Ai;

namespace StateMachine
{

    public class SwarmPatrolState : IEnemyState
    {
        protected GameObject[] drones;
        protected DroneFlySwarm enemy;
        protected int nextWayPoint;
        protected Vector3 startPosition;
        private bool isLocalMove;

        public SwarmPatrolState(DroneFlySwarm statePatternEnemy)
        {
            enemy = statePatternEnemy;
            startPosition = enemy.transform.position;
            drones = enemy.SwarmDrones;
        }

        public virtual void UpdateState()
        {
            Look();
            Patrol();
        }

        public virtual void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.CompareTag("Player"))
                enemy.Damage();
        }

        public virtual void ToDeathState()
        {
            enemy.currentState = enemy.deathlState;
        }

        public virtual void ToPatrolState()
        {
            Debug.Log("Can't transition to same state");
        }

        public virtual void ToAlertState()
        {
            Debug.Log("Find PLayer");
            enemy.currentState = enemy.alertState;
        }



        protected virtual void Look()
        {

        }

        private void ChangeRotation()
        {
            foreach (var item in drones)
            {
                item.transform.localEulerAngles = new Vector3(item.transform.localEulerAngles.x, item.transform.localEulerAngles.y, item.transform.localEulerAngles.z * enemy.moveDirection.z);
            }
        }

        protected virtual void Patrol()
        {
            if (enemy.transform.position.y > 60)
            {
                enemy.changepPoscurr = -10;
            }

            if (enemy.changepPoscurr > 0)
            {
                enemy.changepPoscurr -= Time.deltaTime;
                if (enemy.changepPoscurr < 2)
                {
                    enemy.changepPoscurr = UnityEngine.Random.Range(enemy.changepPosMin, enemy.changepPosMax) * -1;
                    enemy.moveDirection.x *= -1;
                    enemy.moveDirection.z *= -1;
                    ChangeRotation();
                }
            }
            else
            {
                enemy.changepPoscurr += Time.deltaTime;
                if (enemy.changepPoscurr > 0)
                {
                    enemy.changepPoscurr = UnityEngine.Random.Range(enemy.changepPosMin, enemy.changepPosMax);
                    enemy.moveDirection.x *= -1;
                    enemy.moveDirection.z *= -1;
                    ChangeRotation();
                }
            }

            //TODO: add movements
            //  enemy.transform.RotateAround(Vector3.zero, enemy.moveDirection, enemy.speed * Time.deltaTime);

            //Rotation left-right
            var angle = new Vector3();
            angle.y = 90;
            if (enemy.moveDirection.y > 0)
                angle.x = 90;
            else if (enemy.moveDirection.y < 0)
                angle.x = -90;

            //Rotation up - down
            if (enemy.moveDirection.x > 0f)
                angle.x = 180;
            else if (enemy.moveDirection.x < 0f)
                angle.x = 0;

            Quaternion target = Quaternion.Euler(angle.x, angle.y, angle.z);

            enemy.transform.eulerAngles = new Vector3(0f, 90f, 90f);

            //enemy.shipRoot.localRotation = Quaternion.RotateTowards(enemy.shipRoot.localRotation, target, Time.deltaTime * enemy.rotateSpeed);
            //  enemy.transform.Translate(new Vector3(enemy.moveDirection.x, enemy.moveDirection.y, enemy.moveDirection.z) * ((enemy.speed) * Time.deltaTime));
            enemy.my_Rigidbody.AddForce(new Vector3(enemy.moveDirection.x * -1, enemy.changepPoscurr, 0) * ((enemy.speed * 2) * Time.deltaTime), ForceMode.Impulse);
        }

        public virtual void Dead()
        {
            ToDeathState();

        }
    }
}