﻿using Ai;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace StateMachine
{
    public class BolidFastMoveState : BolidPatrolState
    {
        public BolidFastMoveState(SimpleDrone statePatternEnemy) : base(statePatternEnemy)
        {
            enemy = statePatternEnemy;
            startPosition = enemy.transform.position;
            enemyBolid = enemy as BolidDrone;
        }

        public override void UpdateState()
        {
            MoveFast();
        }

        private void MoveFast()
        {
            if (enemy.changepPoscurr > 0)
            {
                enemy.changepPoscurr -= Time.deltaTime;
                if (enemy.changepPoscurr < 0)
                {
                    enemy.changepPoscurr = UnityEngine.Random.Range(enemy.changepPosMin, enemy.changepPosMax) * -1;           
                    enemy.moveDirection.x *= -1;
                    ToSlowState();
                }
            }
            else
            {
                enemy.changepPoscurr += Time.deltaTime;
                if (enemy.changepPoscurr > 0)
                {
                    enemy.changepPoscurr = UnityEngine.Random.Range(enemy.changepPosMin, enemy.changepPosMax);
                    enemy.moveDirection.x *= -1;
                    ToSlowState();
                }
            }

            //Rotation left-right
            var angle = new Vector3();
            if (enemy.moveDirection.x > 0f)
                angle.y = 90;
            else if (enemy.moveDirection.x < 0f)
                angle.y = -90;

            Quaternion target = Quaternion.Euler(angle.x, angle.y, angle.z);
            enemy.shipRoot.localRotation = Quaternion.RotateTowards(enemy.shipRoot.localRotation, target, Time.deltaTime * enemy.rotateSpeed);
            //  enemy.transform.Translate(new Vector3(enemy.moveDirection.x, 0, 0) * ((enemy.speed) * Time.deltaTime));
            enemy.my_Rigidbody.AddForce(new Vector3(enemy.moveDirection.x * -1, 0, 0) * ((enemy.speed) * Time.deltaTime), ForceMode.Impulse);
        }
    }
}