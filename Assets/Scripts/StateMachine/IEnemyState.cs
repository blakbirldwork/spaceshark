﻿using UnityEngine;

namespace StateMachine
{

    public interface IEnemyState
    {

        void UpdateState();

        void OnTriggerEnter(Collider other);

        void ToPatrolState();

        void ToAlertState();

        void ToDeathState();

        void Dead();

    }
}