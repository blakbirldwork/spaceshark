﻿using Ai;
using UnityEngine;

namespace StateMachine
{
    public class PatrolState : IEnemyState

    {
        protected SimpleDrone enemy;
        protected int nextWayPoint;
        protected Vector3 startPosition;


        public PatrolState(SimpleDrone statePatternEnemy)
        {
            enemy = statePatternEnemy;
            startPosition = enemy.transform.position;
        }

        public virtual void UpdateState()
        {
            Look();
            Patrol();
        }

        public virtual void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.CompareTag("Player"))
                enemy.Damage();
        }

        public virtual void ToDeathState()
        {
            enemy.currentState = enemy.deathlState;
        }

        public virtual void ToPatrolState()
        {
            Debug.Log("Can't transition to same state");
        }

        public virtual void ToAlertState()
        {
            Debug.Log("Find PLayer");
            enemy.currentState = enemy.alertState;
        }

        protected virtual void Look()
        {

        }

        protected virtual void Patrol()
        {
            if (enemy.transform.position.y > 60)
            {
                enemy.changepPoscurr = -10;
            }
            else if (enemy.transform.position.y < 3)
            {
                enemy.changepPoscurr = 5;
            }

            if (enemy.changepPoscurr > 0)
            {
                enemy.changepPoscurr -= Time.deltaTime;
                if (enemy.changepPoscurr < 0)
                {
                    enemy.changepPoscurr = UnityEngine.Random.Range(enemy.changepPosMin, enemy.changepPosMax) * -1;
                    enemy.moveDirection.x *= -1;
                }
            }
            else
            {
                enemy.changepPoscurr += Time.deltaTime;
                if (enemy.changepPoscurr > 0)
                {
                    enemy.changepPoscurr = UnityEngine.Random.Range(enemy.changepPosMin, enemy.changepPosMax);
                    enemy.moveDirection.x *= -1;
                }
            }

            //Rotation left-right
            var angle = new Vector3();
            if (enemy.moveDirection.x > 0f)
                angle.y = 90;
            else if (enemy.moveDirection.x < 0f)
                angle.y = -90;

            Quaternion target = Quaternion.Euler(angle.x, angle.y, angle.z);
            enemy.shipRoot.localRotation = Quaternion.RotateTowards(enemy.shipRoot.localRotation, target, Time.deltaTime * enemy.rotateSpeed);
         //   enemy.transform.Translate(new Vector3(enemy.moveDirection.x, enemy.changepPoscurr, 0) * ((enemy.speed) * Time.deltaTime));
         int dirr = 0;
            if (enemy.changepPoscurr > 0)
                dirr = 1;
            else
                dirr = -1;
         enemy.my_Rigidbody.AddForce(new Vector3(enemy.moveDirection.x * -1, dirr, 0) * ((enemy.speed * 2) * Time.deltaTime), ForceMode.Impulse);
        }

        public virtual void Dead()
        {
            ToDeathState();
        }
    }
}