﻿using UnityEngine;
using Ai;

namespace StateMachine
{

    public class GroundPatrolState : PatrolState
    {
        protected SimpleGroundDrone drone;

        public GroundPatrolState(SimpleDrone statePatternEnemy) : base(statePatternEnemy)
        {
            enemy = statePatternEnemy;
            startPosition = enemy.transform.position;
            drone = enemy as SimpleGroundDrone;
        }

        protected override void Look()
        {
            //    base.Look();
        }

        public override void ToDeathState()
        {
            enemy.animator.SetBool(AnimatorHelper.WALK, false);
            base.ToDeathState();
        }



        protected override void Patrol()
        {

            enemy.animator.SetBool(AnimatorHelper.WALK, true);
            enemy.transform.position = Vector3.MoveTowards(enemy.transform.position, drone.PatrolPoints[drone.currTarget], enemy.speed * Time.deltaTime);
            if (Vector3.Distance(enemy.transform.position, drone.PatrolPoints[drone.currTarget]) < 2)
            {
                drone.currTarget++;
                enemy.transform.LookAt(drone.PatrolPoints[drone.currTarget]);
            }

            //  base.Patrol();
        }
    }
}