﻿using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Ai;

namespace StateMachine
{

    public class GroundJumpState : PatrolState
    {
        protected JumpDrone drone;
        private bool isMove;
        private int currPoint = 0;
        private int jumpSpeed = 5;
        Sequence seq;

        public GroundJumpState(SimpleDrone statePatternEnemy) : base(statePatternEnemy)
        {
            enemy = statePatternEnemy;
            startPosition = enemy.transform.position;
            drone = enemy.GetComponent<JumpDrone>();

        }
        protected override void Patrol()
        {
            if (!isMove)
                StartJump();
        }

        public void StopTween()
        {
            seq.Kill();

            isMove = false;
        }

        private void StartJump()
        {
            isMove = true;
            seq = DOTween.Sequence();
            seq.Append(drone.transform.DOLookAt(drone.points[currPoint], 0.5f));
            seq.AppendInterval(1f);
            seq.AppendCallback(()=> {
                drone.CastToAnimator("StartJump");
                });
            seq.AppendInterval(0.3f);
            seq.Append(drone.my_Rigidbody.DOJump(drone.points[currPoint], drone.JumpPower, drone.JumpNum, jumpSpeed));
            seq.AppendInterval(jumpSpeed);
            seq.AppendCallback(() =>
            {
                isMove = false;
                drone.CastToAnimator("Grounded");
                if (currPoint >= drone.points.Count - 1)
                {
                    currPoint--;
                }
                else
                    currPoint++;
            });
        }

        private Vector3[] GetPatch()
        {
            var _patch = new List<Vector3>();

            if (currPoint == 0)
            {
                for (int i = 0; i < 3; i++)
                {
                    _patch.Add(drone.points[currPoint]);
                    currPoint++;
                }
            }
            else
            {
                for (int i = 0; i < 3; i++)
                {
                    currPoint--;
                    _patch.Add(drone.points[currPoint]);
                }
            }
            return _patch.ToArray();
        }
    }
}