﻿using UnityEngine;
using DG.Tweening;
using StateMachine;

namespace Ai
{

    public class DroneDanger : SimpleDrone
    {
        [SerializeField]
        Transform leftSaw;
        [SerializeField]
        Transform rightSaw;
        [SerializeField]
        bool isSartRotation = false;
        [SerializeField]
        bool isReversRotation = false;

        public float explosiveForce = 40;
        public Vector3 force = new Vector3(0, 0, 1);
        public Vector3 freezeRotation;
        public float rotateSawSpeed = 0.01f;
        public int angleX = 10;
        public int angleY = 10;
        public int angleZ = 10;

        private int _angleX = 10;
        private int _angleY = 10;
        private int _angleZ = 10;

        private void Start()
        {
            if (PhotonNetwork.player.IsMasterClient)
                Init();

            weapon = GetComponentInChildren<BaseWeapon>();
            RotationInit();
            my_Rigidbody = GetComponent<Rigidbody>();
        }

        protected override void Init()
        {
            InitPosition = transform.position;
            changepPoscurr = Random.Range(changepPosMin, changepPosMax);
            alertState = new AlertState(this);
            patrolState = new SawPatrolState(this);
            deathlState = new DeathState(this);
            currentState = patrolState;
            isInited = true;
        }

        public void RotationInit()
        {
            _angleX = angleX;
            if (isReversRotation)
                _angleY = angleY * -1;

            _angleZ = angleZ;
            
            if (isSartRotation)
                StartRotation();
        }

        public void StartRotation()
        {
            angleX += _angleX;
            angleY += _angleY;
            angleZ += _angleZ;

            if (angleX >= 360)
                angleX = 0;
            if (angleY >= 360)
                angleY = 0;
            if (angleZ >= 360)
                angleZ = 0;

            force.x = angleX;
            force.y = angleY;
            force.z = angleZ;

            if (freezeRotation.x != 0)
                force.x = freezeRotation.x;
            if (freezeRotation.y != 0)
                force.y = freezeRotation.y;
            if (freezeRotation.z != 0)
                force.z = freezeRotation.z;

            leftSaw.DORotate(force, rotateSawSpeed).OnComplete(StartRotation);
            rightSaw.DORotate(force, rotateSawSpeed);
        }

    }
}