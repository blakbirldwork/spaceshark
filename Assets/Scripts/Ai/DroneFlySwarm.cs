﻿using StateMachine;
using UnityEngine;

namespace Ai
{

    public class DroneFlySwarm : SimpleDrone
    {
        public GameObject[] SwarmDrones;
        private int currDrone = 0;

        protected override void Init()
        {
            InitPosition = transform.position;
            changepPoscurr = Random.Range(changepPosMin, changepPosMax);
            alertState = new AlertState(this);
            patrolState = new SwarmPatrolState(this);
            deathlState = new SwarmDeathState(this);
            currentState = patrolState;
            isInited = true;
        }


        protected override void HeathChange(float value)
        {
            photonView.RPC("DeathOneInSwarm", PhotonTargets.AllViaServer);
            base.HeathChange(value);
        }


        [PunRPC]
        void DeathOneInSwarm()
        {
            var explosive = PoolManager.SpawnObject(deathPartickle, SwarmDrones[currDrone].transform.position, transform.rotation);
            LevelPoolManager.Instance.GetObj("fuel", SwarmDrones[currDrone].transform.position);
            SwarmDrones[currDrone].SetActive(false);
            currDrone++;
            if (currDrone >= SwarmDrones.Length)
                photonView.RPC("ToDeathState", PhotonTargets.AllViaServer);
        }


        [PunRPC]
        void ToDeathState()
        {
            currentState = deathlState;
        }


        public new void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
        {
            if (stream.isWriting)
            {
                // We own this player: send the others our data
                stream.SendNext(this.Heath);
                stream.SendNext(this.currDrone);
            }
            else
            {
                // Network player, receive data
                this.Heath = (float)stream.ReceiveNext();
                this.Heath = (int)stream.ReceiveNext();
            }
        }
    }
}