﻿using System.Collections.Generic;
using UnityEngine;
using StateMachine;

namespace Ai
{

    public class SimpleGroundDrone : SimpleDrone
    {

        public List<Vector3> PatrolPoints;


        public int currTarget
        {
            get
            {
                return _currTarget;
            }
            set
            {
                _currTarget = value;
                if (_currTarget >= PatrolPoints.Count)
                    _currTarget = 0;
            }
        }

        protected int _currTarget = 0;

        protected override void Init()
        {
            InitPosition = transform.position;
            animator = shipRoot.GetComponent<Animator>();
            changepPoscurr = Random.Range(changepPosMin, changepPosMax);
            alertState = new AlertState(this);
            patrolState = new GroundPatrolState(this);
            deathlState = new DeathState(this);
            currentState = patrolState;
            isInited = true;
        }

        public void AddPatrolPositions(Transform[] positions)
        {
            PatrolPoints = new List<Vector3>();
            for (int i = 0; i < positions.Length; i++)
            {
                PatrolPoints.Add(positions[i].position);
            }
        }

        [PunRPC]
        void ToDeathState()
        {
            currentState = deathlState;
        }
    }
}