﻿using StateMachine;
using UnityEngine;

namespace Ai
{
    public class BolidDrone : SimpleDrone
    {
        public BolidFastMoveState FastState;
        public float SlowSpeed = 2;

        protected override void Init()
        {
            InitPosition = transform.position;
            changepPoscurr = Random.Range(changepPosMin, changepPosMax);
            alertState = new AlertState(this);
            patrolState = new BolidPatrolState(this);
            deathlState = new DeathState(this);
            FastState = new BolidFastMoveState(this);
            currentState = patrolState;
            isInited = true;
        }
    }
}