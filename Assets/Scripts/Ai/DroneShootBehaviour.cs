﻿using UnityEngine;

namespace Ai
{

    public class DroneShootBehaviour : MonoBehaviour
    {

        private BaseWeapon weapon;

        // Use this for initialization
        void Start()
        {
            weapon = GetComponentInChildren<BaseWeapon>();
        }


        private void OnTriggerEnter(Collider other)
        {

            if (other.gameObject.CompareTag("Player"))
            {
                if (weapon != null)
                {
                    weapon.RpcTryShoot();
                }
            }
        }
    }
}