﻿using StateMachine;
using UnityEngine;

namespace Ai
{

    public class SimpleDrone : Photon.PunBehaviour, IPunObservable, IDamage
    {
        public float Heath;
        public Rigidbody my_Rigidbody;
        public Transform eyesForward;
        public Transform eyesDown;
        public Transform eyesUp;
        public Transform eyesBack;
        public float sightRange = 20f;
        public float searchingDuration = 4f;
        public float changepPosMax = 10;
        public float changepPosMin = 2;
        public float changepPoscurr = 2;
        public Animator animator;
        public GameObject deathPartickle;
        public IEnemyState currentState;
        public IEnemyState alertState;
        public IEnemyState patrolState;
        public IEnemyState deathlState;
        public Vector3 moveDirection = new Vector3(1, 0, 0);
        public float speed = 0.5f;
        public float rotateSpeed;
        public Transform shipRoot;
        public Vector3 InitPosition;
        public Quaternion synchRotation;
        protected bool isInited;

        protected BaseWeapon weapon;

        // Use this for initialization
        void Start()
        {
            if (PhotonNetwork.player.IsMasterClient)
                Init();
                      
            weapon = GetComponentInChildren<BaseWeapon>();
            my_Rigidbody = GetComponent<Rigidbody>();
        }

        public void DebugInConsole(string s)
        {
            Debug.Log(s);
        }

        protected virtual void Init()
        {
            InitPosition = transform.position;
            changepPoscurr = Random.Range(changepPosMin, changepPosMax);
            alertState = new AlertState(this);
            patrolState = new PatrolState(this);
            deathlState = new DeathState(this);
            currentState = patrolState;
            isInited = true;
        }

        protected virtual void HeathChange(float value)
        {
            if (value <= 0)
            {
                if (PhotonNetwork.player.IsMasterClient)
                    photonView.RPC("ToDeathState", PhotonTargets.AllViaServer, null);
            }
        }

        [PunRPC]
        public  void ToDeathState()
        {
            currentState = deathlState;
        }

        public virtual void SpounDeathPart()
        {
            photonView.RPC("RpcSpounDeath", PhotonTargets.AllViaServer, transform.position);
        }

        [PunRPC]
        public void RpcSpounDeath(Vector3 pos)
        {
            var explosive = PoolManager.SpawnObject(deathPartickle, pos, transform.rotation);
            LevelPoolManager.Instance.GetObj("fuel", pos);
        }

        public virtual void Damage()
        {
            Heath--;
            HeathChange(Heath);
        }

        public void CastToAnimator(string value)
        {
            photonView.RPC("RpcCastToAnimator", PhotonTargets.AllBufferedViaServer, value);
        }

        [PunRPC]
        public void RpcCastToAnimator(string value)
        {
            if (animator != null)
                animator.SetTrigger(value);
        }

        void Update()
        {
            if (currentState != null)
                currentState.UpdateState();

            if (shipRoot != null)
                SynchShipRootRotate();
        }

        protected virtual void SynchShipRootRotate()
        {
            if (!isInited && shipRoot != null)
                shipRoot.transform.rotation = synchRotation;
            else
                synchRotation = shipRoot.transform.rotation;
        }

        public void OnCollisionEnter(Collision collision)
        {
            if (currentState != null)
                currentState.OnTriggerEnter(collision.collider);
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.CompareTag("Player"))
            {
                if (weapon != null)
                    weapon.RpcTryShoot();
            }
        }

        public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
        {
            if (stream.isWriting)
            {
                // We own this player: send the others our data
                stream.SendNext(this.Heath);       
                stream.SendNext(this.synchRotation);         
            }
            else
            {
                // Network player, receive data
                this.Heath = (float)stream.ReceiveNext();
                this.synchRotation = (Quaternion)stream.ReceiveNext();
            }
        }

        public override void OnMasterClientSwitched(PhotonPlayer newMasterClient)
        {
            base.OnMasterClientSwitched(newMasterClient);
            Init();
        }
    }
}