﻿using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;
using StateMachine;

namespace Ai
{

    public class JumpDrone : SimpleDrone
    {

        public float JumpPower = 5;
        public int JumpNum = 1;
        public List<Vector3> points;

        protected override void Init()
        {
            InitPosition = transform.position;
            animator = shipRoot.GetComponent<Animator>();
            changepPoscurr = Random.Range(changepPosMin, changepPosMax);
            alertState = new AlertState(this);
            patrolState = new GroundJumpState(this);
            deathlState = new DeathState(this);
            currentState = patrolState;
            animator = GetComponentInChildren<Animator>();
            isInited = true;
        }

        [PunRPC]
        void ToDeathState()
        {
            (patrolState as GroundJumpState).StopTween();
            currentState = deathlState;
        }

        public void AddPatrolPositions(Transform[] groundPatrolPoints1)
        {
            foreach (var item in groundPatrolPoints1)
            {
                points.Add(item.position);
            }
            transform.position = points[0];
        }
    }
}
