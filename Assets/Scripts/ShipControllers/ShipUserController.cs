﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityStandardAssets.Cameras;
using UnityStandardAssets.CrossPlatformInput;
using UnityEngine.UI;

public class ShipUserController : Photon.PunBehaviour , IDamage
{

    public int Health = 3;
    public GameObject CameraController;

    [SerializeField]  ParticleSystem partickleNormal;
    [SerializeField]  ParticleSystem partickleForsage;
    [SerializeField]  private float fuelScalar = 0.025f;
    [SerializeField]  private float maxFuel = 10;
    private float fuel;
    [SerializeField]  private float maxForsage = 4;
    private float forsageCurr;
    [SerializeField]  private float forsageScalar = 0.01f;
    private BaseWeapon weapon;
    private ShipMovement Ship;
    private Vector3 move;
    private bool fire;
    private Vector3 lastVector = new Vector3(0, -1, 0);
    private bool isActivate = false;
    private bool forsage = false;
    private Vector3 currStartPosition;
    private bool isNeedShot;

    private void Start()
    {
        transform.position = new Vector3(0, 2, 0);
        //TODO: set Random spoun points
        //  currStartPosition = spounPositions[UnityEngine.Random.Range(0, spounPositions.Length)];
        //  currStartPosition = spounPositions[0];
        transform.eulerAngles = new Vector3(0, 180, 0);
        //    transform.position = currStartPosition;
        // Set up the reference.
        Ship = GetComponent<ShipMovement>();
        weapon = GetComponentInChildren<BaseWeapon>();
        fuel = maxFuel;
        forsageCurr = maxForsage;
        OnStartLocalPlayer();
    }

    private void onRestartClick()
    {
        Ship.ResetParams();
        transform.eulerAngles = new Vector3(0, -90, 0);
        transform.position = currStartPosition;
    }

    public void AddFuel(float fuelCount)
    {
        fuel += fuelCount;
        if (fuel > maxFuel)
            fuel = maxFuel;

        UpdateScroll();
    }

    public void Damage()
    {
        photonView.RPC("RpcHealthChange", PhotonTargets.AllViaServer);
    }

    [PunRPC]
    private void RpcHealthChange()
    {
        if (!photonView.isMine)
            return;

        Health--;
        if (Health <= 0)
        {
            isActivate = false;
            photonView.RPC("RpcDeath", PhotonTargets.AllViaServer);
            StartCoroutine("WatToAlive");
        }
    }

    [PunRPC]
    private void RpcDeath()
    {
        isActivate = false;
        move = Vector3.zero;
        StartCoroutine("DeathState");
    }

    [PunRPC]
    private void RpcAlive()
    {
        if(photonView.isMine)
        isActivate = true;
        StopCoroutine("DeathState");
        Transform child = transform.GetChild(0);
        child.gameObject.SetActive(true);
        Health = 3;
    }

    public virtual void TryShoot()
    {
        weapon.RpcTryShoot();
    }

    IEnumerator DeathState()
    {
        yield return new WaitForSeconds(0.1f);
        Transform child = transform.GetChild(0);
        child.gameObject.SetActive(!child.gameObject.activeInHierarchy);
        StartCoroutine("DeathState");
    }

    IEnumerator WatToAlive()
    {
        yield return new WaitForSeconds(3f);
        photonView.RPC("RpcAlive", PhotonTargets.AllViaServer);
    }

    public  void OnStartLocalPlayer()
    {
        if (!photonView.isMine)
            return;
     
            isActivate = true;
            var camera = Instantiate(CameraController, transform.position, Quaternion.identity);
            camera.GetComponent<FreeLookCam>().Target = transform;
            camera.transform.GetComponentInChildren<LookatTarget>().Target = GameObject.FindGameObjectWithTag(TriggerStrings.ZERRO_POINT).transform;
            LevelSettings.Instance.OnRestartClick += onRestartClick;
            LevelSettings.Instance.OnForsagetClick += onForsageClick;
            LevelSettings.Instance.OnShootClick += onShootClick;       
    }

    private void onShootClick()
    {
        isNeedShot = true;
    }

    private void onForsageClick(bool value)
    {
        forsage = value;
        if (value)
        {
            partickleForsage.gameObject.SetActive(true);
            partickleForsage.Play();
            partickleNormal.gameObject.SetActive(false);
        }
        else
        {
            partickleForsage.gameObject.SetActive(false);
            partickleNormal.gameObject.SetActive(true);
            partickleNormal.Play();
        }
    }



    private void Update()
    {
        if (!isActivate)
            return;

        if (!photonView.isMine)
            return;

        fuel -= Time.deltaTime + fuelScalar;
        UpdateScroll();

        if (fuel <= 0)
            FuelIsEmpty();

        var joyValue = LevelSettings.Instance.joystick.JoystickValue;

        //TODO: Update Inputs
        // Get the axis and shoot input.
        float h = joyValue.x; // CrossPlatformInputManager.GetAxis("Horizontal");   
        h *= -1;
        float v = joyValue.y; //CrossPlatformInputManager.GetAxis("Vertical");
                              //  if (transform.position.z > 0)
                              //      v *= -1;
        move = new Vector3(v, h, 0);
        if (move != Vector3.zero)
        { 
            if(!partickleNormal.gameObject.activeInHierarchy)
                partickleNormal.gameObject.SetActive(true);

            lastVector = move;
        }
        else
            partickleNormal.gameObject.SetActive(false);

        fire = isNeedShot;
        isNeedShot = false;

        if (forsage)
        {
            forsageCurr -= Time.deltaTime + forsageScalar;
            if (forsageCurr < 0)
            { forsageCurr = 0;
                forsage = false;
            }
        }
        else
        {
            if (forsageCurr < maxForsage)
            {
                forsageCurr += Time.deltaTime / 2;
                if (forsageCurr > maxForsage)
                    forsageCurr = maxForsage;
            }
        }
        float size = forsageCurr / maxForsage;
        LevelSettings.Instance.ForsageSlider.value = size;

        if (Input.GetKeyDown(KeyCode.Space) || fire)
        {
            if(weapon.isCanFire())
           photonView.RPC("RpcTryShoot", PhotonTargets.AllViaServer,null);
        }
    }

   [PunRPC]
    private void RpcTryShoot()
    {
        weapon.RpcTryShoot(weapon.transform.position, weapon.transform.rotation, lastVector);
    }
  
  
    private void UpdateScroll()
    {
        float size = fuel / maxFuel;
        LevelSettings.Instance.FuelSlider.value = size;
    } 

    private void FuelIsEmpty()
    {
        Debug.LogWarning("Fuel is Empty add new Fuel");
        AddFuel(10);
    }

     private void FixedUpdate()
    {
        if (!photonView.isMine)
            return;
        // Call the Move function of the ship controller
        Ship.Move(move, fire, forsage);
        fire = false;
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
        {
            //We own this player: send the others our data
            stream.SendNext(Health);
        }
        else
        {
            //Network player, receive data
            Health = (int)stream.ReceiveNext();
        }
    }
}
