﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class ShipMovement : Photon.MonoBehaviour
{
    public float speed = 1f;
    public float forsageSpeed;
    public float rotateSpeed = 300f;
    public float angleStep = 2;

    [SerializeField] Transform shipRoot;

    private Rigidbody _rigidbody;
    private bool isStayOnFlor;
    private bool isStayOnCelling;
    private Quaternion target;

    private void Start()
    {
        if (!photonView.isMine)
            return;

        _rigidbody = GetComponent<Rigidbody>();
        LevelSettings.Instance.OnSpeedChange += onSpeedChange;
        LevelSettings.Instance.OnForsageChange += onForsageChange;
        LevelSettings.Instance.speedCorect.SetValue(speed);
        LevelSettings.Instance.forsageSpeedCorect.SetValue(forsageSpeed);
    }

    private void onSpeedChange(float obj)
    {
        speed = obj;
    }

    private void onForsageChange(float obj)
    {
        forsageSpeed = obj;
    }

    public void ResetParams()
    {
        _rigidbody.isKinematic = true;
        StartCoroutine(WaitToDeactivate());
    }

    IEnumerator WaitToDeactivate()
    {
        yield return new WaitForSeconds(0.1f);
        _rigidbody.isKinematic = false;
    }

    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.tag == TriggerStrings.FLOR)
        {
            isStayOnFlor = true;
        }
        else if (collision.gameObject.tag == TriggerStrings.CELING)
        {
            isStayOnCelling = true;
        }
    }

    public void Move(Vector3 moveDirection, bool jump, bool forsage)
    {
        if (moveDirection != Vector3.zero)
        {
            Vector3 rotationVector = moveDirection;
            rotationVector.x = 0;
            float speedChanger = rotationVector.y < 0 ? -rotationVector.y : rotationVector.y;

            //Move left-right
            var angle = new Vector3();
            if (moveDirection.y > 0)
                angle.y = 90;
            else if (moveDirection.y < 0)
                angle.y = -90;

            //Move up - down
            if (moveDirection.x != 0f)
                angle.x = -90 * moveDirection.x;
            //Not Move in ground
            if (isStayOnFlor && moveDirection.x < 0)
                moveDirection.x = 0;

            if (isStayOnCelling && moveDirection.x > 0)
                moveDirection.x = 0;

            //Rotation
            target = Quaternion.Euler(angle.x, angle.y, angle.z);

            if (!forsage)
                _rigidbody.AddForce(new Vector3(moveDirection.y * -1, moveDirection.x, 0) * ((speed * 2) * Time.deltaTime), ForceMode.Impulse);
            else
                _rigidbody.AddForce(new Vector3(moveDirection.y * -1, moveDirection.x, 0) * ((forsageSpeed * 2) * Time.deltaTime), ForceMode.Impulse);
        }
        shipRoot.localRotation = Quaternion.RotateTowards(shipRoot.localRotation, target, Time.deltaTime * rotateSpeed);
        isStayOnFlor = false;
        isStayOnCelling = false;
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
    }
}
