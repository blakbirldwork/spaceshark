﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipRigidbodyHelper : MonoBehaviour {
    private Vector3 startPos;
    private Quaternion startRotation;

    private void Awake()
    {
        startPos = transform.localPosition;
        startRotation = transform.localRotation;
    }

    private void FixedUpdate()
    {
        transform.localPosition = startPos;
       // transform.localRotation = startRotation;
    }
}
