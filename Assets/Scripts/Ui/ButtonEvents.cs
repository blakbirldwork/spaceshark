﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ButtonEvents : MonoBehaviour, IPointerDownHandler,IPointerUpHandler
{
    public Action OnDown;
    public Action OnUp;

    public void OnPointerDown(PointerEventData eventData)
    {
        if (OnDown != null)
            OnDown();
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (OnUp != null)
            OnUp();
    }
}
