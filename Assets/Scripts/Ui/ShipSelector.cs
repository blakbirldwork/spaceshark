﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipSelector : MonoBehaviour {

    public GameObject [] Ships;
    public string [] ShipsName;
    public int currIndex;

    private void Start()
    {
        currIndex = 0;
        PlayerPrefs.SetString("PlayerShip", ShipsName[currIndex]);
    }

    public void RightClick()
    {
        currIndex++;
        if (currIndex >= Ships.Length)
            currIndex = 0;

        ActivateShip();
    }

    private void ActivateShip()
    {
        foreach (var item in Ships)
        {
            item.gameObject.SetActive(false);
        }
        Ships[currIndex].SetActive(true);
        PlayerPrefs.SetString("PlayerShip",ShipsName[currIndex]);
    }

    public void LeftClick()
    {
        currIndex--;
        if (currIndex < 0)
            currIndex = Ships.Length - 1;

        ActivateShip();
    }
    
}
