﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NguiSpeedCorect : MonoBehaviour {
    [SerializeField]
    UIButton plus;
    [SerializeField]
    UIButton minus;
    [SerializeField]
    UILabel speedLabel;
    [SerializeField]
    float step = 0.5f;

    public Action<float> OnValueChange;
    private float value;

    public void SetValue(float value)
    {
        this.value = value;
        speedLabel.text = value.ToString();
    }

    public void OnPlusClick()
    {
        value += step;
        speedLabel.text = value.ToString();
        if (OnValueChange != null)
            OnValueChange(value);
    }

    public void OnMinusClick()
    {
        value -= step;
        if (value <= 0)
            value = 0;

        speedLabel.text = value.ToString();
        if (OnValueChange != null)
            OnValueChange(value);
    }
	
	
}
