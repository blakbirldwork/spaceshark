﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System;
using UnityEngine.SceneManagement;

public class NetworkConnector : Photon.PunBehaviour {
    [SerializeField]  Button startButton;
    [SerializeField]  UILabel waitObj;
    [SerializeField]  Animator ShipHolder;
    [SerializeField]  Animator DoorHolder;
    bool isConnecting;
    public  string _gameVersion = "0.01";

    // Use this for initialization
    void Start () {
        //  startButton.onClick.AddListener(StartClick);   
        PhotonNetwork.ConnectUsingSettings(_gameVersion);      
    }

    public void StartClick()
    {
        StartCoroutine("WaitAnimtions");
    }

    public void Connect()
    {
        // keep track of the will to join a room, because when we come back from the game we will get a callback that we are connected, so we need to know what to do then
        isConnecting = true;
        PhotonNetwork.automaticallySyncScene = true;
        // we check if we are connected or not, we join if we are , else we initiate the connection to the server.
        if (PhotonNetwork.connected)
        {
            Debug("connected");
            // #Critical we need at this point to attempt joining a Random Room. If it fails, we'll get notified in OnPhotonRandomJoinFailed() and we'll create one.
              PhotonNetwork.JoinRandomRoom();
           // PhotonNetwork.CreateRoom(null);
        }
        else
        {
            Debug("not connected");
            // #Critical, we must first and foremost connect to Photon Online Server.
            PhotonNetwork.ConnectUsingSettings(_gameVersion);
        }
    }

    IEnumerator WaitAnimtions()
    {
        //startButton.gameObject.SetActive(false);
        DoorHolder.SetTrigger("Start Anim");
        ShipHolder.SetTrigger("Start Fly");
        yield return new WaitForSeconds(2f);
        waitObj.gameObject.SetActive(true);
        Connect();
    }

    private void Debug(string text)
    {
        waitObj.text = text;
    }

   public override void OnConnectedToMaster()
    {
        Debug("OnConnectedToMaster" + isConnecting);
        if (isConnecting)
        {
            // #Critical: The first we try to do is to join a potential existing room. If there is, good, else, we'll be called back with OnPhotonRandomJoinFailed()
             PhotonNetwork.JoinRandomRoom();
        }
    }

    public override void OnPhotonRandomJoinFailed(object[] codeAndMsg)
    {
        Debug("OnPhotonRandomJoinFailed");
        // #Critical: we failed to join a random room, maybe none exists or they are all full. No worries, we create a new room.
        PhotonNetwork.CreateRoom(null);
    }

    public override void OnDisconnectedFromPhoton()
    {
        Debug("OnDisconnectedFromPhoton");
        // #Critical: we failed to connect or got disconnected. There is not much we can do. Typically, a UI system should be in place to let the user attemp to connect again.
        isConnecting = false;
        SceneManager.LoadScene(0);
    }

    public override void OnJoinedRoom()
    {
        Debug("OnJoinedRoom" + PhotonNetwork.room.PlayerCount);
        // #Critical: We only load if we are the first player, else we rely on  PhotonNetwork.automaticallySyncScene to sync our instance scene.
        if (PhotonNetwork.room.PlayerCount == 1)
        {
            // #Critical
            // Load the Room Level. 
            PhotonNetwork.LoadLevel(1);
        }
    }
}
