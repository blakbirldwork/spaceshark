﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System;

public class Player_ID : Photon.PunBehaviour, IPunObservable
{

   
    private string playerUniqueIdentity;
  
    private Transform myTransform;

  
    // Use this for initialization
    void Awake()
    {
        myTransform = transform;
        SetIdentity();
    }

  
    void SetIdentity()
    {
       
            myTransform.name = MakeUniqueIdentity();
        
    }

    string MakeUniqueIdentity()
    {
        string uniqueName = "Player " + this.photonView.owner.ID;
        return uniqueName;
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
        {
            // We own this player: send the others our data
            stream.SendNext(this.playerUniqueIdentity);
          
        }
        else
        {
            // Network player, receive data
            this.playerUniqueIdentity = (string)stream.ReceiveNext();
          
        }
    }
}
